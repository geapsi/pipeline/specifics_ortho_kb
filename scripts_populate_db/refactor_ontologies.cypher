MATCH (n)-[rel:SCO_RESTRICTION]->(m) WITH rel
CALL apoc.refactor.setType(rel, 'SCO') YIELD input, output RETURN COUNT(output) as SCO_RESTRICTION_TO_SCO
