// Import the onto
CALL n10s.onto.import.fetch("https://github.com/Planteome/plant-ontology/raw/master/po.owl","RDF/XML");
// Add extra labels
// missing all labels like CARO and NCBITaxon

CALL n10s.rdf.stream.fetch("https://github.com/Planteome/plant-ontology/raw/master/po.owl","RDF/XML", {limit:1000000000}) yield subject
WITH subject as sbjct
// PO
MATCH (r:Resource { uri: sbjct })
WHERE r.uri =~ "http://purl.obolibrary.org/obo/PO_.*"
SET r:PO;
CALL n10s.rdf.stream.fetch("https://github.com/Planteome/plant-ontology/raw/master/po.owl","RDF/XML", {limit:1000000000}) yield subject
WITH subject as sbjct
// NCBITaxon
MATCH (r:Resource { uri: sbjct })
WHERE r.uri =~ "http://purl.obolibrary.org/obo/NCBITaxon_.*"
SET r:PO:NCBITaxon;
// CARO
CALL n10s.rdf.stream.fetch("https://github.com/Planteome/plant-ontology/raw/master/po.owl","RDF/XML", {limit:1000000000}) yield subject
WITH subject as sbjct
MATCH (r:Resource { uri: sbjct })
WHERE r.uri =~ "http://purl.obolibrary.org/obo/CARO_.*"
SET r:PO:CARO;
