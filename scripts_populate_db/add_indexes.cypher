// creating indexes on all ids

// BASICS
CREATE INDEX gene_id IF NOT EXISTS FOR (g:Gene) ON (g.gene_id);
CREATE INDEX rna_id IF NOT EXISTS FOR (r:RNA) ON (r.rna_id);
CREATE INDEX protein_id IF NOT EXISTS FOR (p:Protein) ON (p.protein_id);

// LINKS BTW SPECIES
CREATE INDEX orthogroup_id IF NOT EXISTS FOR (o:Orthogroup) ON (o.orthogroup_id);
CREATE INDEX synteny_id IF NOT EXISTS FOR (s:Block) ON (s.block_id);

// ANNOTATION
CREATE INDEX protein_annotation_id IF NOT EXISTS FOR (pa:FunctionalAnnotation) ON (pa.annotation_id);

// QTL
CREATE INDEX qtl_id IF NOT EXISTS FOR (q:QTL) ON (q.qtl_id);

// TRANSCRIPTOMICS
CREATE INDEX sample_id IF NOT EXISTS FOR (s:Sample) ON (s.sample_id);
CREATE INDEX condition_id IF NOT EXISTS FOR (c:Condition) ON (c.condition_id);
