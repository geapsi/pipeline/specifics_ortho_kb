#!/usr/bin/env bash
# Create a neo4j database and import data in it
## Requirements
### proper install of docker and import of neo4j image (docker pull neo4j)

# VARIABLES
## USER VARIABLES
container_name="ortho_kb_test"
db_name="OrthoKB-example"
db_username="neo4j" # credentials when connecting to the Neo4j Browser
db_pwd="test"
# docker image TAG (see docker image ls); can be "latest"
neo4j_version="4.4.26"
neo4j_http_port=7474:7474
neo4j_bolt_port=7687:7687
### NEO4J PLUGINS VERSIONS
# neosemantics
n10s_v='4.4.0.3'
# apoc
apoc_v='4.4.0.24'
# Absolute path to neo4j directory
NEO_WDIR="my/path/to/neo4j/"
## INTERNAL VARIABLES

### modify at your own risk
import="neo4j/import"
nodes="${import}/nodes"
edges="${import}/edges"
# path to merged/ directory containing the csv files
input="example_data/results/merged"
config_line_import="bin/neo4j-admin import --verbose --database=${db_name} --quote='\"' --high-io --skip-bad-relationships=true --report-file /import/import.report \ "


# Deleting container with same name if exists
## Note: cannot have two containers running on the same ports
echo "#####################"
echo "Stopping and removing previous container if existed"
docker stop $container_name
docker container rm $container_name
docker container ls -a
echo "Removing old import, data and logs directories..."
sudo rm -rf $NEO_WDIR/data
sudo rm -rf $NEO_WDIR/logs
echo "Removing old neo4j import files"
read -p "Continue? (Y/N): " confirm && [[ $confirm == [yY] || $confirm == [yY][eE][sS] ]] || exit 1
mkdir -p $NEO_WDIR/import
[ ! -e $NEO_WDIR/import/ ] || rm -rf $NEO_WDIR/import/nodes/ $NEO_WDIR/import/edges/


echo "Creating neo4j directories"
ls -l $NEO_WDIR/
mkdir -p $NEO_WDIR/data
mkdir -p $NEO_WDIR/logs
mkdir -p $NEO_WDIR/plugins
mkdir -p $NEO_WDIR/import
mkdir -p $NEO_WDIR/scripts
ls -al $NEO_WDIR

cp scripts_populate_db/*.cypher $NEO_WDIR/scripts/

echo "#####################"
echo "Creating docker container"
docker run --name ${container_name} \
    --user=$(id -u):$(id -g) \
    -p${neo4j_http_port} -p${neo4j_bolt_port} \
    -d \
    -v $NEO_WDIR/data:/data \
    -v $NEO_WDIR/logs:/logs \
    -v $NEO_WDIR/plugins:/plugins \
    -v $NEO_WDIR/scripts:/scripts \
    -v $NEO_WDIR/import:/import \
    --env NEO4J_dbms_security_procedures_unrestricted=apoc.\\\* \
    --env NEO4J_dbms_unmanaged_extension_classes=n10s.endpoint=/rdf \
    --env NEO4J_apoc_import_file_enabled=true \
    --env NEO4J_apoc_export_file_enabled=true \
    --env NEO4J_AUTH=${db_username}/${db_pwd} \
    neo4j:${neo4j_version}


echo "Container created"
echo "#####################"

echo "Ensuring uniqueness of node files and copying nodes and edges files to neo4j directory"
docker_nodes="/${nodes#*/}"
docker_edges="/${edges#*/}"

import_nodes_edges=$(mktemp)
# delete previous file if exists
[ ! -e $outfile ] || rm -if $import_nodes_edges
# make tmp files
nodes_tmp=$(mktemp)
edges_tmp=$(mktemp)
sort_tmp=$(mktemp)

# go through csv files and associate header file (same name but .header)
for file in $(find $input -maxdepth 1 -type f -name *.csv); do
        filebase=$(basename $file)
        csv_file=$filebase
        header_file=${filebase%.*}.header
        if [[ $csv_file == *nodes* ]];
        then
                # ensure no duplicated rows
                sort $file | uniq > $sort_tmp
                mv $sort_tmp $file
                echo "--nodes=${docker_nodes}/${header_file},${docker_nodes}/${csv_file} \\" >> $nodes_tmp


        elif [[ $csv_file == *edges* ]];
        then
                echo "--relationships=${docker_edges}/${header_file},${docker_edges}/${csv_file} \\" >> $edges_tmp
        else
                echo "unrecognized file"
                echo $csv_file
        fi
done
cat $nodes_tmp $edges_tmp | sed '$ s/\\$//' > $import_nodes_edges
rm $nodes_tmp $edges_tmp

echo "Copying input files in neo4j directories"
mkdir -p $nodes
mkdir -p $edges
# nodes and edges
find $input -name '*nodes*' -exec cp "{}" $nodes \;
find $input -name '*edges*' -exec cp "{}" $edges \;
find $input -name '*ttl' -exec cp "{}" $nodes \;

echo "Copying Cypher scripts to neo4j directory"
cp scripts_populate_db/*.cypher $NEO_WDIR/scripts/
echo "#####################"

echo "Starting the database before import"
docker stop $container_name
docker start $container_name
docker exec -it $container_name bash -c "neo4j stop"
# Import
import_file=ortho_kb_import_file
(echo $config_line_import ; cat $import_nodes_edges) > $import_file
echo "Populating the docker container neo4j database"
docker exec -it $container_name bash -c "$(cat $import_file)"
# changing the default database to the chosen name
docker exec -it ${container_name} bash -c "sed -i "s/^#dbms.default_database=neo4j/dbms.default_database=${db_name}/" /var/lib/neo4j/conf/neo4j.conf"

echo "#####################"
echo "Downloading ontologies"
# DOWNLOAD ONTOLOGIES
NEO_PLUGINS=${NEO_WDIR}/plugins
## neosemantics
echo "Neosemantics"
rm -f $NEO_PLUGINS/neosemantics-**.jar
url="https://github.com/neo4j-labs/neosemantics/releases/download/${n10s_v}/neosemantics-${n10s_v}.jar"
filename=$(basename $url)
if [ -f "$NEO_PLUGINS/$filename" ]; then
    echo "$filename exists."
else
    echo "Downloading $filename"
    wget -q $url -P $NEO_PLUGINS
fi
## apoc
echo "APOC"
rm -f $NEO_PLUGINS/apoc-**all.jar
url="https://github.com/neo4j-contrib/neo4j-apoc-procedures/releases/download/${n10s_v}/apoc-${n10s_v}-all.jar"
filename=$(basename $url)
if [ -f "$NEO_PLUGINS/$filename" ]; then
    echo "$filename exists."
else
    echo "Downloading $filename"
    wget -q $url -P $NEO_PLUGINS
fi
echo "Connecting the graph with ontologies"
docker stop $container_name
docker start $container_name
docker exec -it $container_name bash -c "neo4j start"
# wait for the db to start
sleep 60
CYPHER_PATH=bin/cypher-shell
## prepare n10s for ontology import
echo "n10s initialization"
docker exec -it $container_name bash -c "${CYPHER_PATH} -u ${db_username} -p ${db_pwd} -f /scripts/initiate_n10s.cypher"
echo "Adding Plant Ontology (PO)"
docker exec -it $container_name bash -c "${CYPHER_PATH} -u ${db_username} -p ${db_pwd} -f /scripts/add_PO.cypher"
echo "Adding Plant Experimental Conditions Ontology (PECO)"
docker exec -it $container_name bash -c "${CYPHER_PATH} -u ${db_username} -p ${db_pwd} -f /scripts/add_PECO.cypher"
echo "Adding Plant Trait Ontology (TO)"
docker exec -it $container_name bash -c "${CYPHER_PATH} -u ${db_username} -p ${db_pwd} -f /scripts/add_TO.cypher"
echo "Adding user defined ontology terms and connecting them to their parent terms"
docker exec -it $container_name bash -c "${CYPHER_PATH} -u ${db_username} -p ${db_pwd} -f /scripts/create_new_ontology_terms_to_connect_to_onto.cypher"
## make sure indexes are created on important properties
docker exec -it $container_name bash -c "${CYPHER_PATH} -u ${db_username} -p ${db_pwd} -f /scripts/add_indexes.cypher"
echo "Adding relations from omics conditions to ontology terms"
docker exec -it $container_name bash -c "${CYPHER_PATH} -u ${db_username} -p ${db_pwd} -f /scripts/import_rnaseq_condition_onto_edges.cypher"
echo "Adding relations from QTL to ontology terms"
docker exec -it $container_name bash -c "${CYPHER_PATH} -u ${db_username} -p ${db_pwd} -f /scripts/import_genetics_qtl_onto_edges.cypher"
echo "Adding MapMan ontology and relationships"
docker exec -it $container_name bash -c "${CYPHER_PATH} -u ${db_username} -p ${db_pwd} -f /scripts/add_mapman_ontology.cypher"
echo "Adding Gene Ontology and relationships"
docker exec -it $container_name bash -c "${CYPHER_PATH} -u ${db_username} -p ${db_pwd} -f /scripts/add_gene_ontology.cypher"
echo "Refactoring ontologies where necessary"
docker exec -it $container_name bash -c "${CYPHER_PATH} -u ${db_username} -p ${db_pwd} -f /scripts/refactor_ontologies.cypher"
