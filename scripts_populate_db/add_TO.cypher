CALL n10s.onto.import.fetch("https://raw.githubusercontent.com/Planteome/plant-trait-ontology/master/plant-trait-ontology.obo.owl","RDF/XML");
// Add extra labels

// BFO: basic formal ontology
CALL n10s.rdf.stream.fetch("https://raw.githubusercontent.com/Planteome/plant-trait-ontology/master/plant-trait-ontology.obo.owl","RDF/XML", {limit:1000000000}) yield subject
WITH subject as sbjct
MATCH (r:Resource { uri: sbjct })
WHERE r.uri =~ "http://purl.obolibrary.org/obo/BFO_.*"
SET r:TO:BFO;

// CARO: Common Anatomy Reference Ontology
CALL n10s.rdf.stream.fetch("https://raw.githubusercontent.com/Planteome/plant-trait-ontology/master/plant-trait-ontology.obo.owl","RDF/XML", {limit:1000000000}) yield subject
WITH subject as sbjct
MATCH (r:Resource { uri: sbjct })
WHERE r.uri =~ "http://purl.obolibrary.org/obo/NCBITaxon_.*"
SET r:TO:CARO;

// CHEBI: Chemical Entities of Biological Interest
CALL n10s.rdf.stream.fetch("https://raw.githubusercontent.com/Planteome/plant-trait-ontology/master/plant-trait-ontology.obo.owl","RDF/XML", {limit:1000000000}) yield subject
WITH subject as sbjct
MATCH (r:Resource { uri: sbjct })
WHERE r.uri =~ "http://purl.obolibrary.org/obo/CHEBI_.*"
SET r:TO:CHEBI;

// ENVO: Environment Ontology
CALL n10s.rdf.stream.fetch("https://raw.githubusercontent.com/Planteome/plant-trait-ontology/master/plant-trait-ontology.obo.owl","RDF/XML", {limit:1000000000}) yield subject
WITH subject as sbjct
MATCH (r:Resource { uri: sbjct })
WHERE r.uri =~ "http://purl.obolibrary.org/obo/ENVO_.*"
SET r:TO:ENVO;

// EO: Plant Environment Ontology
CALL n10s.rdf.stream.fetch("https://raw.githubusercontent.com/Planteome/plant-trait-ontology/master/plant-trait-ontology.obo.owl","RDF/XML", {limit:1000000000}) yield subject
WITH subject as sbjct
MATCH (r:Resource { uri: sbjct })
WHERE r.uri =~ "http://purl.obolibrary.org/obo/EO_.*"
SET r:TO:EO;

// FOODON: Food Ontology
CALL n10s.rdf.stream.fetch("https://raw.githubusercontent.com/Planteome/plant-trait-ontology/master/plant-trait-ontology.obo.owl","RDF/XML", {limit:1000000000}) yield subject
WITH subject as sbjct
MATCH (r:Resource { uri: sbjct })
WHERE r.uri =~ "http://purl.obolibrary.org/obo/FOODON_.*"
SET r:TO:FOODON;

// GO: Gene Ontology
CALL n10s.rdf.stream.fetch("https://raw.githubusercontent.com/Planteome/plant-trait-ontology/master/plant-trait-ontology.obo.owl","RDF/XML", {limit:1000000000}) yield subject
WITH subject as sbjct
MATCH (r:Resource { uri: sbjct })
WHERE r.uri =~ "http://purl.obolibrary.org/obo/GO_.*"
SET r:TO:GO;

// NCBITaxon: NCBI Taxonomy
CALL n10s.rdf.stream.fetch("https://raw.githubusercontent.com/Planteome/plant-trait-ontology/master/plant-trait-ontology.obo.owl","RDF/XML", {limit:1000000000}) yield subject
WITH subject as sbjct
MATCH (r:Resource { uri: sbjct })
WHERE r.uri =~ "http://purl.obolibrary.org/obo/NCBITaxon_.*"
SET r:TO:NCBITaxon;

// PATO: Phenotype And Trait Ontology
CALL n10s.rdf.stream.fetch("https://raw.githubusercontent.com/Planteome/plant-trait-ontology/master/plant-trait-ontology.obo.owl","RDF/XML", {limit:1000000000}) yield subject
WITH subject as sbjct
MATCH (r:Resource { uri: sbjct })
WHERE r.uri =~ "http://purl.obolibrary.org/obo/PATO_.*"
SET r:TO:PATO;

// PCO: Population and Community Ontology
CALL n10s.rdf.stream.fetch("https://raw.githubusercontent.com/Planteome/plant-trait-ontology/master/plant-trait-ontology.obo.owl","RDF/XML", {limit:1000000000}) yield subject
WITH subject as sbjct
MATCH (r:Resource { uri: sbjct })
WHERE r.uri =~ "http://purl.obolibrary.org/obo/PCO_.*"
SET r:TO:PCO;

// TO: Plant Experimental Conditions Ontology
CALL n10s.rdf.stream.fetch("https://raw.githubusercontent.com/Planteome/plant-trait-ontology/master/plant-trait-ontology.obo.owl","RDF/XML", {limit:1000000000}) yield subject
WITH subject as sbjct
MATCH (r:Resource { uri: sbjct })
WHERE r.uri =~ "http://purl.obolibrary.org/obo/TO_.*"
SET r:TO;

// RO: Relation Ontology
CALL n10s.rdf.stream.fetch("https://raw.githubusercontent.com/Planteome/plant-trait-ontology/master/plant-trait-ontology.obo.owl","RDF/XML", {limit:1000000000}) yield subject
WITH subject as sbjct
MATCH (r:Resource { uri: sbjct })
WHERE r.uri =~ "http://purl.obolibrary.org/obo/RO_.*"
SET r:TO:RO;
