// connect condition to ontology terms describing it
CREATE INDEX resource_name IF NOT EXISTS FOR (r:Resource) ON (r.name);
CREATE INDEX condition_id IF NOT EXISTS FOR (c:Condition) ON (c.condition_id);

CALL apoc.periodic.iterate('
CALL apoc.load.csv("file:///edges/rnaseq_condition_onto_edges.csv", {header:false}) yield list as line
','
	WITH line
        WITH line[0] as cdt_id, line[1] as onto_term, line[2] as rel
        MATCH (c:Condition {condition_id: cdt_id})
        MATCH (r:Resource {name: onto_term})

        WITH c, r, rel,
        CASE WHEN rel = "IS_TISSUE" THEN [1] ELSE [] END as is_tissue,
        CASE WHEN rel = "SUBJECTED_TO" THEN [1] ELSE [] END as subjected_to,
        CASE WHEN rel = "HAS_LOCATION" THEN [1] ELSE [] END as has_location,
	CASE WHEN rel = "HAS_DEV_STAGE" THEN [1] ELSE [] END as has_dev_stage

        FOREACH (x IN is_tissue | MERGE (c)-[:IS_TISSUE]->(r))
        FOREACH (x IN subjected_to | MERGE (c)-[:SUBJECTED_TO]->(r))
        FOREACH (x IN has_location | MERGE (c)-[:HAS_LOCATION]->(r))
	FOREACH (x IN has_dev_stage | MERGE (c)-[:HAS_DEV_STAGE]->(r))
', {batchSize:500, parallel:false});
MATCH (:Condition)-[r]->(:Resource)
RETURN COUNT(r);
