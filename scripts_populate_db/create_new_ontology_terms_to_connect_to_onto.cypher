// Create new ontology term not already existent in their respective ontologies
CALL apoc.periodic.iterate('
CALL apoc.load.csv("file:///nodes/new_ontology_term_nodes.csv", {header:false}) yield list as line
','
	WITH line
	WITH line[0] as name, line[1] as label, line[2] as def, line[3] as uri, line[4] as status, line[5] as neo4j_labels
	MERGE (term:Resource:Manual {name: name, label: label, definition: def, uri: uri, status: status})
	WITH term, split(neo4j_labels, ";") as labels
        UNWIND labels as label
	CALL apoc.create.addLabels( id(term), [ label ] ) YIELD node
	RETURN "done"

', {batchSize:500, parallel:false});

// note: not possible to have relationships names as variable, so have to use a CASE WHEN/FOREACH
CALL apoc.periodic.iterate('
CALL apoc.load.csv("file:///edges/new_ontology_term_ontology_edges.csv", {header:false}) yield list as line
','
        WITH line
        WITH line[0] as new_term, line[1] as parent_term
        MATCH (child:Resource:Manual {name: new_term})
        MATCH (parent:Resource {name: parent_term})
        WITH child, parent
        MERGE (child)-[:SCO]->(parent)

', {batchSize:500, parallel:false});
MATCH (:Resource:Manual)-[r]->(:Resource)
RETURN COUNT(r);
