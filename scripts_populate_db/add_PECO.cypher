CALL n10s.onto.import.fetch("https://raw.githubusercontent.com/Planteome/plant-experimental-conditions-ontology/master/peco.owl","RDF/XML");
// Add extra labels

// BFO: basic formal ontology
CALL n10s.rdf.stream.fetch("https://raw.githubusercontent.com/Planteome/plant-experimental-conditions-ontology/master/peco.owl","RDF/XML", {limit:1000000000}) yield subject
WITH subject as sbjct
MATCH (r:Resource { uri: sbjct })
WHERE r.uri =~ "http://purl.obolibrary.org/obo/BFO_.*"
SET r:PECO:BFO;

// CARO: Common Anatomy Reference Ontology
CALL n10s.rdf.stream.fetch("https://raw.githubusercontent.com/Planteome/plant-experimental-conditions-ontology/master/peco.owl","RDF/XML", {limit:1000000000}) yield subject
WITH subject as sbjct
MATCH (r:Resource { uri: sbjct })
WHERE r.uri =~ "http://purl.obolibrary.org/obo/NCBITaxon_.*"
SET r:PECO:CARO;

// CHEBI: Chemical Entities of Biological Interest
CALL n10s.rdf.stream.fetch("https://raw.githubusercontent.com/Planteome/plant-experimental-conditions-ontology/master/peco.owl","RDF/XML", {limit:1000000000}) yield subject
WITH subject as sbjct
MATCH (r:Resource { uri: sbjct })
WHERE r.uri =~ "http://purl.obolibrary.org/obo/CHEBI_.*"
SET r:PECO:CHEBI;

// ENVO: Environment Ontology
CALL n10s.rdf.stream.fetch("https://raw.githubusercontent.com/Planteome/plant-experimental-conditions-ontology/master/peco.owl","RDF/XML", {limit:1000000000}) yield subject
WITH subject as sbjct
MATCH (r:Resource { uri: sbjct })
WHERE r.uri =~ "http://purl.obolibrary.org/obo/ENVO_.*"
SET r:PECO:ENVO;

// EO: Plant Environment Ontology
CALL n10s.rdf.stream.fetch("https://raw.githubusercontent.com/Planteome/plant-experimental-conditions-ontology/master/peco.owl","RDF/XML", {limit:1000000000}) yield subject
WITH subject as sbjct
MATCH (r:Resource { uri: sbjct })
WHERE r.uri =~ "http://purl.obolibrary.org/obo/EO_.*"
SET r:PECO:EO;

// FOODON: Food Ontology
CALL n10s.rdf.stream.fetch("https://raw.githubusercontent.com/Planteome/plant-experimental-conditions-ontology/master/peco.owl","RDF/XML", {limit:1000000000}) yield subject
WITH subject as sbjct
MATCH (r:Resource { uri: sbjct })
WHERE r.uri =~ "http://purl.obolibrary.org/obo/FOODON_.*"
SET r:PECO:FOODON;

// GO: Gene Ontology
CALL n10s.rdf.stream.fetch("https://raw.githubusercontent.com/Planteome/plant-experimental-conditions-ontology/master/peco.owl","RDF/XML", {limit:1000000000}) yield subject
WITH subject as sbjct
MATCH (r:Resource { uri: sbjct })
WHERE r.uri =~ "http://purl.obolibrary.org/obo/GO_.*"
SET r:PECO:GO;

// NCBITaxon: NCBI Taxonomy
CALL n10s.rdf.stream.fetch("https://raw.githubusercontent.com/Planteome/plant-experimental-conditions-ontology/master/peco.owl","RDF/XML", {limit:1000000000}) yield subject
WITH subject as sbjct
MATCH (r:Resource { uri: sbjct })
WHERE r.uri =~ "http://purl.obolibrary.org/obo/NCBITaxon_.*"
SET r:PECO:NCBITaxon;

// PATO: Phenotype And Trait Ontology
CALL n10s.rdf.stream.fetch("https://raw.githubusercontent.com/Planteome/plant-experimental-conditions-ontology/master/peco.owl","RDF/XML", {limit:1000000000}) yield subject
WITH subject as sbjct
MATCH (r:Resource { uri: sbjct })
WHERE r.uri =~ "http://purl.obolibrary.org/obo/PATO_.*"
SET r:PECO:PATO;

// PCO: Population and Community Ontology
CALL n10s.rdf.stream.fetch("https://raw.githubusercontent.com/Planteome/plant-experimental-conditions-ontology/master/peco.owl","RDF/XML", {limit:1000000000}) yield subject
WITH subject as sbjct
MATCH (r:Resource { uri: sbjct })
WHERE r.uri =~ "http://purl.obolibrary.org/obo/PCO_.*"
SET r:PECO:PCO;

// PECO: Plant Experimental Conditions Ontology
CALL n10s.rdf.stream.fetch("https://raw.githubusercontent.com/Planteome/plant-experimental-conditions-ontology/master/peco.owl","RDF/XML", {limit:1000000000}) yield subject
WITH subject as sbjct
MATCH (r:Resource { uri: sbjct })
WHERE r.uri =~ "http://purl.obolibrary.org/obo/PECO_.*"
SET r:PECO;

// RO: Relation Ontology
CALL n10s.rdf.stream.fetch("https://raw.githubusercontent.com/Planteome/plant-experimental-conditions-ontology/master/peco.owl","RDF/XML", {limit:1000000000}) yield subject
WITH subject as sbjct
MATCH (r:Resource { uri: sbjct })
WHERE r.uri =~ "http://purl.obolibrary.org/obo/RO_.*"
SET r:PECO:RO;
