// warning, must use absolute path for n10s
CALL n10s.onto.import.fetch("file:///import/nodes/mapman_ontology.ttl", "Turtle");
// Add labels
MATCH (r:Resource)
WHERE r.uri =~ "http://mapman.*"
SET r:MapMan;
// then connect nodes and edges
CREATE INDEX mapman_name IF NOT EXISTS FOR (mp:MapMan) ON (mp.name);
CREATE INDEX rna_id IF NOT EXISTS FOR (r:RNA) ON (r.rna_id);

CALL apoc.periodic.iterate('
CALL apoc.load.csv("file:///edges/rna_mapman_edges.csv", {header:false}) yield list as line
','
        WITH split(line[0], ";") as rna_names, line[1] as mapman_name
        UNWIND rna_names as rna_name
	MATCH (r:RNA {rna_id: rna_name})
        MATCH (mp:Resource:MapMan {name: mapman_name})
        MERGE (r)-[:HAS_ANNOTATION]->(mp)

', {batchSize:500, parallel:false});
MATCH (:RNA)-[r:HAS_ANNOTATION]->(:MapMan)
RETURN COUNT(r);
