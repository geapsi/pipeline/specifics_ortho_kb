// connect QTL to ontology terms describing it
CREATE INDEX resource_name IF NOT EXISTS FOR (r:Resource) ON (r.name);
CREATE INDEX condition_id IF NOT EXISTS FOR (q:QTL) ON (q.qtl_id);

// note: not possible to have relationships names as variable, so have to use a CASE WHEN/FOREACH
CALL apoc.periodic.iterate('
CALL apoc.load.csv("file:///edges/genetics_qtl_onto_edges.csv", {header:false}) yield list as line
','
        WITH line
        WITH line[0] as qtl_id, line[1] as onto_term, line[2] as rel
        MATCH (q:QTL {qtl_id: qtl_id})
        MATCH (r:Resource {name: onto_term})

        WITH q, r, rel,
	CASE WHEN rel = "HAS_TRAIT" THEN [1] ELSE [] END as has_trait,
        CASE WHEN rel = "IS_TISSUE" THEN [1] ELSE [] END as is_tissue,
        CASE WHEN rel = "SUBJECTED_TO" THEN [1] ELSE [] END as subjected_to,
        CASE WHEN rel = "HAS_LOCATION" THEN [1] ELSE [] END as has_location,
        CASE WHEN rel = "HAS_DEV_STAGE" THEN [1] ELSE [] END as has_dev_stage

        FOREACH (x IN has_trait | MERGE (q)-[:HAS_TRAIT]->(r))
	FOREACH (x IN is_tissue | MERGE (q)-[:IS_TISSUE]->(r))
        FOREACH (x IN subjected_to | MERGE (q)-[:SUBJECTED_TO]->(r))
        FOREACH (x IN has_location | MERGE (q)-[:HAS_LOCATION]->(r))
        FOREACH (x IN has_dev_stage | MERGE (q)-[:HAS_DEV_STAGE]->(r))
', {batchSize:500, parallel:false});
MATCH (:QTL)-[r]->(:Resource)
RETURN COUNT(r);
