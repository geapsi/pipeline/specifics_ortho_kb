CALL n10s.onto.import.fetch("http://current.geneontology.org/ontology/go.owl","RDF/XML");
CREATE INDEX go_name IF NOT EXISTS FOR (go:GO) ON (go.name);
// Add labels
MATCH (r:Resource)
WHERE r.uri =~ "http://purl.obolibrary.org/obo/GO_.*"
SET r:GO;

CREATE INDEX go_name IF NOT EXISTS FOR (go:GO) ON (go.name);
CREATE INDEX rna_id IF NOT EXISTS FOR (r:RNA) ON (r.rna_id);

CALL apoc.periodic.iterate('
CALL apoc.load.csv("file:///edges/rna_go_edges.csv", {header:false}) yield list as line
','
        WITH split(line[1], ";") as go_names, line[0] as rna_name
        UNWIND go_names as go_name
	MATCH (r:RNA {rna_id: rna_name})
        MATCH (go:Resource:GO {name: go_name})
        MERGE (r)-[:HAS_ANNOTATION]->(go)

', {batchSize:500, parallel:false});

MATCH (:RNA)-[r:HAS_ANNOTATION]->(:GO)
RETURN COUNT(r);
