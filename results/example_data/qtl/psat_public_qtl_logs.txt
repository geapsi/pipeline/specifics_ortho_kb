WARNING:root:Removed the following QTL, they had missing chromosome information:
 []
WARNING:root:The following biparental QTL had missing delimiting interval marker information:
 []
WARNING:root:The following diversity panel QTL had missing peakmarker marker information:
 ['beji2020_FD_Field_15JANV_5', 'beji2020_FD_Field_15JANV_6']
WARNING:root:The following biparental QTL had missing marker limits, but were conserved for correct peakmarker information:
 []
WARNING:root:Removed the following biparental QTL, they had missing marker information:
 []
WARNING:root:Removed the following diversity panel QTL, they had missing marker information:
 ['beji2020_FD_Field_15JANV_5', 'beji2020_FD_Field_15JANV_6']
INFO:root:State before marker inversion.
INFO:root:Number of QTL markers where leftmarker start > end: 32
INFO:root:Number of QTL markers where rightmarker start > end: 22
INFO:root:Number of QTL markers where peakmarker start > end: 32
INFO:root:Inverting marker positions where necessary
