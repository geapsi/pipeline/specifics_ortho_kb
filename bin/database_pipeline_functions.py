import re
import sys
import pandas as pd
import numpy as np

# def opened_with_error(filename, mode = 'r'):
#     try:
#         f = open(filename, mode)
#     except OSError as e:
#         print(e)
#         sys.exit(1)
#     else:
#         try:
#             yield f, None
#         finally:
#             f.close()

# INPUT FILE CHECK
## GENERAL
def check_tabulated_line(line, expected_col_number = 0, file_type = ''):
    """ Check whether the line is tab-sep and if the number of cols is ok
    @param line    line of a file (str)
    @param expected_col_number  the number of columns in this type of file (int)
    @param type    the type of file, used for print (str)
    """
    try:
        l_line = line.split('\t')
        line_first_col = l_line[1]
    except:
        raise IndexError(f'The {file_type} file should be tab-separated')
        sys.exit(1)
    if len(l_line) != expected_col_number:
        raise Exception(f'The {tyfile_typepe} file is expected to have {expected_col_number} columns')

def conv_string_to_boolean(str):
    if str in [True, False]:
        return str
    elif str in ['True', 'true', 'T', 'Yes', 'yes', 'Y']:
        boolean = True
    elif str in ['False', 'false', 'F', 'No', 'no', 'N']:
        boolean = False
    else:
        raise Exception(f'The string {str} does not match any accepted boolean format. \
            Please use any of "True", "true", "T", "Yes", "yes", "Y", "False", "false", "F", "No", "no", "N"')
    return boolean

## GFF
def check_gff3_format(gff_file, lines_to_check = 50):
    """ Check the gff file format
    @param gff_file    a TextIOWrapper of the gff3 file
    @param lines_to_check  the number of columns in this type of file (int)
    """
    first_line = gff_file.readline().strip()
    gff3_header = '##gff-version 3'
    if first_line != gff3_header:
        raise Exception('The gff3 file first line must be ##gff-version 3')
    # check first n rows
    for line in gff_file:
        if line.startswith('#'):
            continue
        cnt = 0
        if not cnt < lines_to_check:
            try:
                int(line.split('\t')[3])
                int(line.split('\t')[4])
            except ValueError:
                raise ValueError('The gff start and end columns'
                'must be integers')
            cnt += 1
    gff_file.seek(0)

## GENE_FAMILY
def check_trapid_gene_family_format(gf_file, lines_to_check = 15):
    """ Check the TRAPID gene family annotation file
    @param gf_file    a TextIOWrapper of the gene family file
    @param lines_to_check  the number of columns in this type of file (int)
    """
    first_line = gf_file.readline().strip()
    check_tabulated_line(first_line, 3, 'gene family')
    l_first_line = first_line.split('\t')
    # check header
    l_correct_header = [l_first_line[0] == '#counter',
        l_first_line[1] == 'transcript_id', l_first_line[2] == 'gf_id']
    if not all(l_correct_header):
        raise ValueError('''The gene family header does not follow the expected
            format of #counter\ttranscript_id\tgf_id''')
    # check first n rows
    for i in range(0, lines_to_check):
        line = next(gf_file).strip()
        try:
            int(line.split('\t')[0])
        except ValueError:
            raise ValueError('counter col is expected to be an integer')
    gf_file.seek(0)

## SPECIES INFO
def check_species_info_format(sp_info_file, lines_to_check = 15):
    """ Check the format of the custom species info file
    @param sp_info_file    a TextIOWrapper of the species info file
    @param lines_to_check  the number of columns in this type of file (int)
    """
    # TODO
    return None

# GFF PARSING
def get_rna_gene_dic(gff_file):
    """ Get a dic with rna_id:gene_id
    @param string    gff file

    @return  the dic
    """
    pattern_id = 'ID=(.+?)[;\n]'
    pattern_parent = 'Parent=(.+?)[;\n]'
    d = {}
    for line in gff_file:
        if (line.startswith('#') or len(line.strip()) == 0):
            continue
        else:
            row = line.split('\t')
            if row[2] != "mRNA":
                continue
            else:
                # extract gene id from last field of gff
                rna_id = re.search(pattern_id, row[-1]).group(1)
                gene_id = re.search(pattern_parent, row[-1]).group(1)
                d[rna_id] = gene_id
    return d

def ensure_df_col_format(df, cols):
    """ If df is empty, creates a valid empty df. Either way, the cols
    provided will be assigned as columns of the df
    @param dff    a pd dataframe
    @param cols   a list of columns

    @return  the original df or an empty one
    """
    if df.empty:
        df = pd.DataFrame(columns = cols)
    else:
        df.columns = cols
    return df

def melt_and_rename_relationships(df, d_col_to_rel_name, val_name, node_id):
    """ Returns the df melted for columns matching keys of d_col, with the value column
        renamed as val_name
    @param df  df with dataset_id, node_id, and columns in d_col_rel_name
    @param d_col_to_rel_name  dic with colnames as key and relationship names as value
    @param val_name name of the value column

    @return  a melted df according to d_col_rel_name
    """
    df_melt = df.melt(id_vars = ['dataset_id', node_id],
            value_vars=list(d_col_to_rel_name.keys()), var_name='relationship',
            value_name=val_name).drop_duplicates() #.apply(lambda x: x.str.strip() if x is not np.nan else x)
    df_melt = df_melt.applymap(lambda x: x.strip() if isinstance(x, str) else x)
    df_melt['relationship'] = df_melt['relationship'].map(d_col_to_rel_name)
    df_melt.replace('nan', np.nan, inplace=True)
    return df_melt

def create_node_onto_edges(df, d_onto, d_add_original_on_edge, node_id):
    """ Get edges between a node and ontology terms. If a node is annotated by several terms,
    the list of ontology terms is splitted by ';'.
    @param df    df of sample parents (cols: $node_id, onto_trait, onto_tissue, \
        onto_stage, onto_condition, onto_location)
    @param d_onto    dic of columns containing ontology terms to link with \
        parents as key and the name of the relationship as the value
    @param d_add_original_on_edge   dic of columns as key and True/False as values \
        stating whether orginal descriptions should be added on the edges towards ontology nodes
    @return    the df with node_id and associated ontology terms
    """
    df_onto = df.copy()
    des_original_desc = df.copy()

    for onto in d_onto.keys():
        # unlist annotations from ontologies
        df_onto[onto] = df_onto[onto].astype(str).str.split(';')
        df_onto = df_onto.explode(column=onto)
    df_onto_melt = melt_and_rename_relationships(df_onto, d_onto, 'term', node_id)
    df_onto_melt['term'] = df_onto_melt['term'].str.replace(':', '_')
    # create a list of cols for which we want the original terms to be properties
    # of the edge
    l_col_on_edge = [desc_type for desc_type, to_add in d_add_original_on_edge.items() if to_add]
    if l_col_on_edge:
        d_original_desc_to_col = {}
        for key, value in d_onto.items():
            desc_type = key.split('_')[-1]
            if desc_type in l_col_on_edge:
                d_original_desc_to_col[desc_type] = value

        df_original_desc_melt = melt_and_rename_relationships(des_original_desc,
            d_original_desc_to_col, 'original_desc', node_id)
        df_onto_melt = df_onto_melt.dropna().merge(df_original_desc_melt,
        how='left', on=['dataset_id', node_id, 'relationship'])
        df_onto_melt.columns = ['dataset_id', node_id, 'relationship', 'term', 'original_desc']
    else:
        df_onto_melt['original_desc'] = ''
    return df_onto_melt[[node_id, 'term', 'relationship', 'original_desc']]

def get_node_free_term_edges(df, node_id):
    df_onto = df[[node_id] + ['free_term', 'free_rel']]
    # split col content
    df_onto['free_term'] = df_onto['free_term'].astype(str).str.split(';')
    df_onto['free_rel'] = df_onto['free_rel'].astype(str).str.split(';')
    df_term = df_onto[[node_id, 'free_term']].explode(column='free_term')
    df_rel = df_onto[[node_id, 'free_rel']].explode(column='free_rel')
    # only one node column
    df_free = pd.concat([df_term, df_rel['free_rel']], axis=1).reset_index(drop=True).replace('nan', pd.NA).dropna()
    df_free = df_free.applymap(lambda x: x.strip() if isinstance(x, str) else x)
    df_free.columns = [node_id, 'term', 'rel']
    return df_free

## DATASETS
def get_dataset_status(status):
    """Get a string stating if a dataset is public or private
    @param status    the status
    @return    a properly formatted status string
    """
    if status == 'public':
        return 'Public'
    elif status == 'private':
        return 'Private'
    else:
        return 'Unknown_status'