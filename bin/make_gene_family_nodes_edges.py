#!/usr/bin/env python3

"""
01/03/22

From TRAPID gene families, get nodes and edges (connection gene to gene family).

Usage: python3 make_gene_family_nodes_edges.py -gene_family lcul_transcript_gf_4855_20220301_133610_b667987d50656c.zip -gff lcul.gff3 -output_nodes nodes_gene_family.csv -output_edges edges_gene_gene_family.csv
"""
import sys
import argparse
import re
import pandas as pd
from database_pipeline_functions import check_trapid_gene_family_format
from database_pipeline_functions import check_gff3_format
from database_pipeline_functions import get_rna_gene_dic
from database_pipeline_functions import ensure_df_col_format

# ARGUMENTS
parser = argparse.ArgumentParser(description = 'From TRAPID gene families, \
    get nodes and edges (connection gene to gene family)')
parser.add_argument('-gene_family', help = 'gene family file from TRAPID',
    required = True)
parser.add_argument('-gff', help = 'annotation file in gff format',
    required = True)
parser.add_argument('-output_nodes', help = 'gene family nodes in .csv',
    required = False, default = 'nodes_gene_family.csv')
parser.add_argument('-output_edges', help = 'genes to gene families in .csv',
    required = False, default = 'edges_gene_gene_family.csv')
args = parser.parse_args()

# FUNCTIONS
def get_gene_family_nodes(line, l):
    """ Add the gene family name to list l
    @param line    line of TRAPID gene fam file
    @param l       list containing elements for output writing

    @return  the list with additional element
    """
    row = line.strip().split('\t')
    # if transcript has family
    if len(row) > 2:
        gene_family = str(row[2].split('_')[1])
        l.append (
                {
                    'gene_family_id' : gene_family,
                    'source' : 'TRAPID',
                    'link_plaza' : 'https://bioinformatics.psb.ugent.be/plaza'\
                            '/versions/plaza_v4_5_dicots/gene_families/view/' +
                        gene_family,
                    ':LABEL' : 'GeneFamily;FunctionalAnnotation;Trapid'
                }
        )
    return l

def get_gene_gene_family_edges(line, l, d):
    """ Add gene family info to list l
    @param line    line of TRAPID gene fam file
    @param l       list containing elements for output writing
    @param d       dic with rna_id : gene for id conversion

    @return  the list with additional element
    """
    row = line.strip().split('\t')
    if len(row) > 2:
        gene_family = str(row[2].split('_')[1])
        if row[1] not in d.keys():
            return l
        l.append (
                    {
                        'gene_id' : d[row[1]],
                        'gene_family_id' : gene_family,
                        ':TYPE' : 'HAS_ANNOTATION'
                    }
            )
    return l

# MAIN

if __name__ == "__main__":
    with open(args.gff, 'r') as gff_file:
        check_gff3_format(gff_file)
        d_rna_gene = get_rna_gene_dic(gff_file)

    with open(args.gene_family, 'r') as gene_family_file:
        check_trapid_gene_family_format(gene_family_file)
        l_nodes = []
        l_edges = []
        for line in gene_family_file:
            if line.startswith('#'):
                continue
            l_nodes = get_gene_family_nodes(line, l_nodes)
            l_edges = get_gene_gene_family_edges(line, l_edges, d_rna_gene)

    df_nodes = pd.DataFrame(l_nodes)
    df_edges = pd.DataFrame(l_edges)

    # COLUMNS
    col_nodes = ['family_id:ID(gene_family-ID)', 'source:String',
        'link:String', ':LABEL']
    col_edges = [':START_ID(gene-ID)', ':END_ID(gene_family-ID)',
        ':TYPE']
    df_nodes = ensure_df_col_format(df_nodes, col_nodes)
    df_edges = ensure_df_col_format(df_edges, col_edges)

    # OUTPUTS
    # WRITE OUTPUTS
    df_nodes.to_csv(args.output_nodes, index = False)
    df_edges.to_csv(args.output_edges, index = False)
