#!/usr/bin/env python3

"""
22/03/24

From RNA-seq featureCounts tsv output, selects data of interest to create nodes and edges

Usage: python3 get_c-make_transcriptomics_counts_nodes_edges.py -go lcul_trapid_go.txt -gff lcul.gff3 -output_data data_file.data -output_map map_file.map
"""

import argparse
import re
from collections import defaultdict

import numpy as np
import pandas as pd
import warnings
# to hide pandas warnings
warnings.simplefilter(action='ignore', category=FutureWarning)
from scipy.stats import gmean, gstd, tstd

from database_pipeline_functions import ensure_df_col_format, create_node_onto_edges, get_node_free_term_edges, get_dataset_status, conv_string_to_boolean


# ARGUMENTS
parser = argparse.ArgumentParser(description='From RNA-seq featureCounts tsv \
    output, selects data of interest to create nodes and edges')

parser.add_argument('-mode', help='either "nf-core" or "featurecounts"',
    required=True)
####################nf-core/rnaseq###########################
parser.add_argument('-nf_salmon_tpm', help='salmon nf-core/rnaseq output \
called salmon.merged.gene_tpm.tsv. Requires -salmon_counts', required=False)
parser.add_argument('-nf_salmon_counts', help='salmon nf-core/rnaseq output \
salmon.merged.gene_counts.tsv. Requires -salmon_tpm', required=False)
parser.add_argument('-nf_metadata', help='nf-core/fetchngs output samplesheet.csv',
    required=False)
parser.add_argument('-metadata_annotation',
    help='annotation of samples with ontology terms in csv format. \
    Required columns are condition_id, onto_tissue, onto_stage, onto_condition, \
    onto_location, free_term, free_rel. \
    All columns can contain multiple terms with semi-colon sep.', required=False)
parser.add_argument('-dataset',
    help='input dataset information on the project (supervisor, doi etc.)',
    required=True)
parser.add_argument('-status',
    help='status of the dataset, either public or private',
    required=True)
parser.add_argument('-bioinfo_protocol',
    help='input all bioinfo processing steps realized on the dataset',
    required=True)
parser.add_argument('-norm', help='normalization method to use.',
    required=False, default='TPM')
parser.add_argument('-tpm_threshold',
    help='minimal number of counts for a gene to be included \
    (sum of TPM in all samples)', required=False, default=5)
parser.add_argument('-filter_genes_zero_counts',
    help='should gene nodes be connected to sample and condition nodes if \
        their expression is 0', required=False, default=True)
#######################output################################
parser.add_argument('-output_nodes_bioinfo_protocol',
    help='nodes of bioinfo protocol', required=False, default='dataset_nodes.csv')
parser.add_argument('-output_nodes_dataset',
    help='rnaseq dataset nodes in csv format', required=False,
    default='dataset_nodes.csv')
parser.add_argument('-output_nodes_condition',
    help='rnaseq condition nodes in csv format', required=False,
    default='condition_nodes.csv')
parser.add_argument('-output_nodes_free_term',
    help='free desc term nodes in csv format', required=False,
    default='free_term_nodes.csv')
parser.add_argument('-output_nodes_sample',
    help='rnaseq sample replicates nodes in csv format', required=False,
    default='samples_nodes.csv')
parser.add_argument('-output_edges_dataset_bioinfo_protocol',
    help='dataset to bioinfo protocol in csv format', required=False,
    default='sample_dataset_edges.csv')
parser.add_argument('-output_edges_condition_dataset',
    help='samples to dataset in csv format', required=False,
    default='sample_dataset_edges.csv')
parser.add_argument('-output_edges_sample_condition',
    help='sample nodes to condition nodes in csv format', required=False,
    default='sample_condition_edges.csv')
parser.add_argument('-output_edges_gene_sample',
    help='genes to rnaseq samples rep in csv format', required=False,
    default='gene_sample_edges.csv')
parser.add_argument('-output_edges_gene_condition',
    help='gene to conditions in csv format', required=False,
    default='gene_condition_edges.csv')
parser.add_argument('-output_edges_condition_onto',
    help='rnaseq condition to ontology node in csv format', required=False,
    default='condition_onto_edges.csv')
parser.add_argument('-output_edges_condition_free_term',
    help='rnaseq condition to free desc term node in csv format', required=False,
    default='condition_free_term_edges.csv')
args = parser.parse_args()

# FUNCTIONS

def filter_counts(df, threshold):
    """ remove rows where row sum < threshold
    @param df    df of counts with genes as rows and samples as columns
    @param threshold    minimal number of counts for each gene

    @return  the filtered df
    """
    df_filtered = df.loc[df.sum(axis = 1) >= int(threshold)]
    print('Used threshold of:', threshold, '\nFiltering removed', df.shape[0] - df_filtered.shape[0], 'genes')
    return df_filtered

def add_raw_counts(df_norm, df_ftc):
    """ add raw counts to the dataframe
    @param df_norm    the normalized dataframe (only columns of normalized counts)
    @param df_ftc    dataframe of featureCounts

    @return    a df with normalized and raw counts
    """
    df_norm_m = df_norm.melt(ignore_index=False)
    df_norm_m.reset_index(inplace=True)
    df_ftc_m = df_ftc.drop('Length', axis=1).melt(ignore_index=False)
    df_ftc_m.reset_index(inplace=True)

    # merge on indexed gene id
    df_merged = df_norm_m.merge(df_ftc_m, how='left', on=['Geneid', 'variable'])
    df_merged.set_index('Geneid', inplace=True)
    df_merged.columns = ['sample', 'normalized_counts', 'counts']
    return df_merged

def get_metadata_condition(df):
    """ get samples average
    @param df    df with metadata

    @return    the df with sample parents and counts of replicates
    """
    df_metadata_rep_parent = df.copy()
    # get parent id
    df_metadata_rep_parent.set_index('condition_id', inplace=True)
    # nb of rep per parent
    df_metadata_rep_parent['replicate_count'] = df_metadata_rep_parent.groupby('condition_id').size()
    df_return = df.merge(df_metadata_rep_parent, how='left', left_on='condition_id', right_index = True)
    df_return.reset_index(inplace=True)
    df_return = df_return[['sample', 'condition_id', 'replicate_count',
    'onto_tissue_y', 'onto_stage_y', 'onto_condition_y', 'onto_location_y', 'free_term_y', 'free_rel_y']]
    df_return.columns = ['sample', 'condition', 'replicate_count', 'onto_tissue',
    'onto_stage', 'onto_condition', 'onto_location', 'free_term', 'free_rel']
    df_return.drop_duplicates(inplace=True)
    return df_return

def filter_gene_sample_edges(df):
    """ remove edges if gene count for the sample is 0
    @param df    df of tpm FROM featureCount
    @return    the filtered df
    """
    df_return = df[df['counts'] > 0]
    return df_return

def get_gene_condition_edges_nf(df, df_global_sample):
    """ get samples average
    @param df    df of tpm
    @param df_global_sample    df with global sample (of rep)
    @return    the df with geom mean of counts and sd
    """
    # smallest value to replace 0 (to avoid log(0))
    small_float = np.finfo(float).eps
    df_counts = df.copy()
    df_counts['normalized_counts'] = df_counts['tpm'].replace(0, small_float)
    df_merged = df_counts.merge(df_global_sample, how='left', on='sample')

    df_sample_per_condition = df_merged.groupby(['gene_id', 'condition'])['counts'].count().reset_index()
    df_one_sample_per_condition = df_sample_per_condition[df_sample_per_condition['counts'] == 1]['condition'].drop_duplicates()
    df_sev_sample_per_condition = df_sample_per_condition[df_sample_per_condition['counts'] > 1]['condition'].drop_duplicates()

    df_merged_one = df_merged[df_merged['condition'].isin(df_one_sample_per_condition)]
    df_merged_sev = df_merged[df_merged['condition'].isin(df_sev_sample_per_condition)]

    # for condition where several samples, can perform mean and sd
    df_return_sev = pd.DataFrame(columns = ['gene_id', 'condition', 'geom_mean', 'geom_sd', 'arith_mean', 'arith_sd'])
    if len(df_merged_sev) > 0:
        # geometric mean and gsd
        df_geom_mean = df_merged_sev.groupby(['gene_id', 'condition'])['normalized_counts'].apply(gmean).round(decimals=2).reset_index()
        df_geom_mean.rename(columns = {'normalized_counts': 'geom_mean'}, inplace=True)
        df_gstd = df_merged_sev.groupby(['gene_id', 'condition'])['normalized_counts'].apply(gstd).round(decimals=2).reset_index()
        df_gstd.rename(columns = {'normalized_counts': 'geom_sd'}, inplace=True)
        # arithmetic mean and sd
        df_arith_mean = df_merged_sev.groupby(['gene_id', 'condition'])['normalized_counts'].mean().round(decimals=2).reset_index()
        df_arith_mean.rename(columns = { 'normalized_counts': 'arith_mean'}, inplace=True)
        df_tstd = df_merged_sev.groupby(['gene_id', 'condition'])['normalized_counts'].apply(tstd).round(decimals=2).reset_index()
        df_tstd.rename(columns = {'normalized_counts': 'arith_sd'}, inplace=True)
        # merge dfs
        df_geom = df_geom_mean.merge(df_gstd, how='left', on=['gene_id', 'condition'])
        df_arith = df_arith_mean.merge(df_tstd, how='left', on=['gene_id', 'condition'])
        df_return_sev = df_geom.merge(df_arith, how='left', on=['gene_id', 'condition'])

    # for condition where one sample
    df_merged_one['geom_mean'] = df_merged_one['normalized_counts']
    # gsd is a coef
    df_merged_one['geom_sd'] = 1
    df_merged_one['arith_mean'] = df_merged_one['normalized_counts']
    df_merged_one['arith_sd'] = 0

    df_return_one = df_merged_one[['gene_id', 'condition', 'geom_mean', 'geom_sd', 'arith_mean', 'arith_sd']]

    df_return = pd.concat([df_return_one, df_return_sev])
    df_return['method'] = 'TPM'
    return df_return

def filter_gene_condition_edges(df):
    """ remove edges if mean expr of the gene in the condition is 0
    @param df    df of tpm FROM featureCount
    @return    the filtered df
    """
    df_return = df[(df['arith_mean'] > 0) & (df['geom_mean'] > 0)]
    return df_return

def parse_samplesheet_neo4j_names(df, d):
    """
    @param df    df of metadata
    @param d    dic matching colnames with neo4j names
    @return    the df with new neo4j colnames
    """
    colnames = df.columns
    l_cols = [i for i in colnames if i in d.keys()]
    l_neo4j_cols = [d[i] for i in l_cols]
    df_out = df.copy()
    df_out = df_out[l_cols]
    df_out.columns = l_neo4j_cols
    return df_out

def get_dataset_bioinfo_protocol_edges(df_protocol, df_dataset):
    """ create edges between conditions and the protocol
    @param df_protocol    df of protocol (should be one non-header row)
    @param df_dataset    df of all conditions
    @return    the edges between protocol node and conditions in neo4j format
    """
    bioinfo_protocol_id = df_protocol.iloc[0,0]
    df_return = df_dataset.iloc[:,0].to_frame()
    df_return['end_id'] = bioinfo_protocol_id
    return df_return

# MAIN
if __name__ == "__main__":

    df_bioinfo_protocol = pd.read_csv(args.bioinfo_protocol, sep=',', comment='#',
        skipinitialspace=True, header=0)
    # SALMON (slm)
    # counts
    df_slm_counts = pd.read_csv(args.nf_salmon_counts, sep='\t', header=0,
        comment='#', skipinitialspace=True)
    df_slm_counts.drop('gene_name', axis=1, inplace=True)
    df_slm_counts_m = df_slm_counts.melt(id_vars='gene_id', var_name='sample',
        value_name='counts')
    # tpm
    df_slm_tpm = pd.read_csv(args.nf_salmon_tpm, sep='\t', header=0,
        comment='#', skipinitialspace=True)
    df_slm_tpm.drop('gene_name', axis=1, inplace=True)
    df_slm_tpm.set_index('gene_id', inplace=True)
    ## filtering counts
    df_slm_tpm = filter_counts(df_slm_tpm, args.tpm_threshold)
    df_slm_tpm.reset_index(inplace=True)
    df_slm_tpm_m = df_slm_tpm.melt(id_vars='gene_id', var_name='sample',
        value_name='tpm')
    # merge
    df_slm_merge = df_slm_tpm_m.merge(df_slm_counts_m, how='left',
        on=['gene_id', 'sample'])
    # metadata
    dataset_status = get_dataset_status(args.status)
    df_samplesheet = pd.read_csv(args.nf_metadata, sep=',', header=0,
        comment='#', skipinitialspace=True)
    l_onto_cols = ['dataset_id', 'sample', 'condition_id', 'tissue',
        'onto_tissue', 'stage', 'onto_stage', 'condition', 'onto_condition',
        'location', 'onto_location', 'free_term', 'free_rel']
    df_onto = pd.read_csv(args.metadata_annotation, sep=',', header=0,
        comment='#', skipinitialspace=True, usecols=l_onto_cols)
    df_metadata = df_samplesheet.merge(df_onto, how='left', on='sample')
    # to filter datasets
    dataset_id = df_metadata['study_accession'][0]

    ## columns of metadata file
    l_onto = ['onto_tissue', 'onto_stage', 'onto_condition', 'onto_location']
    # first element is the term, the second is the relationship name
    l_free_term = ['free_term', 'free_rel']
    l_all_terms = l_onto + l_free_term

    d_samplesheet_neo4j = {'sample': 'sample',
        'strandedness': 'strandedness:String',
        'library_layout': 'library_layout:String',
        'instrument_model': 'instrument_model:String',
        'instrument_platform': 'instrument_platform:String',
        'read_count': 'read_count:long',
        'experiment_title': 'experiment_title:String',
        'sample_title': 'sample_title:String'}

    # switch id in salmon df
    df_edges_gene_sample = df_slm_merge[['gene_id', 'sample', 'tpm', 'counts']]
    # remove edges if gene expression is 0
    if conv_string_to_boolean(args.filter_genes_zero_counts):
        df_edges_gene_sample = filter_gene_sample_edges(df_edges_gene_sample)
    df_edges_gene_sample['method'] = str(args.norm)
    df_edges_gene_sample.rename(columns = {'tpm': 'normalized_counts'},
        inplace=True)
    df_edges_gene_sample.set_index('gene_id', inplace=True)
    df_metadata.set_index('sample', inplace=True)

    # get sample nodes with counts of rep
    df_metadata_condition = get_metadata_condition(df_metadata)
    df_nodes_condition = df_metadata_condition[['condition', 'replicate_count'] + l_all_terms].drop_duplicates()

    d_onto = {'onto_tissue': 'IS_TISSUE',
        'onto_stage': 'HAS_DEV_STAGE',
        'onto_condition': 'SUBJECTED_TO',
        'onto_location': 'HAS_LOCATION'}
    d_onto_original_on_edge = {'tissue': True,
        'stage': True,
        'condition': True,
        'location': False}

    df_edges_condition_onto = create_node_onto_edges(df_onto, d_onto, d_onto_original_on_edge, 'condition_id')
    df_nodes_condition_all = df_nodes_condition[['condition', 'replicate_count']]
    # deal with free field for terms
    df_edges_condition_free_term = get_node_free_term_edges(df_onto, 'condition_id').drop_duplicates()
    # need to create nodes for these homemade terms
    df_nodes_free_terms = df_edges_condition_free_term[['term']].drop_duplicates().replace('nan', pd.NA).dropna()

    # get sample parents in the same file as replicates (just no value for properties)
    # edges replicate to parent
    df_edges_sample_condition = df_metadata_condition.copy()
    df_edges_sample_condition = df_edges_sample_condition[['sample', 'condition']]
    df_metadata.drop(l_onto, axis=1, inplace=True)
    df_nodes_sample = parse_samplesheet_neo4j_names(df_metadata.reset_index(), d_samplesheet_neo4j)
    df_edges_gene_condition = get_gene_condition_edges_nf(df_slm_merge, df_edges_sample_condition)
    if conv_string_to_boolean(args.filter_genes_zero_counts):
        df_edges_gene_condition = filter_gene_condition_edges(df_edges_gene_condition)
    # bioinfo processing nodes
    df_nodes_bioinfo_protocol = df_bioinfo_protocol.copy()

    # WRITE FILES
    ## nodes sample rep
    df_nodes_sample.rename(columns={'sample': 'sample_id:ID(rnaseq_sample-ID)'},
        inplace=True)
    df_nodes_sample[':LABEL'] = 'RNASeq;Sample' + ';' + dataset_status
    df_nodes_sample.to_csv(args.output_nodes_sample, index=False)
    ## nodes condition
    df_nodes_condition = df_nodes_condition_all.copy()
    df_nodes_condition.columns = ['condition_id:ID(rnaseq_condition-ID)', 'replicate_count:long']
    df_nodes_condition[':LABEL'] = 'RNASeq;Condition' + ';' + dataset_status
    df_nodes_condition.to_csv(args.output_nodes_condition, index=False)
    ## nodes free term
    col_free_term_nodes = ['label:ID(rnaseq_free_term-ID)']
    df_nodes_free_terms = ensure_df_col_format(df_nodes_free_terms,
        col_free_term_nodes)
    df_nodes_free_terms[':LABEL'] = 'Free'
    df_nodes_free_terms.to_csv(args.output_nodes_free_term, index=False)
    ## nodes dataset
    df_nodes_dataset = pd.read_csv(args.dataset, sep=',', comment='#',
        skipinitialspace=True)
    df_nodes_dataset = df_nodes_dataset[["dataset_id", "author", "year", "doi",
        "genera", "species", "title"]]
    df_nodes_dataset = df_nodes_dataset[df_nodes_dataset.iloc[:,0] == dataset_id]
    df_nodes_dataset_out = df_nodes_dataset.copy()
    col_dataset_nodes = ['dataset_id:ID(rnaseq_dataset-ID)', 'author:String',
        'year:int', 'doi:String', 'genera:String', 'species:String', 'title:String']
    df_nodes_dataset_out = ensure_df_col_format(df_nodes_dataset_out,
        col_dataset_nodes)
    df_nodes_dataset_out[':LABEL'] = 'RNASeq;Dataset' + ';' + dataset_status
    df_nodes_dataset_out.to_csv(args.output_nodes_dataset, index=False)
    ## nodes bioinfo protocol
    col_bioinfo_protocol = ['protocol_id:ID(bioinfo_protocol-ID)',
    'metadata_collection:String', 'metadata_collection_v:String',
    'rnaseq_processing:String', 'rnaseq_processing_v:String', 'mapping:String',
    'mapping_v:String', 'gene_aggregation:String']
    df_nodes_bioinfo_protocol = ensure_df_col_format(df_bioinfo_protocol,
        col_bioinfo_protocol)
    df_nodes_bioinfo_protocol[':LABEL'] = 'Protocol;RNASeq'
    df_nodes_bioinfo_protocol.to_csv(args.output_nodes_bioinfo_protocol,
        index=False)

    ## edges gene sample
    df_edges_gene_sample['gene_id'] = df_edges_gene_sample.index
    df_edges_gene_sample = df_edges_gene_sample[['gene_id', 'sample', 'normalized_counts', 'counts', 'method']]
    col_gene_sample_edges = [':START_ID(gene-ID)',
        ':END_ID(rnaseq_sample-ID)', 'normalized_count:float', 'count:float',
        'normalization_method:String']
    df_edges_gene_sample = ensure_df_col_format(df_edges_gene_sample,
        col_gene_sample_edges)
    df_edges_gene_sample[':TYPE'] = 'IS_EXPRESSED'
    df_edges_gene_sample.to_csv(args.output_edges_gene_sample, index=False)
    ## edges gene condition
    col_gene_condition_edges = [':START_ID(gene-ID)',
        ':END_ID(rnaseq_condition-ID)', 'geom_mean:float', 'geom_sd:float',
        'arith_mean:float', 'arith_sd:float', 'normalization_method:String']
    df_edges_gene_condition = ensure_df_col_format(df_edges_gene_condition,
        col_gene_condition_edges)
    df_edges_gene_condition[':TYPE'] = 'IS_EXPRESSED'
    df_edges_gene_condition.to_csv(args.output_edges_gene_condition, index=False)
    ## edges sample condition
    col_sample_condition_edges = [':START_ID(rnaseq_sample-ID)',
        ':END_ID(rnaseq_condition-ID)']
    df_edges_sample_condition = ensure_df_col_format(df_edges_sample_condition,
        col_sample_condition_edges)
    df_edges_sample_condition[':TYPE'] = 'BELONGS_TO'
    df_edges_sample_condition.to_csv(args.output_edges_sample_condition,
        index=False)
    ## edges condition dataset
    df_edges_condition_dataset = df_nodes_condition_all.copy()
    df_edges_condition_dataset.iloc[:,1] = dataset_id
    col_condition_dataset_edges = [':START_ID(rnaseq_condition-ID)',
        ':END_ID(rnaseq_dataset-ID)']
    df_edges_condition_dataset = ensure_df_col_format(df_edges_condition_dataset,
        col_condition_dataset_edges)
    df_edges_condition_dataset[':TYPE'] = 'PART_OF'
    df_edges_condition_dataset.to_csv(args.output_edges_condition_dataset,
        index=False)
    ## edges condition onto
    col_condition_onto_edges = [':START_ID(rnaseq_condition-ID)',
        ':END_ID', ':TYPE', 'original_desc:String']
    df_edges_condition_onto = ensure_df_col_format(df_edges_condition_onto,
        col_condition_onto_edges)
    df_edges_condition_onto.to_csv(args.output_edges_condition_onto, index=False)
    ## edges condition free term
    col_condition_free_term_edges = [':START_ID(rnaseq_condition-ID)',
        ':END_ID(rnaseq_free_term-ID)', ':TYPE']
    df_edges_condition_free_term = ensure_df_col_format(df_edges_condition_free_term,
        col_condition_free_term_edges)
    df_edges_condition_free_term.to_csv(args.output_edges_condition_free_term,
        index=False)
    ## edges dataset bioinfo processing
    df_edges_dataset_bioinfo_protocol = get_dataset_bioinfo_protocol_edges(df_bioinfo_protocol, df_nodes_dataset)
    col_dataset_bioinfo_protocol_edges = [':START_ID(rnaseq_dataset-ID)',
        ':END_ID(bioinfo_protocol-ID)']
    df_edges_dataset_bioinfo_protocol = ensure_df_col_format(df_edges_dataset_bioinfo_protocol,
        col_dataset_bioinfo_protocol_edges)
    df_edges_dataset_bioinfo_protocol[':TYPE'] = 'PROCESSED_WITH'
    df_edges_dataset_bioinfo_protocol.to_csv(args.output_edges_dataset_bioinfo_protocol,
        index=False)
