#!/usr/bin/env python3

"""
01/03/22

From TRAPID rna families, get nodes and edges (connection rna to rna family).

Usage: python3 make_rna_family_nodes_edges.py -rna_family lcul_transcript_gf_4855_20220301_133610_b667987d50656c.zip -gff lcul.gff3 -output_nodes nodes_rna_family.csv -output_edges edges_rna_rna_family.csv
"""

import argparse
import pandas as pd

from database_pipeline_functions import ensure_df_col_format

# ARGUMENTS
parser = argparse.ArgumentParser(description='From TRAPID rna families, get nodes and edges (connection rna to rna family).')
parser.add_argument('-rna_family', help='rna family file from TRAPID', required=True)
parser.add_argument('-output_nodes', help='rna family nodes in csv format', required=False, default='nodes_rna_family.csv')
parser.add_argument('-output_edges', help='rnas to rna families in csv format', required=False, default='edges_rna_rna_family.csv')
args = parser.parse_args()

# FUNCTIONS

def get_rna_family_nodes(line, l, s):
    """ Add the rna family name to list l
    @param line    line of TRAPID rna fam file
    @param l       list containing elements for output writing
    @param s       set of rna families

    @return  the list with additional element
    """
    row = line.strip().split('\t')
    # if transcript has family
    if len(row) > 2:
        rna_family = str(row[2].split('_')[1])
        if not rna_family in s:
            s.add(rna_family)
            l.append (
                    {
                        'rna_family_id' : rna_family,
                        'source' : 'TRAPID',
                        'link_rfam' : 'https://rfam.xfam.org/family/' + rna_family,
                        ':LABEL' : 'FunctionalAnnotation;RNAFamily;Trapid'
                    }
            )
    return l

def get_rna_rna_family_edges(line, l):
    """ Add rna family info to list l
    @param line    line of TRAPID rna fam file
    @param l       list containing elements for output writing

    @return  the list with additional element
    """
    row = line.strip().split('\t')
    if len(row) > 2:
        rna_family = str(row[2].split('_')[1])
        l.append (
                    {
                        'rna_id' : row[1],
                        'rna_family_id' : rna_family,
                        ':TYPE' : 'HAS_ANNOTATION'
                    }
        )
    return l

# MAIN

if __name__ == "__main__":

    l_nodes = []
    l_edges = []

    with open(args.rna_family, 'r', encoding = 'utf-8') as fl:
        s_ids = set()
        for line in fl:
            if line.startswith('#'):
                continue
            l_nodes = get_rna_family_nodes(line, l_nodes, s_ids)
            l_edges = get_rna_rna_family_edges(line, l_edges)

    df_nodes = pd.DataFrame(l_nodes)
    df_edges = pd.DataFrame(l_edges)

    # COLUMNS
    col_nodes = ['family_id:ID(rna_family-ID)', 'source:String', 'link:String', ':LABEL']
    col_edges = [':START_ID(rna-ID)', ':END_ID(rna_family-ID)', ':TYPE']
    df_nodes = ensure_df_col_format(df_nodes, col_nodes)
    df_edges = ensure_df_col_format(df_edges, col_edges)

    # OUTPUTS
    # WRITE OUTPUTS
    df_nodes.to_csv(args.output_nodes, index = False)
    df_edges.to_csv(args.output_edges, index = False)
