#!/usr/bin/env python3

# conda activate alignmentAnalysis
"""
12/03/21
Create species, accession, genome and gene nodes from gff and also using
Emapper output for annotation

Usage: TODO
"""

import argparse
import re
import pandas as pd

from database_pipeline_functions import check_gff3_format
from database_pipeline_functions import get_rna_gene_dic
from database_pipeline_functions import ensure_df_col_format

# ARGPARSE
parser = argparse.ArgumentParser(description='Parse gff and emapper to get '
    'gene nodes for neo4j')
parser.add_argument('-gff', help='gff file for the species', required=True)
parser.add_argument('-fai', help='fai files generated with samtools faidx\
    for the genome file of the species', required=True)
parser.add_argument('-emappannot', help='annotation file from emapper',
    required=True)
parser.add_argument('-sp_info', help='tsv file containing info on species',
    required=True)
parser.add_argument('-chr_conv_table',
    help='two-cols csv file with old to new chr format conversion',
    required=False)
parser.add_argument('-species',
    help='species acronym used in the tsv file containing info on species',
    required=True)
parser.add_argument('-output_nodes_gene',
    help='output gene nodes in csv format', required=False,
    default='gene_nodes.csv')
parser.add_argument('-output_nodes_chromosome',
    help='output chromosome nodes in csv format', required=False,
    default='chromosome_nodes.csv')
parser.add_argument('-output_nodes_genome',
    help='output annotation nodes in csv format', required=False,
    default='genome_nodes.csv')
parser.add_argument('-output_nodes_accession',
    help='output accession nodes in csv format', required=False,
    default='accession_nodes.csv')
parser.add_argument('-output_nodes_species',
    help='output species nodes in csv format', required=False,
    default='species_nodes.csv')
parser.add_argument('-output_edges_gene_chromosome',
    help='output gene to chromosome edges in csv format', required=False,
    default='gene_chromosome_edges.csv')
parser.add_argument('-output_edges_chromosome_genome',
    help='output chromosome to genome edges in csv format', required=False,
    default='chromosome_genome_edges.csv')
parser.add_argument('-output_edges_genome_accession',
    help='output genome to accession edges in csv format',
    required=False, default='genome_accession_edges.csv')
parser.add_argument('-output_edges_accession_species',
    help='output accession to species edges in csv format', required=False,
    default='accession_species_edges.csv')


args = parser.parse_args()

# FUNCTIONS
def read_fai(fai):
    """Reads an opened fai file to produce a pd DataFrame
    @param fai  fai file

    @return  a pd DataFrame
    """
    df = pd.read_csv(fai, sep='\t', header=None)
    return df

def make_rna_gene_dic(merged_gff):
    """ Creates a dic with parent and feature id
    @param merged_gff_file  path of the merged gff file

    @return  create a dic with rna_id as key and gene_id as value
    """
    pattern_id = 'ID=(.+?);'
    pattern_parent_id = 'Parent=(.+?)[;\n]'
    d_rna_gene = {}
    with open(merged_gff, 'r', encoding='utf-8') as fl:
        for row in fl:
                if (row.startswith('#') or len(row.strip()) == 0):
                    continue
                elif row.split('\t')[2] != "mRNA":
                    continue
                else:
                    m_id = re.search(pattern_id, row)
                    m_parent_id = re.search(pattern_parent_id, row)
                    if m_id and m_parent_id:
                        rna_id = m_id.group(1)
                        gene_id = m_parent_id.group(1)
                        d_rna_gene[rna_id] = gene_id
                    else:
                        print("Missing  \n", row)
                        print("Continue")
                        continue
    return d_rna_gene

def get_gff_gene_info(gff_file):
    """ From gff file get a pd dataframe with gene info
    @param gff_file  gff file with only

    @return  pd df of genes
    """
    pattern = 'ID=(.+?)[;\n]'
    l = []
    with open(gff_file, 'r', encoding='utf-8') as fl:
        for line in fl:
                if (line.startswith('#') or len(line.strip()) == 0):
                    continue
                else:
                    row = line.split('\t')
                    if row[2] != 'gene':
                        continue
                    else:
                        # extract gene id from last field of gff
                        gene_id = re.search(pattern, row[-1]).group(1)
                        l.append(
                            {
                                'gene_id': gene_id,
                                'chromosome': row[0],
                                'start': row[3],
                                'end':  row[4],
                                'strand': row[6]
                            }
                        )
    df_gff = pd.DataFrame(l)
    return df_gff

def add_old_chr_name(df_gff, df_conv_table):
    """ Add an old chromosome or scaffold name column to df_gff
    @param gff_file  gff file with only
    @param df_conv_table  df with old and new chr names

    @return  all relevant data as a df
    """
    df_output = pd.merge(df_gff, df_conv_table, how= 'left', left_on = 'chromosome', right_on = 'new_chr')
    del df_output['new_chr']
    return df_output

def merge_gff_spinfo(df_gff, df_info, sp):
    """ From gff file and sp_info_file, creates a dataframe with all information for nodes
    @param df_gff  pd df of genes
    @param df_info   pd df of species info
    @param sp  species of the gff file considered

    @return  all relevant data as a df
    """
    df_gff['genome_id'] = df_info.loc[sp, 'genome_id']
    return df_gff

if __name__ == "__main__":

    df_info = pd.read_csv(args.sp_info, sep='\t', comment='#', header=0, index_col=0)
    df_info['genome_id'] = df_info.index

    # get chromosome size
    with open(args.fai, 'r', encoding='utf-8') as fai_file:
        df_fai = read_fai(fai_file)
    df_chr_size = df_fai[[0, 1]]
    df_chr_size.columns = ['chromosome', 'size']
    with open(args.gff, 'r', encoding='utf-8') as gff_file:
        check_gff3_format(gff_file)
        d_gff = get_rna_gene_dic(gff_file)

    # get gff dataframe, add old chr names and species information
    df_gff = get_gff_gene_info(args.gff)
    if args.chr_conv_table:
        df_chr_conv = pd.read_csv(args.chr_conv_table, sep=',', comment='#', names=['old_chr', 'new_chr'])
        df_gff_old = add_old_chr_name(df_gff, df_chr_conv)
    else:
        df_gff_old = df_gff
    df_gene = merge_gff_spinfo(df_gff_old, df_info, str(args.species))

    # selecting a subset of columns to spare some mem
    columns = ['query', 'seed_ortholog', 'evalue', 'score',\
    'eggNOG_OGs', 'max_annot_lvl', 'COG_category', 'Description', 'Preferred_name', 'GOs', 'EC', 'KEGG_ko', 'KEGG_Pathway',\
    'KEGG_Module', 'KEGG_Reaction', 'KEGG_rclass', 'BRITE', 'KEGG_TC', 'CAZy', 'BIGG_Reaction', 'PFAMs']
    df_emapper = pd.read_csv(args.emappannot, sep='\t', comment='#', names=columns)
    df_emapper = df_emapper[['query','Description', 'Preferred_name']]
    df_emapper['Description'] = df_emapper['Description'].map(lambda x: re.sub(r'^-$', '', x))
    df_emapper['Preferred_name'] = df_emapper['Preferred_name'].map(lambda x: re.sub(r'^-$', '', x))
    # removing .1 or any suffix (emapper was run with proteins)
    df_emapper['query'] = df_emapper['query'].map(lambda x: d_gff[x])

    # left join on output df
    df_gene_final = pd.merge(df_gene, df_emapper, how='left', left_on='gene_id', right_on='query')
    del df_gene_final['query']
    df_gene_final.fillna('', inplace = True)

    # nodes gene
    df_gene_nodes = df_gene_final.copy()
    # nodes chromosome
    df_chromosome_nodes = df_gene_final[['chromosome', 'old_chr']].drop_duplicates()
    df_chromosome_nodes = df_chromosome_nodes.merge(df_chr_size, how='left', on='chromosome')
    # nodes genome
    # must have a list as index if want to keep DataFrame structure
    df_genome_nodes = df_info.loc[[args.species], ['genome_id', 'assembly_v', 'annotation_v', 'source']]
    # nodes accession
    df_accession_nodes = df_info.loc[[args.species], ['accession']]
    # nodes species
    df_species_nodes = df_info.loc[[args.species], ['ncbi_taxon_id', 'genus', 'species']]

    # edges gene to chromosome
    df_gene_chromosome_edges = df_gene_final.loc[:, ['gene_id', 'chromosome', 'start', 'end']]
    # edges chromosome to genome
    df_chromosome_genome_edges = df_gene_final.loc[:, ['chromosome', 'genome_id']].drop_duplicates()
    # edges genome to accession
    df_genome_accession_edges = df_info.loc[[args.species], ['genome_id', 'accession']]
    # edges accession to species
    df_accession_species_edges = df_info.loc[[args.species], ['accession', 'ncbi_taxon_id']]

    # OUTPUT
    ## nodes
    df_gene_nodes[':LABEL'] = 'Gene'
    if args.chr_conv_table:
        output_colnames = ['gene_id:ID(gene-ID)', 'chromosome:String', 'start:long', 'end:long', 'strand:String',
    'old_chr:String', 'genome:String', 'eggNOG_desc:String', 'eggNOG_symbol:String', ':LABEL']
    else:
        output_colnames = ['gene_id:ID(gene-ID)', 'chromosome:String', 'start:long', 'end:long', 'strand:String',
    'genome:String', 'eggNOG_desc:String', 'eggNOG_symbol:String', ':LABEL']
    df_gene_nodes.to_csv(args.output_nodes_gene, index=False, header=output_colnames)

    df_chromosome_nodes[':LABEL'] = 'Chromosome'
    output_colnames =  ['chromosome_id:ID(chromosome-ID)', 'original_name:String', 'size:String', ':LABEL']
    df_chromosome_nodes = ensure_df_col_format(df_chromosome_nodes, output_colnames)
    df_chromosome_nodes.to_csv(args.output_nodes_chromosome, index=False, header=output_colnames)

    df_genome_nodes[':LABEL'] = 'Genome'
    output_colnames =  ['genome_id:ID(genome-ID)', 'assembly_v:String', 'annotation_v:String', 'source:String', ':LABEL']
    df_genome_nodes = ensure_df_col_format(df_genome_nodes, output_colnames)
    df_genome_nodes.to_csv(args.output_nodes_genome, index=False, header=output_colnames)

    df_accession_nodes[':LABEL'] = 'Accession'
    output_colnames =  ['accession_id:ID(accession-ID)', ':LABEL']
    df_accession_nodes = ensure_df_col_format(df_accession_nodes, output_colnames)
    df_accession_nodes.to_csv(args.output_nodes_accession, index=False, header=output_colnames)

    df_species_nodes[':LABEL'] = 'Species'
    output_colnames =  ['ncbi_taxon_id:ID(species-ID)', 'genus:String', 'species:String', ':LABEL']
    df_species_nodes = ensure_df_col_format(df_species_nodes, output_colnames)
    df_species_nodes.to_csv(args.output_nodes_species, index=False, header=output_colnames)

    ## edges
    df_gene_chromosome_edges[':TYPE'] = 'LOCATED_ON'
    output_colnames =  [':START_ID(gene-ID)', ':END_ID(chromosome-ID)', 'start:long', 'end:long', ':TYPE']
    df_gene_chromosome_edges = ensure_df_col_format(df_gene_chromosome_edges, output_colnames)
    df_gene_chromosome_edges.to_csv(args.output_edges_gene_chromosome, index=False, header=output_colnames)

    df_chromosome_genome_edges[':TYPE'] = 'SUBSET_OF'
    output_colnames =  [':START_ID(chromosome-ID)', ':END_ID(genome-ID)', ':TYPE']
    df_chromosome_genome_edges = ensure_df_col_format(df_chromosome_genome_edges, output_colnames)
    df_chromosome_genome_edges.to_csv(args.output_edges_chromosome_genome, index=False, header=output_colnames)

    df_genome_accession_edges[':TYPE'] = 'PART_OF'
    output_colnames =  [':START_ID(genome-ID)', ':END_ID(accession-ID)', ':TYPE']
    df_genome_accession_edges = ensure_df_col_format(df_genome_accession_edges, output_colnames)
    df_genome_accession_edges.to_csv(args.output_edges_genome_accession, index=False, header=output_colnames)

    df_accession_species_edges[':TYPE'] = 'PART_OF'
    output_colnames =  [':START_ID(accession-ID)', ':END_ID(species-ID)', ':TYPE']
    df_accession_species_edges = ensure_df_col_format(df_accession_species_edges, output_colnames)
    df_accession_species_edges.to_csv(args.output_edges_accession_species, index=False, header=output_colnames)