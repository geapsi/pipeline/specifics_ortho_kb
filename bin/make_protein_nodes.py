#!/usr/bin/env python3

"""
21/02/11
Takes a fasta input of proteins, and extract their name and length, creates an output dataframe

Usage: python3 parse_fasta_proteins_names_to_df.py -fasta /home/bimbert/Documents/Data/Proteins/psat.fa -taxon_id 31258 -output 
"""

import argparse
from Bio import SeqIO
import pandas as pd

from database_pipeline_functions import ensure_df_col_format

parser = argparse.ArgumentParser(description = 'Takes a fasta input of proteins, \
    and extract their name and length, creates an output dataframe')
parser.add_argument('-fasta', help = 'input protein fasta file', required = True)
parser.add_argument('-sp_info', help = 'tsv file containing info on species to \
    be added as nodes attributes', required=True)
parser.add_argument('-species', help = 'species acronym used in the tsv file \
    containing info on species', required=True)
parser.add_argument('-sep', help = 'header separator used. Default is a space. \
    If you wish to keep the full header, set it to None', default = " ")
parser.add_argument('-output', help = 'output file name. If not mentionned, \
    writes in place', required=True, default = "protein_nodes.csv")

args = parser.parse_args()


# FUNCTIONS
def create_df(fasta, df_sp_info, separator, species):
    """ Creates a pd dataframe with information on proteins from the fasta
    @param fasta     fasta of proteins
    @param sp_info   a pd dataframe with
    @param s       set of rna families

    @return  the list with additional element
    """

    d = []
    for record in SeqIO.parse(fasta,"fasta"):
        meth = 'false'
        #protein_id = "prot_" + record.id.split(separator)[0]
        protein_id = record.id.split(separator)[0]
        if record.seq.startswith('M'):
            meth = 'true'
        d.append(
            {
                'protein_id': protein_id,
                'length': len(record.seq),
                'm_init_aa': meth
            }
        )
    df_fa = pd.DataFrame(d)
    df_fa['ncbi_taxon_id'] = df_sp_info.loc[species,'ncbi_taxon_id']
    return df_fa


if __name__ == "__main__":

    df_info = pd.read_csv(args.sp_info, sep = '\t', comment = '#', header = 0, index_col = 0)
    df_prot = create_df(args.fasta, df_info, args.sep, args.species)
    col_prot_nodes = ['protein_id:ID(protein-ID)','length:int',
        'starts_with_M:boolean','ncbi_taxon_id:String']
    df_prot = ensure_df_col_format(df_prot, col_prot_nodes)
    df_prot[':LABEL'] = 'Protein'
    df_prot.to_csv(args.output, index=False, header = True)