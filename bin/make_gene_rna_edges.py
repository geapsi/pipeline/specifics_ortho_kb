#!/usr/bin/env python3

# conda activate alignmentAnalysis
"""
2021/10/22

Creates a csv describing relationships between a gene and its RNA (the longest isoform).
Usage: python3 make_gene_rna_edges.py -gff psat.gff3  -output psat_edges_gene_to_rna.csv
"""

import argparse
import pandas as pd

from database_pipeline_functions import check_gff3_format
from database_pipeline_functions import get_rna_gene_dic
from database_pipeline_functions import ensure_df_col_format

# ARGPARSE
parser = argparse.ArgumentParser(description = 'Get relationships between gene\
    and rna nodes')
parser.add_argument('-gff', help = 'gff file', required=True)
parser.add_argument('-output', help = 'output file', required=False)

args = parser.parse_args()

if __name__ == "__main__":
    with open(args.gff, 'r', encoding = 'utf-8') as gff_file:
        check_gff3_format(gff_file)
        d_rna_gene = get_rna_gene_dic(gff_file)
    df_rna_gene = pd.DataFrame.from_dict(d_rna_gene, orient = 'index').reset_index().rename(columns = {'index': 'rna_id', 0: 'gene_id'})
    df_gene_rna_edges = df_rna_gene[['gene_id', 'rna_id']].copy()
    df_gene_rna_edges[':TYPE'] = 'HAS_TRANSCRIPT'

    col_edges = [':START_ID(gene-ID)', ':END_ID(rna-ID)',
        ':TYPE']
    df_gene_rna_edges = ensure_df_col_format(df_gene_rna_edges, col_edges)
    df_gene_rna_edges.to_csv(args.output, index=False, header = True)