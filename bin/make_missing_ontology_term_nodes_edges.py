#!/usr/bin/env python3

"""
Creates nodes of ontology terms that are not yet in the latest release of ontologies
Connects them to the parent term mentionned in the input.
Usage: TODO
"""

import argparse

import pandas as pd


parser = argparse.ArgumentParser(description= 'Creates ontology term placeholders \
    before they are officially added to their respective ontologies')
parser.add_argument('-csv_new_terms', help='csv file with ontology terms to add as nodes \
    and connect to their ontologies. \
        Columns should be ontology, subclassof, name, definition', required=True)
parser.add_argument('-output_nodes_new_onto_terms', help='csv of new onto term nodes',
    required=False, default='new_ontology_term_nodes.csv')
parser.add_argument('-output_edges_new_onto_terms_onto', help='csv of edges between \
    new onto term nodes to their parent nodes',
    required=False, default='new_ontology_terms_ontology_edges.csv')
args = parser.parse_args()

def strip_and_check_onto_format(df):

    df['subclassof'] = df['subclassof'].str.replace(':', '_').str.replace(' ', '').str.strip()
    df['name'] = df['name'].str.replace(':', '_').str.replace(' ', '').str.strip()
    print(df)
    return df

def create_new_onto_nodes(df):
    """Returns a df columns to create an ontology node
    @param df   pandas dataframe with cols name, label, definition, uri, status, ontology
    @return  pandas dataframe with cols name, label, definition, uri, status, neo4j_label
    """
    df_nodes = df.copy()
    # creating a fake uri
    df_nodes['uri'] = 'http://purl.obolibrary.org/obo/' + df['name']
    df_nodes['status'] = 'Unofficial substitute term. Should be replaced with an official term.'
    df_nodes['ontology'] += ';Resource'
    df_nodes = df_nodes[['name', 'label', 'definition', 'uri', 'status', 'ontology']]
    df_nodes.columns = ['name', 'label', 'definition', 'uri', 'status', 'neo4j_label']
    return df_nodes

def create_new_onto_to_parent_edges(df):
    """Returns a df columns to create relationships between new onto nodes and their parent node
    @param df   pandas dataframe with cols new_term_id, subclassof
    @return  pandas dataframe with cols new_term, parent_term, relationship_name
    """
    df_edges = df.copy()
    df_edges = df_edges[['name', 'subclassof']]
    # case where a new term is connected to more than one parent term
    df_edges['subclassof'] = df_edges['subclassof'].str.split(';')
    df_edges = df_edges.explode('subclassof').reset_index(drop=True)
    df_edges['relationship'] = 'SCO'
    df_edges.columns = ['new_term', 'parent_term', 'relationship_name']
    return df_edges

if __name__ == '__main__':

    with open(args.csv_new_terms, 'r', encoding='utf-8-sig') as new_onto_terms:
        df_onto = pd.read_csv(new_onto_terms)

    df_onto = strip_and_check_onto_format(df_onto)
    print(df_onto)
    df_nodes = create_new_onto_nodes(df_onto)
    df_edges = create_new_onto_to_parent_edges(df_onto)

    df_nodes.columns = ['name:ID', 'label:String', 'definition:String', 'uri:String',
        'status:String', ':LABEL']
    df_nodes.to_csv(args.output_nodes_new_onto_terms, index=False)
    df_edges.columns = [':START_ID', ':END_ID', ':TYPE']
    df_edges.to_csv(args.output_edges_new_onto_terms_onto, index=False)