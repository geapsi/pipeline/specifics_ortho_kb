#!/usr/bin/env python3

"""
17/02/21
Takes a two node files (mRNA and proteins), checks first column (assumed to be id), and performs a leftjoin to ensure each protein can be linked to RNA\
consequently a RNA can have no protein.

Usage: python3 make_edges_mRNA_protein.py -nodes_RNA -nodes_protein -sep ":" -output
"""

import os
import sys
import argparse
import re

import pandas as pd

from database_pipeline_functions import ensure_df_col_format

parser = argparse.ArgumentParser(description='Takes a fasta input of proteins, and extract their name and length, creates an output dataframe')
parser.add_argument('-nodes_rna', help='input csv nodes of RNAs', required=True)
parser.add_argument('-nodes_protein', help='input csv nodes of proteins',
    required=True)
parser.add_argument('-output_edges', help='output file of RNA to protein edges',
    required=False, default='rna_protein_edges.csv')

args = parser.parse_args()


# FUNCTIONS
def check_sep(df_nodes, sep="_"):
    """ Check the first row of protein nodes and verifies that the -sep is the same as the one found
    If different, tells so
    @param filename   node file as pd dataframe

    @return  the found prefix or None
    """
    print("Using separator '",sep, "'")
    first_id = df_nodes.iloc[0,0]
    print("Checking first id (first cell)")
    m = bool(re.search(sep, first_id))
    prefix = None
    if m:
        prefix = first_id.split(sep)[0] + sep
        print("Found prefix '", prefix, "'")
    else:
        print("Separator (default is '_') was not found in the id")
        sys.exit("No prefix found with current separator. Ending")

    return prefix

def make_raw_id(df_nodes, prefix):
    """ Removes the prefix (eg gene) from the id (first col) to create another col called raw_id
    @param filename   node file as pd dataframe

    @return  same dataframe with raw_id column
    """
    if len(prefix) > 0:
        df_nodes['raw_node_id'] = [row_node_id.split(prefix)[1] for row_node_id in df_nodes.iloc[:,0]]
    else:
        df_nodes['raw_node_id'] = df_nodes.iloc[:,0]

    print("raw_node_id column created")
    return df_nodes


def create_edges_df(df_nodes_mrna, df_nodes_protein):
    """! From two modified df (from make_raw_id()), create a 2col df for edges import in neo4j
    @param df_nodes_mRNA   mRNA df from make_raw_id()
    @param df_nodes_protein   protein df from make_raw_id()

    @return  a df for neo4j import of edges to create
    """

    #stuff with join (leftjoin, keeping all mRNAs but not "free" proteins?)
    print("\nInitial number of RNAs")
    ini_rna = len(df_nodes_mrna)
    print(ini_rna)
    print("\nInitial number of proteins")
    ini_prot = len(df_nodes_protein)
    print(ini_prot)

    df = pd.merge(df_nodes_mrna.iloc[:,[0,-1]], df_nodes_protein.iloc[:,[0,-1]], left_on = 'raw_node_id', right_on = 'raw_node_id', how = 'inner')

    df.drop('raw_node_id', axis=1, inplace=True)
    print("Protein loss")
    # protein before - protein now
    print(ini_prot - len(df.iloc[:,1].unique()))
    return df

if __name__ == "__main__":

    df_nodes_rna = pd.read_csv(args.nodes_rna)
    df_nodes_protein = pd.read_csv(args.nodes_protein)

    # get prefixs
    pref_rna = ""
    #pref_prot = check_sep(df_nodes_protein)
    pref_prot = ""

    print("\nCreating column without id prefix")
    df_nodes_rna_newcol = make_raw_id(df_nodes_rna, pref_rna)
    df_nodes_protein_newcol = make_raw_id(df_nodes_protein, pref_prot)
    # create 2col df for import edges import
    df_nodes_rna_id = df_nodes_rna.iloc[:,0]
    df_nodes_protein_id = df_nodes_protein.iloc[:,0]

    df_edges = create_edges_df(df_nodes_rna_newcol, df_nodes_protein_newcol)
    col_edges = [':START_ID(rna-ID)', ':END_ID(protein-ID)']
    df_edges = ensure_df_col_format(df_edges, col_edges)
    df_edges[':TYPE'] = 'HAS_PRODUCT'
    df_edges.to_csv(args.output_edges, index=False, header=True)