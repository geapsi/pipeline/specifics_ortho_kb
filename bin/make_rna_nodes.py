#!/usr/bin/env python3

# conda activate python3_basics
"""
19/03/21
Create rna nodes from gff3 file

Usage: python3 make_rna_nodes_v2.py -gff psat_filtered_rna.gff3 -sp_info ../../nodes_genes/WIP/species_info.tsv -output test_psat
"""

import re
import argparse

import pandas as pd

# ARGPARSE
parser = argparse.ArgumentParser(description = 'Parse gff and emapper to get rna \
    nodes for neo4j')
parser.add_argument('-gff', help='gff file of the species.', required=True)
parser.add_argument('-sp_info', help='tsv file containing info on species',
    required=True)
parser.add_argument('-species', help='species acronym used in the tsv file \
    containing info on species', required=True)
parser.add_argument('-output', help='output nodes', required=False,
    default='rna_nodes.csv')

args = parser.parse_args()

# FUNCTIONS
def merge_gff_spinfo(gff_file, df_info, species):
    """ Add rna and associated go to the dic
    @param gff_file   gff file
    @param df_info    pd df from species info file
    @param species    species acronym, first col of sp_info_file

    @return  a dataframe with parsed info on gff and species_info
    """
    pattern = 'ID=(.+?)[;\n]'
    pattern_parent = 'Parent=(.+?)[;\n]'
    d = {}
    with open(gff_file, 'r', encoding='utf-8') as fl:
        for line in fl:
                if (line.startswith('#') or len(line.strip()) == 0):
                    continue
                row = line.split('\t')
                feature = row[2]
                if feature not in ['mRNA', 'exon']:
                    continue
                if feature == 'mRNA':
                    rna_id = re.search(pattern, row[-1]).group(1)
                    d[rna_id] = {}
                    d[rna_id]['length'] = (int(row[4]) - int(row[3]) + 1)
                    d[rna_id]['exon_count'] = 0
                if feature == 'exon':
                    parent_id = re.search(pattern_parent, row[-1]).group(1)
                    if parent_id in d.keys():
                        if ',' in parent_id:
                            for i in parent_id.split(','):
                                d[i]['exon_count'] += 1
                        else:
                            d[parent_id]['exon_count'] += 1
    df_gff = pd.DataFrame.from_dict(d, orient = 'index')
    df_gff.insert(0, 'rna_id', df_gff.index)
    df_gff['ncbi_taxon_id'] = df_info.loc[species,'ncbi_taxon_id']

    return df_gff

if __name__ == "__main__":

    df_info = pd.read_csv(args.sp_info, sep='\t', comment='#', header=0,
        index_col=0)
    df_gff = merge_gff_spinfo(args.gff, df_info, str(args.species))
    df_gff[':LABEL'] = 'RNA'
    df_gff.to_csv(args.output, index=False,
        header=['rna_id:ID(rna-ID)', 'length:int', 'exon_count:int',
            'ncbi_taxon_id:long', ':LABEL'])