#!/usr/bin/env python3

"""
21/11/30
From QTL file, get QTL nodes and QTL dataset nodes. This script also calculates \
genes belonging to QTLs.

Usage: TODO
"""

import argparse
import re
import logging
from itertools import chain

import pandas as pd
import numpy as np
import pybedtools

from database_pipeline_functions import ensure_df_col_format, create_node_onto_edges, get_node_free_term_edges, get_dataset_status

parser = argparse.ArgumentParser(description= 'Create nodes and edges for QTL \
    integration')
parser.add_argument('-qtl', help='csv file with qtl information, header \
    as first line', required= True)
parser.add_argument('-qtl_annotation', help='csv file with qtl annotation, header \
as first line', required= True)
parser.add_argument('-dataset',
    help='input dataset information on the project (supervisor, doi etc.)',
    required=True)
parser.add_argument('-status',
    help='status of the dataset, either public or private',
    required=True)
parser.add_argument('-gff', help='gff file for the species considered to \
    connect genes in the confidence interval', required=True)
parser.add_argument('-chr_conv', help='if chromosomes from the qtl file must \
    be converted, pass a two-cols csv file with old and new chromosomes. \
    The file should not have any header', required=False)
parser.add_argument('-output_nodes_qtl', help='output file name for qtl nodes' \
    , required=False, default='qtl_nodes.csv')
parser.add_argument('-output_nodes_trait', help='output file name for trait \
    nodes', required=False, default='trait_nodes.csv')
parser.add_argument('-output_nodes_population', help='output file name for \
    population nodes', required=False, default='population_nodes.csv')
parser.add_argument('-output_nodes_site', help='output file name for site \
    nodes', required=False, default='site_nodes.csv')
parser.add_argument('-output_nodes_year', help='output file name for year \
    nodes', required=False, default='site_nodes.csv')
parser.add_argument('-output_nodes_free_term',
    help='free desc term nodes in csv format', required=False,
    default='free_term_nodes.csv')
parser.add_argument('-output_nodes_dataset', help='output file name for qtl \
    dataset nodes', required=False, default='qtl_dataset_nodes.csv')
parser.add_argument('-output_edges_gene_qtl', help='output file name for \
    edges between qtl and genes', required=False,
    default='gene_qtl_edges.csv')
parser.add_argument('-output_edges_qtl_chromosome',
        help='output file name for edges between qtl and chromosomes',
        required=False,default='qtl_chromosome_edges.csv')
parser.add_argument('-output_edges_qtl_dataset', help='output file name for \
    edges between qtl and dataset nodes', required=False,
    default='qtl_dataset_edges.csv')
parser.add_argument('-output_edges_qtl_onto', help='output file name \
    for edges between qtl and ontology terms describing it', required=False,
    default='qtl_onto_edges.csv')
parser.add_argument('-output_edges_qtl_free_term',
    help='qtl to free desc term node in csv format', required=False,
    default='condition_free_term_edges.csv')
parser.add_argument('-output_edges_qtl_population', help='output file name \
    for edges between qtl and population', required=False,
    default='qtl_population_edges.csv')
parser.add_argument('-output_edges_qtl_site', help='output file name for \
    edges between qtl and site', required=False,
    default='qtl_site_edges.csv')
parser.add_argument('-output_edges_qtl_year', help='output file name for \
    edges between qtl and year', required=False,
    default='qtl_site_edges.csv')
parser.add_argument('-logs', help='output file name for logs',
    required=False, default='qtl_logs.txt')
args = parser.parse_args()

pd.options.mode.chained_assignment = None  # default='warn'
logging.basicConfig(filename = args.logs, level = logging.DEBUG)

def remove_trailing_spaces(df):
    """ For all string columns of the df, removes trailing spaces
    @param df  any pd dataframe

    @return  the same dataframe without trailing spaces
    """
    df_no_trailing_spaces = df.applymap(
        lambda x: x.strip() if isinstance(x, str) else x
        )
    return df_no_trailing_spaces

def remove_rows_to_ignore(df):
    """ Removes all rows where the 'ignore' column is set to 'yes'
    @param df  any pd dataframe

    @return  the same dataframe without rows to ignore
    """
    df_no_ignored = df.loc[df['ignore']  != 'yes']
    df_no_ignored.drop(columns = ['ignore'], inplace=True)
    return df_no_ignored

def create_unique_qtl_id(df):
    """ Creates a qtl_id which is the concat of dataset_id and qtl_id
    @param df  a df qtl position dataframe

    @return  the same dataframe with the new qtl_id and the original_qtl_id
    """
    df['original_qtl_id'] = df['qtl_id']
    df['qtl_id'] = df['dataset_id'].astype(str) + '_' + df['qtl_id'].astype(str)
    return df

def convert_chr(qtl, chr_conv):
    """ Convert chromosomes from qtl to new format from chr_conv
    @param qtl   pandas dataframe of qtl
    @param chr_conv   pandas dataframe of conversion from old to new chr names
    @return  the qtl with chromosomes in new format
    """
    df_merge = qtl.merge(chr_conv, how='left', left_on='chromosome',
        right_on='old_chr')
    df_merge['chromosome'] = df_merge['new_chr']
    df_merge.drop(columns = ['old_chr', 'new_chr'], inplace=True)
    return df_merge

def write_log_coordinates_inversion(df_qtls):
    """ Report QTL with inversion of marker coordinates (start should be < end)
    @param df_qtls   pandas dataframe of qtl
    @return  the qtl with chromosomes in new format
    """
    logging.info('State before marker inversion.')
    logging.info('Number of QTL markers where leftmarker start > end: %s',
        len(df_qtls.loc[df_qtls['leftmarker_start'] > df_qtls['leftmarker_end']]))
    logging.info('Number of QTL markers where rightmarker start > end: %s',
        len(df_qtls.loc[df_qtls['rightmarker_start'] > df_qtls['rightmarker_end']]))
    logging.info('Number of QTL markers where peakmarker start > end: %s',
        len(df_qtls.loc[df_qtls['peakmarker_start'] > df_qtls['peakmarker_end']]))
    logging.info('Inverting marker positions where necessary')
    return None

def filter_missing_data(df):
    """ Remove QTL where missing essential information.
        Also using population_type info, if diversity_panel, peak is mandatory
    @param df   pandas dataframe of qtl
    @return  filtered df
    """
    # chromosome info is essential
    qtl_chr_missing = df[df['chromosome'].isnull()]['qtl_id'].tolist()

    # markers delimiting QTL and their location must be known
    df_biparental = df.loc[df['population_type']  == 'Biparental']
    biparental_checklist =  ['leftmarker_start', 'leftmarker_end',
    'rightmarker_start', 'rightmarker_end']
    biparental_complete_rows = df_biparental[biparental_checklist].dropna()\
        .index.values.tolist()
    missing_biparental_limits = df_biparental.loc[~df_biparental.index\
        .isin(biparental_complete_rows)]['qtl_id'].tolist()

    # but if peakmarker info is complete, can still accept the QTL
    biparental_checklist_second_chance = ['peakmarker_start', 'peakmarker_end']
    biparental_correct_peakmarker = df_biparental[\
        biparental_checklist_second_chance].dropna().index.values.tolist()
    biparental_second_chance_candidates = df_biparental.loc[df_biparental\
        .index.isin(biparental_correct_peakmarker)]['qtl_id'].tolist()
    biparental_second_chance = [x for x in biparental_second_chance_candidates \
        if x in missing_biparental_limits]
    removed_biparental = [x for x in missing_biparental_limits \
        if x not in biparental_second_chance]

    # for GWAS markers peakmarker location must be known
    df_gwas = df.loc[df['population_type']  == 'DiversityPanel']
    gwas_checklist =  ['peakmarker_start', 'peakmarker_end']
    gwas_complete_rows = df_gwas[gwas_checklist].dropna().index.values.tolist()
    missing_gwas_peak = df_gwas.loc[~df_gwas.index\
        .isin(gwas_complete_rows)]['qtl_id'].tolist()

    # but if interval info is complete, can still accept the QTL
    gwas_checklist_second_chance = biparental_checklist
    gwas_correct_peakmarker = df_gwas[\
        gwas_checklist_second_chance].dropna().index.values.tolist()
    gwas_second_chance_candidates = df_gwas.loc[df_gwas\
        .index.isin(gwas_correct_peakmarker)]['qtl_id'].tolist()
    gwas_second_chance = [x for x in gwas_second_chance_candidates \
        if x in missing_gwas_peak]
    removed_gwas = [x for x in missing_gwas_peak \
        if x not in gwas_second_chance]

    logging.warning('Removed the following QTL, they had missing chromosome information:\n %s', qtl_chr_missing)
    logging.warning('The following biparental QTL had missing delimiting interval marker information:\n %s', missing_biparental_limits)
    logging.warning('The following diversity panel QTL had missing peakmarker marker information:\n %s', missing_gwas_peak)
    logging.warning('The following biparental QTL had missing marker limits, but were conserved for correct peakmarker information:\n %s', biparental_second_chance)
    logging.warning('Removed the following biparental QTL, they had missing marker information:\n %s', removed_biparental)
    logging.warning('Removed the following diversity panel QTL, they had missing marker information:\n %s', removed_gwas)

    l_rm_qtl = qtl_chr_missing + removed_biparental + removed_gwas
    df_return = df[~df['qtl_id'].isin(l_rm_qtl)]
    return df_return

def invert_columns(df, condition, l_col):
    """ Need to ensure that the first element in the list is < to the second.
    If not, swap.
    @param df   pandas dataframe from qtl table
    @param l_col  pair of columns considered

    @return  same df with swapped values if necessary
    """
    for i in range(0, len(l_col[0])):
        element_a = l_col[0][i]
        element_b = l_col[1][i]
        df.loc[condition, [element_a, element_b]] = df.loc[condition, \
            [element_b, element_a]].values
    return df

def correct_marker_order(df):
    df_peak = df[['dataset_id', 'qtl_id', 'chromosome', 'peakmarker_id', 'peakmarker_genetpos',
        'peakmarker_start', 'peakmarker_end']].dropna(subset=['qtl_id',
        'peakmarker_start', 'peakmarker_end'])
    df_interval = df[['dataset_id', 'qtl_id', 'chromosome',
        'leftmarker_id', 'leftmarker_genetpos',
        'leftmarker_start', 'leftmarker_end',
        'rightmarker_id', 'rightmarker_genetpos',
        'rightmarker_start', 'rightmarker_end']].dropna(subset=['qtl_id',
        'leftmarker_start', 'leftmarker_end',
        'rightmarker_start', 'rightmarker_end'])

    # conditions for which a switch of left/right coordinates is necessary
    reversed_peak = df_peak['peakmarker_start'] > df_peak['peakmarker_end']
    reversed_left = df_interval['leftmarker_start'] > df_interval['leftmarker_end']
    reversed_right = df_interval['rightmarker_start'] > df_interval['rightmarker_end']
    reversed_markers = df_interval['leftmarker_start'] > df_interval['rightmarker_end']

    if len(df_peak) > 0:
        df_peak = invert_columns(df_peak, reversed_peak,
            [['peakmarker_start'], ['peakmarker_end']])
    if len(df_interval) > 0:
        df_interval = invert_columns(df_interval, reversed_left,
            [['leftmarker_start'], ['leftmarker_end']])
        df_interval = invert_columns(df_interval, reversed_right,
            [['rightmarker_start'], ['rightmarker_end']])
        df_interval = invert_columns(df_interval, reversed_markers,
            [['leftmarker_start', 'leftmarker_end', 'leftmarker_genetpos', 'leftmarker_id'],
            ['rightmarker_start', 'rightmarker_end', 'rightmarker_genetpos', 'rightmarker_id']])

    df_corrected = df.merge(df_peak, on=['dataset_id', 'qtl_id', 'chromosome'],
        how='left', suffixes=('_merge_old_cols', ''))
    df_corrected = df_corrected.merge(df_interval, on=['dataset_id', 'qtl_id', 'chromosome'],
        how='left', suffixes=('_merge_old_cols', ''))
    cols_to_drop = [col for col in df_corrected.columns if col.endswith('_merge_old_cols')]
    df_corrected.drop(columns=cols_to_drop, inplace=True)
    return df_corrected

def make_coordinate_cols_integer(df):
    """ Bedtools requires all coordinates to be integers, forcing it
    @param df   pandas dataframe from qtl table

    @return  same df with swapped values if necessary
    """
    # ensure coordinate columns are integer for bedtools
    l_col_coords = [col for col in df.columns if ('_start' in col) or ('_end' in col)]
    df[l_col_coords] = df[l_col_coords].apply(np.int64)
    return df

def qtls_to_bed(df_qtls, l_col_out, filter_NA=False):
    """ From qtls in pandas df to df with selected columns in bed format and
    remove rows with NA
    @param df_qtls  path of the merged gff file
    @param l_col_out  list of output columns (in order)

    @return  a pandas df with selected columns to write as bed file
    """
    df_to_return = df_qtls[l_col_out]
    if filter_NA:
        df_to_return.dropna(inplace=True)
    return df_to_return

def get_df_gff(gff_file):
    """ Reads a gff file and returns a list of dic (one dic per line) with info
    on genes and their location
    @param gff_file  a classical gff3 formated file

    @return  a pandas df with selected columns to write as bed file
    """
    pattern_parent = 'Parent=(.+?)[;\n]'
    l = []
    s_genes = set()
    for line in gff_file:
        if (line.startswith('#') or len(line.strip()) == 0):
            continue
        row = line.split('\t')
        if row[2] != 'mRNA':
            continue
        # extract gene id from last field of gff
        gene_id = re.search(pattern_parent, row[-1]).group(1)
        if gene_id not in s_genes:
            s_genes.add(gene_id)
            l.append(
                {
                    'chromosome': row[0],
                    'start': row[3],
                    'end': row[4],
                    'gene_id': gene_id
                }
            )
    df_gff = pd.DataFrame(l)
    return df_gff

def create_qtl_chromosome_edges(df):
    """ From a df of QTL and GWAS get a link between the QTL and the chr it is
    located on. Also, adds properties to specify coordinates
    @param df  a df with QTL info (qtl_id, chromosome, population_type,
    leftmarker_start, leftmarker_end, rightmarker_start, rightmarker_end,
    peakmarker_start, peakmarker_end)

    @return  a df with qtl_id, chromosome, TYPE, start, end
    """
    # delimiting markers
    confidence_interval_cols = [col for col in df.columns if col not in \
        ['peakmarker_start', 'peakmarker_end']]
    df_confidence_interval = df[confidence_interval_cols].dropna()
    df_confidence_interval = df_confidence_interval[['qtl_id', 'chromosome',
    'leftmarker_start', 'rightmarker_end']]

    # only peakmarker info
    peak_cols = [col for col in df.columns if col not in \
        ['leftmarker_start', 'leftmarker_end',
        'rightmarker_start', 'rightmarker_end']]
    df_peak = df[peak_cols].dropna()
    df_only_peak = df_peak[~df_peak['qtl_id']\
        .isin(df_confidence_interval['qtl_id'])]
    df_only_peak = df_only_peak[['qtl_id', 'chromosome',
    'peakmarker_start', 'peakmarker_end']]

    final_cols = ['qtl_id', 'chromosome', 'start', 'end']
    df_confidence_interval.columns = final_cols
    df_only_peak.columns = final_cols

    # the range will be that of delimiting markers or peakmarker if the info
    # of delim markers is not available
    df_qtl_chr = pd.concat([df_confidence_interval, df_only_peak])
    df_qtl_chr['start'] = df_qtl_chr['start'].apply(np.int64)
    df_qtl_chr['end'] = df_qtl_chr['end'].apply(np.int64)
    return df_qtl_chr

def create_qtl_dataset_edges(df_dataset, df_qtl):
    """ Create links between datasets and qtls
    @param df_dataset  pd df of datasets
    @param df_qtl  pd df of qtls

    @return  a two cols pd df with qtl_id and dataset_id
    """
    df_dataset_qtl = df_dataset.merge(df_qtl[['dataset_id', 'qtl_id']],
        how='left', on='dataset_id')
    df_dataset_qtl = df_dataset_qtl[['qtl_id', 'dataset_id']].dropna()
    return df_dataset_qtl

def select_cols(df, l_cols_select, l_cols_out):
    """ Selects the l_cols_select and rename them with l_cols_out
    @param df  pandas dataframe
    @param l_cols_select  list of columns to select (index)
    @param l_cols_out  list of column names for selection

    @return  sub df
    """
    df_sub = df.iloc[:,l_cols_select]
    df_sub.columns = l_cols_out
    return df_sub

def create_duplicate_qtl_peak_edge(df_qtl_peak_edges):
    """ Create a duplicated edge for each gene colocalizing with the peak of a QTL.
    @param df_qtl_peak_edges  df with gene_id, qtl_id, distance, :TYPE

    @return  the same dataframe with NA distance and the appropriate :TYPE
    """
    df_qtl_peak_edges_coloc = df_qtl_peak_edges.copy()
    df_qtl_peak_edges_coloc['distance'] = np.nan
    df_qtl_peak_edges_coloc[':TYPE'] = 'COLOCALIZES_WITH'

    df_full = pd.concat((df_qtl_peak_edges, df_qtl_peak_edges_coloc), axis=0)

    return df_full

if __name__ == "__main__":

    pd.set_option('display.max_columns', None)
    # INPUT
    qtl_columns = {
        'dataset_id': 'object',
        'qtl_id': 'object',
        'population_type': 'object',
        'model': 'object',
        'lod': np.float64,
        'pvalue': np.float64,
        'additive': np.float64,
        'r2': np.float64,
        'linkage_group': 'object',
        'assembly': 'object',
        'chromosome': 'object',
        'peakmarker_id': 'object',
        'peakmarker_genetpos': np.float64,
        'peakmarker_start': 'Int64',
        'peakmarker_end': 'Int64',
        'leftmarker_id': 'object',
        'leftmarker_genetpos': np.float64,
        'leftmarker_start': 'Int64',
        'leftmarker_end': 'Int64',
        'rightmarker_id': 'object',
        'rightmarker_genetpos': np.float64,
        'rightmarker_start': 'Int64',
        'rightmarker_end': 'Int64',
        'ignore' : 'object',
        'comment_1': 'object',
        'comment_2': 'object'
    }
    df_qtls = pd.read_csv(args.qtl, sep=',', comment='#',
        header=0, dtype=qtl_columns)
    df_qtl_annotation = pd.read_csv(args.qtl_annotation, sep=',', comment='#', header=0)
    df_qtl_annotation = create_unique_qtl_id(df_qtl_annotation)
    # dataset_id, author, year, doi, genus, species
    df_datasets = pd.read_csv(args.dataset, sep=',', comment='#', header=0)
    dataset_status = get_dataset_status(args.status)

    df_qtls = remove_trailing_spaces(df_qtls)
    df_qtl_annotation = remove_trailing_spaces(df_qtl_annotation)

    # remove qtl lines indicated to be ignored
    df_qtls = remove_rows_to_ignore(df_qtls)

    # qtl_id is not dataset_id + qtl_id (the original is kept)
    df_qtls = create_unique_qtl_id(df_qtls)

    if args.chr_conv:
        df_chr_conv = pd.read_csv(args.chr_conv, sep=',', comment='#', names=[
            'old_chr', 'new_chr'])
        df_qtls = convert_chr(df_qtls, df_chr_conv)
    df_qtls = filter_missing_data(df_qtls)
    df_qtl_annotation = df_qtls[['qtl_id']].merge(df_qtl_annotation, how='left', on='qtl_id')

    # checks that start < to end for left/right markers and for peakmarkers
    # and that leftmarker before rightmarker, then correct it
    write_log_coordinates_inversion(df_qtls)
    df_qtls = correct_marker_order(df_qtls)

    df_qtl_peak = df_qtls[['qtl_id', 'chromosome', 'peakmarker_id', 'peakmarker_genetpos',
        'peakmarker_start', 'peakmarker_end']].dropna(subset=['qtl_id',
        'peakmarker_start', 'peakmarker_end'])
    df_qtl_interval = df_qtls[['qtl_id', 'chromosome',
        'leftmarker_id', 'leftmarker_genetpos',
        'leftmarker_start', 'leftmarker_end',
        'rightmarker_id', 'rightmarker_genetpos',
        'rightmarker_start', 'rightmarker_end']].dropna(subset=['qtl_id',
        'leftmarker_start', 'leftmarker_end',
        'rightmarker_start', 'rightmarker_end'])

    if len(df_qtl_peak) > 0:
        df_qtl_peak = make_coordinate_cols_integer(df_qtl_peak)
    if len(df_qtl_interval) > 0:
        df_qtl_interval = make_coordinate_cols_integer(df_qtl_interval)

    # BEDTOOLS INTERSECT GET GENES IN QTL

    with open(args.gff, 'r', encoding='utf-8') as gff_file:
        df_gff_filtered = get_df_gff(gff_file)
    bed_gff = pybedtools.BedTool.from_dataframe(df_gff_filtered)

    # deal with intervals
    are_intervals = True if len(df_qtl_interval) > 0 else False
    if are_intervals:
        l_col_qtl = ['chromosome', 'leftmarker_start', 'rightmarker_end', 'qtl_id']
        df_bed_qtls = qtls_to_bed(df_qtl_interval, l_col_qtl, filter_NA = True)
        bed_qtls = pybedtools.BedTool.from_dataframe(df_bed_qtls)
        df_intersect_qtls_gff = bed_qtls.intersect(
            bed_gff, wa=True, wb=True).to_dataframe()
    else:
        df_intersect_qtls_gff = pd.DataFrame(columns = ['chrom', 'start', 'end',
        'name', 'score', 'strand', 'thickStart', 'thickEnd'])
    df_intersect_qtls_gff_sub = select_cols(df_intersect_qtls_gff, [7,3], ['gene_id', 'qtl_id'])

    # deal with peaks
    are_peaks = True if len(df_qtl_peak) > 0 else False
    if are_peaks:
        l_col_peak = ['chromosome', 'peakmarker_start', 'peakmarker_end', 'qtl_id']
        df_bed_qtls_peaks = qtls_to_bed(df_qtl_peak, l_col_peak, filter_NA = True)
        bed_qtls_peaks = pybedtools.BedTool.from_dataframe(df_bed_qtls_peaks)
        df_intersect_peaks_gff = bed_qtls_peaks.sort().closest(
            bed_gff.sort(), k=1, d=True).to_dataframe()
    else:
        df_intersect_peaks_gff = pd.DataFrame(columns = ['chrom', 'start', 'end',
        'name', 'score', 'strand', 'thickStart', 'thickEnd', 'itemRgb'])
    df_intersect_peaks_gff_sub = select_cols(
        df_intersect_peaks_gff, [7, 3, 5, 6, 8], ['gene_id', 'peak_id',
        'start', 'end', 'distance'])
    df_intersect_peaks_gff_sub = df_intersect_peaks_gff_sub[['gene_id', 'peak_id', 'distance']]

    # merge bedtools intersect results
    # get qtls where only peak was mentionned,
    # (contains all GWAS and Biparental with missing left and right markers!).
    # Need to concat manually because merge will exclude them
    df_peaks = df_intersect_peaks_gff_sub.copy()
    df_peaks.rename(columns = {'peak_id': 'qtl_id'}, inplace=True)
    df_peaks[':TYPE'] = 'IS_CLOSEST_TO_PEAK'
    # add a colocalizes_with relation for each gene--peak line
    df_peaks = create_duplicate_qtl_peak_edge(df_peaks)

    df_intersect_qtls_gff_sub['distance'] = np.nan
    df_intersect_qtls_gff_sub[':TYPE'] = 'COLOCALIZES_WITH'

    df_gene_qtl_edges = pd.concat((df_intersect_qtls_gff_sub, df_peaks), axis=0)
    df_gene_qtl_edges = df_gene_qtl_edges[['gene_id', 'qtl_id', 'distance', ':TYPE']].drop_duplicates()

    # get df for qtl nodes and dataset nodes
    columns_qtl_node = ['qtl_id', 'original_qtl_id', 'population_type', 'model', 'lod',
    'pvalue', 'additive', 'r2', 'linkage_group', 'chromosome',
    'peakmarker_id', 'peakmarker_genetpos', 'peakmarker_start', 'peakmarker_end',
    'leftmarker_id', 'leftmarker_genetpos', 'leftmarker_start', 'leftmarker_end',
    'rightmarker_id', 'rightmarker_genetpos', 'rightmarker_start', 'rightmarker_end',
    'comment_1', 'comment_2']
    df_qtl_nodes = df_qtls[columns_qtl_node]

    l_markers_loc = ['leftmarker_start', 'leftmarker_end', 'rightmarker_start',
        'rightmarker_end', 'peakmarker_start', 'peakmarker_end']
    df_qtl_chromosome = create_qtl_chromosome_edges(df_qtl_nodes[\
        ['qtl_id', 'chromosome'] + l_markers_loc])
    # Label to differentiate GWAS from biparental populations (QTL)
    df_qtl_nodes[':LABEL'] = 'QTL;' + df_qtl_nodes['population_type'].astype(str) + ';' + dataset_status
    df_qtl_nodes.drop('population_type', axis=1, inplace=True)
    # QTL to dataset
    df_qtl_dataset_edges = create_qtl_dataset_edges(df_datasets, df_qtls)
    df_dataset_nodes = df_datasets[df_datasets['dataset_id'].isin(df_qtl_dataset_edges['dataset_id'])]
    # POPULATION, SITE, YEAR
    df_qtl_population_edges = df_qtl_annotation[['qtl_id', 'population']].drop_duplicates().dropna()
    df_qtl_site_edges = df_qtl_annotation[['qtl_id', 'site']].drop_duplicates().dropna()
    df_qtl_year_edges = df_qtl_annotation[['qtl_id', 'year']].drop_duplicates().dropna()

    df_qtl_nodes = df_qtl_nodes.drop(['chromosome', 'peakmarker_start',
        'peakmarker_end', 'leftmarker_start','leftmarker_end',
        'rightmarker_start','rightmarker_end'], axis=1)
    df_population_nodes = df_qtl_population_edges[['population']].drop_duplicates()
    df_site_nodes = df_qtl_site_edges[['site']].drop_duplicates()
    df_year_nodes = df_qtl_year_edges[['year']].drop_duplicates()

    # DESCRIPTION WITH ONTOLOGIES
    d_onto = {'onto_tissue': 'IS_TISSUE',
        'onto_trait': 'HAS_TRAIT',
        'onto_stage': 'HAS_DEV_STAGE',
        'onto_condition': 'SUBJECTED_TO',
        'onto_location': 'HAS_LOCATION'}
    d_onto_original_on_edge = {'tissue': False,
        'trait': True,
        'stage': True,
        'condition': True,
        'location': False}
    df_qtl_onto_edges = create_node_onto_edges(df_qtl_annotation, d_onto,
        d_onto_original_on_edge, 'qtl_id')
    df_edges_qtl_free_term = get_node_free_term_edges(df_qtl_annotation, 'qtl_id')
    df_nodes_free_terms = df_edges_qtl_free_term[['term']].drop_duplicates().replace('nan', pd.NA).dropna()
    # WRITE FILES
    ## NODES
    col_qtl_nodes = ['qtl_id:ID(genetics_qtl-ID)', 'original_qtl_id:String',
        'model:String',
        'lod:double', 'pvalue:double', 'additive:double',
        'r2:double', 'linkage_group:String',
        'peakmarker_id:String', 'peakmarker_genetpos:double',
        'leftmarker_id:String', 'leftmarker_genetpos:double',
        'rightmarker_id:String', 'rightmarker_genetpos:double',
        'comment_1:String', 'comment_2:String', ':LABEL']
    df_qtl_nodes = ensure_df_col_format(df_qtl_nodes,
        col_qtl_nodes)
    df_qtl_nodes.to_csv(args.output_nodes_qtl, index=False, na_rep='')
    df_qtl_nodes = ensure_df_col_format(df_qtl_nodes,
            col_qtl_nodes)

    df_dataset_nodes = df_dataset_nodes[['dataset_id', 'author', 'year',
        'doi', 'genus', 'species']]
    df_dataset_nodes[':LABEL'] = 'QTL;Dataset' + ';' + dataset_status
    col_dataset_nodes = ['dataset_id:ID(genetics_dataset-ID)', 'author:String',
        'year:String', 'doi:String', 'genus:String', 'species:String', ':LABEL']
    df_dataset_nodes = ensure_df_col_format(df_dataset_nodes,
        col_dataset_nodes)
    df_dataset_nodes.to_csv(args.output_nodes_dataset, index=False, na_rep='')

    col_population_nodes = ['population_id:ID(genetics_population-ID)']
    df_population_nodes = ensure_df_col_format(df_population_nodes,
        col_population_nodes)
    df_population_nodes[':LABEL'] = 'Population'
    df_population_nodes.to_csv(args.output_nodes_population, index=False, na_rep='')

    col_site_nodes = ['site_id:ID(genetics_site-ID)']
    df_site_nodes = ensure_df_col_format(df_site_nodes,
        col_site_nodes)
    df_site_nodes[':LABEL'] = 'Site'
    df_site_nodes.to_csv(args.output_nodes_site, index=False, na_rep='')

    col_year_nodes = ['year_id:ID(genetics_year-ID)']
    df_year_nodes = ensure_df_col_format(df_year_nodes,
        col_year_nodes)
    df_year_nodes[':LABEL'] = 'Year'
    df_year_nodes.to_csv(args.output_nodes_year, index=False, na_rep='')

    col_free_term_nodes = ['label:ID(genetics_free_term-ID)']
    df_nodes_free_terms = ensure_df_col_format(df_nodes_free_terms,
        col_free_term_nodes)
    df_nodes_free_terms[':LABEL'] = 'Free'
    df_nodes_free_terms.to_csv(args.output_nodes_free_term, index=False)

    ## EDGES
    col_qtl_dataset_edges = [':START_ID(genetics_qtl-ID)',
        ':END_ID(genetics_dataset-ID)']
    df_qtl_dataset_edges = ensure_df_col_format(df_qtl_dataset_edges,
        col_qtl_dataset_edges)
    df_qtl_dataset_edges[':TYPE'] = 'PART_OF'
    df_qtl_dataset_edges.to_csv(args.output_edges_qtl_dataset, index=False, na_rep='')

    col_gene_qtl_edges = [':START_ID(gene-ID)',
        ':END_ID(genetics_qtl-ID)', 'distance_to_peak:long', ':TYPE']
    df_gene_qtl_edges = ensure_df_col_format(df_gene_qtl_edges,
        col_gene_qtl_edges)
    df_gene_qtl_edges.to_csv(args.output_edges_gene_qtl, index=False,
        na_rep='', float_format='%.0f')

    col_qtl_chromosome_edges = [':START_ID(genetics_qtl-ID)',
        ':END_ID(chromosome-ID)', 'start:long', 'end:long']
    df_qtl_chromosome = ensure_df_col_format(df_qtl_chromosome,
        col_qtl_chromosome_edges)
    df_qtl_chromosome[':TYPE'] = 'LOCATED_ON'
    df_qtl_chromosome.to_csv(args.output_edges_qtl_chromosome, index=False,
        na_rep='')

    col_qtl_onto_edges = [':START_ID(genetics_qtl-ID)', ':END_ID', ':TYPE',
        'original_desc:String']
    df_qtl_onto_edges.columns = col_qtl_onto_edges
    df_qtl_onto_edges.to_csv(args.output_edges_qtl_onto, index=False,
        na_rep='')

    col_qtl_free_term_edges = [':START_ID(genetics_qtl-ID)',
        ':END_ID(genetics_free_term-ID)', ':TYPE']
    df_edges_qtl_free_term = ensure_df_col_format(df_edges_qtl_free_term,
        col_qtl_free_term_edges)
    df_edges_qtl_free_term.to_csv(args.output_edges_qtl_free_term,
        index=False)

    col_qtl_population_edges = [':START_ID(genetics_qtl-ID)',
        ':END_ID(genetics_population-ID)']
    df_qtl_population_edges = ensure_df_col_format(df_qtl_population_edges,
        col_qtl_population_edges)
    df_qtl_population_edges[':TYPE'] = 'DETECTED_IN'
    df_qtl_population_edges.to_csv(args.output_edges_qtl_population,
        index=False, na_rep='')

    col_qtl_site_edges = [':START_ID(genetics_qtl-ID)',
        ':END_ID(genetics_site-ID)']
    df_qtl_site_edges = ensure_df_col_format(df_qtl_site_edges,
        col_qtl_site_edges)
    df_qtl_site_edges[':TYPE'] = 'MEASURED_ON'
    df_qtl_site_edges.to_csv(args.output_edges_qtl_site, index=False,
        na_rep='')

    col_qtl_year_edges = [':START_ID(genetics_qtl-ID)',
        ':END_ID(genetics_year-ID)']
    df_qtl_year_edges = ensure_df_col_format(df_qtl_year_edges,
        col_qtl_year_edges)
    df_qtl_year_edges[':TYPE'] = 'MEASURED_IN'
    df_qtl_year_edges.to_csv(args.output_edges_qtl_year, index=False,
        na_rep='')