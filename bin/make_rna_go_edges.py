#!/usr/bin/env python3

"""
01/03/22
From TRAPID Gene Ontology annotation file, get nodes and edges for neo4j import.
Nodes should not be bluk imported since the ontology is imported through n10s.

Usage: python3 make_rna_go_nodes_edges.py -emappannot WIP/psat.emapper.annotations -relation_type HAS_ANNOTATION -output test
"""

import argparse
from collections import defaultdict

import pandas as pd

from database_pipeline_functions import ensure_df_col_format, conv_string_to_boolean

# ARGPARSE
parser = argparse.ArgumentParser(description='Create nodes and edges files \
    between RNA and GOterm')
parser.add_argument('-go', help='annotation file from trapid', required=True)
parser.add_argument('-ignore_hidden_gos', help='either to skip hidden GOs \
    (the most specific GOs are kept)', required=False, default=True)
parser.add_argument('-output_edges', help='rnas to gos in csv format',
    required=False, default='edges_rna_go.csv')

args = parser.parse_args()

# FUNCTIONS

def get_rna_go_dic(line, d, ignore_hidden):
    """ Add rna and associated go to the dic
    @param line    line of TRAPID go file
    @param d       dic containing rna:go
    @param ignore_hidden       bool, collect hidden terms as well

    @return  the dic with additional element
    """
    row = line.strip().split('\t')
    if len(row) > 2:
        go = row[2].replace(':', '_')
        # 0 or 1, if 1 then term is hidden since exists more specific term(s)
        is_hidden = row[4]
        if ignore_hidden:
            if is_hidden == "0":
                rna_id = row[1]
                if rna_id in d.keys():
                    existent_gos = d[rna_id]
                    d[rna_id] = existent_gos + ';' + go
                else:
                    d[rna_id] = go
        else:
            rna_id = row[1]
            if rna_id in d.keys():
                existent_gos = d[rna_id]
                d[rna_id] = existent_gos + ';' + go
            else:
                d[rna_id] = go
    return d

def get_rna_go_edges(rna_id, gos, l):
    """ Add rna family info to list l
    @param rna_id    string rna id
    @param gos    go terms associated to rna_id
    @param l    the list collecting edges

    @return  the list edges elements
    """
    l.append (
                {
                    'rna_id' : rna_id,
                    'go' : gos,
                    'source' : 'TRAPID',
                    ':TYPE' : 'HAS_ANNOTATION'
                }
    )
    return l

if __name__ == "__main__":

    d_gos = defaultdict(list)
    with open(args.go, 'r', encoding='utf-8') as fl:
        l_edges = []
        for line in fl:
            if line.startswith('#'):
                continue
            d_gos = get_rna_go_dic(line, d_gos, conv_string_to_boolean(args.ignore_hidden_gos))

    for rna in d_gos:
        l_edges = get_rna_go_edges(rna, d_gos[rna], l_edges)
    df_edges = pd.DataFrame(l_edges)

    # WRITE OUTPUT
    col_edges = [':START_ID(rna-ID)', ':END_ID(go-ID)',
        'source:String', ':TYPE']
    df_edges = ensure_df_col_format(df_edges, col_edges)
    df_edges.to_csv(args.output_edges, index = False)