#!/usr/bin/env python3

"""
22/01/27

Integration of protein domains into Neo4j.
From InterProScan (IPS) tsv output, creates csv file of protein domains nodes. Also creates relations between these protein domains and protein nodes.
Information extracted from IPS file
NODES
protein_annotation:
- annotation_id (col 5)
- source (col 4)
- description (col 6)
- interpro_link (col 12)
EDGES
HAS_DOMAIN:
- evalue (col 9)
- start (col 7)
- stop (col 8)
- date (col 11)

Usage: python3 make_protein_annotations_nodes_edges.py -ips lcul.tsv -output_nodes lcul_ips.csv -output_edges edges_protein_ips.csv
"""

import re
import argparse
import pandas as pd

from database_pipeline_functions import ensure_df_col_format

# ARGUMENTS
parser = argparse.ArgumentParser(description = 'Creates neo4j nodes and edges for protein annotation by InterProScan')
parser.add_argument('-ips', help = 'InterProScan tsv output', required= True)
parser.add_argument('-output_nodes', help = 'output of protein annotation nodes in csv format', required=False, default = "ips_nodes.csv")
parser.add_argument('-output_edges', help = 'output of protein to protein annotation edges', required=False, default = "protein_protein_annotation_edges.csv")
args = parser.parse_args()


if __name__ == "__main__":

    l_nodes = []
    l_edges = []

    with open(args.ips, encoding = 'utf-8') as fl:
        s_ids = set()
        for line in fl:
            row = line.strip().split('\t')
            annotation_id = row[4]
            source = row[3]
            if source == 'GENE3D':
                interpro_link = 'https://www.ebi.ac.uk/interpro/entry/cathgene3d/' + annotation_id
            elif source == 'CDD':
                interpro_link =  'https://www.ebi.ac.uk/interpro/entry/cdd/' + annotation_id
            elif source == 'PANTHER':
                interpro_link =  'https://www.ebi.ac.uk/interpro/entry/panther/' + annotation_id
            elif source == 'SUPERFAMILY':
                interpro_link =  'https://www.ebi.ac.uk/interpro/entry/ssf/' + annotation_id
            elif source == 'TIGRFAM':
                interpro_link =  'https://www.ebi.ac.uk/interpro/entry/tigrfams/' + annotation_id
            elif source == 'Coils' or source == 'mobidb-lite':
                # no link of interest found
                interpro_link =  ''
            else:
                # using IPRXXX number by default
                interpro_link = 'https://www.ebi.ac.uk/interpro/entry/InterPro/' + row[11]

            if annotation_id not in s_ids:
                s_ids.add(annotation_id)
                l_nodes.append (
                    {
                        'annotation_id' : annotation_id,
                        'source' : source,
                        'description' : row[5],
                        'interpro_link' : interpro_link,
                        ':LABEL' : 'FunctionalAnnotation;InterPro'
                    }
                )
            l_edges.append (
                {
                    'prot_id' : row[0],
                    'annotation_id' : annotation_id,
                    # evalue useless because from the database, no meaning here
                    # 'evalue' : row[8],
                    'start' : row[6],
                    'end' : row[7],
                    ':TYPE' : 'HAS_ANNOTATION'
                }
            )

    df_nodes = pd.DataFrame(l_nodes)
    df_edges = pd.DataFrame(l_edges)

    # COLUMNS
    col_nodes = ['annotation_id:ID(protein_annotation-ID)', 'source:String',
        'description:String', 'link:String', ':LABEL']
    df_nodes = ensure_df_col_format(df_nodes, col_nodes)
    col_edges = [':START_ID(protein-ID)', ':END_ID(protein_annotation-ID)',
        'start:int', 'end:int', ':TYPE']
    df_edges = ensure_df_col_format(df_edges, col_edges)

    # OUTPUTS
    # WRITE OUTPUTS
    df_nodes.to_csv(args.output_nodes, index = False)
    df_edges.to_csv(args.output_edges, index = False)