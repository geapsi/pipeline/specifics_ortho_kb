#!/usr/bin/env python3

"""
28/02/22

From the mapman ontology .txt file, get terms to create nodes and rna annotated with them

Usage: python3 parse_mapman_nodes_edges.py -mapman Job13744.results.txt -output_nodes nodes_mapman.csv -output_edges edges_rna_mapman.csv
"""

import argparse
import re
from collections import defaultdict
# import urllib.parse #for parsing strings to URI's

# mapman rdf creation
import pandas as pd #for handling csv and csv contents
from rdflib import Graph, Literal, RDF, URIRef, Namespace #basic RDF handling
from rdflib.namespace import FOAF , XSD, OWL, RDFS #most common namespaces


# ARGUMENTS
parser = argparse.ArgumentParser(description = 'From MapMan annotation, extract information to create nodes of terms and link them to associated rnas.')
parser.add_argument('-mapman', help = 'mapman ontology result file obtained using mapman annotation protein fasta file.', required = True)
parser.add_argument('-gff', help = 'annotation file in gff format.', required = True)
parser.add_argument('-skip_bin_35', help = 'bin 35 and children are not annoted terms. Not required in the database.', default = False, action = 'store_true')
parser.add_argument('-output_nodes', help = 'mapman terms nodes in csv format', required = False, default = 'nodes_mapman.csv')
parser.add_argument('-output_edges', help = 'rnas to mapman terms in csv format', required = False, default = 'edges_rna_mapman.csv')
args = parser.parse_args()

# FUNCTIONS

def remove_quotes(string):
    """ Remove quotes from beginning and end of the string
    @param string    column string

    @return  same string but without quotes
    """
    new_str = string[1:-1]
    return new_str


def get_dic_protein_gene_id(gff):
    """ Get dic with lower(protein_id) : [gene, chr, protein_id]
    @param gff    the gff file

    @return  a dic with protein_id : gene_id
    """
    d = defaultdict(list)
    # mRNA/protein ID
    pattern_id = 'ID=(.+?)[;\n]'
    # gene ID
    pattern_parent = 'Parent=(.+?)[;\n]'
    with open(gff, 'r', encoding = 'utf-8') as fl:
        for line in fl:
            if line.startswith('#'):
                continue
            row = line.split('\t')
            if row[2] != 'mRNA':
                continue
            else:
                # get ids from gff
                protein_id = re.search(pattern_id, row[-1]).group(1)
                gene_id = re.search(pattern_parent, row[-1]).group(1)
                # gene_id, chr, protein_id
                protein_id_low = protein_id.lower()
                d[protein_id_low].append(gene_id)
                d[protein_id_low].append(row[0])
                d[protein_id_low].append(protein_id)
    return d

def create_mapman_rdf_graph():

    g = Graph()
    mpm = Namespace('http://mapman.org/term/')
    mpm_desc = Namespace('http://mapman.org/dec/')

# MAIN

if __name__ == "__main__":

    # dic with prot_id : [gene, chr]
    d_gff = get_dic_protein_gene_id(args.gff)

    # write node and edge files
    s_mapman_term = set()
    d_nodes = {}
    d_edges = defaultdict(list)
    g = Graph()
    OWL = Namespace('http://www.w3.org/2002/07/owl#')
    mpm = Namespace('http://mapman.org/term/')
    mapman_label = "mapman"

    with open(args.mapman, 'r', encoding = 'utf-8') as fl:
        for line in fl:
            if line.startswith('#') or line.startswith('BINCODE'):
                continue
            row = line.strip().split('\t')
            # if term mapped to a protein
            term = remove_quotes(row[0])
            # skip "not annotated" and "annotated" mapman bins
            if args.skip_bin_35:
                if term.startswith("35"):
                    continue
            # the 5'-UTR messes things. Only keep the last part of the description since onto in the db
            description = remove_quotes(row[1]).replace("'", "-prime").split('.')[-1]
            # get nodes
            if term not in s_mapman_term:
                s_mapman_term.add(term)
                d_nodes[term] = description
            if row[2] != "''":
                prot_id_low = remove_quotes(row[2])
                if prot_id_low in d_gff.keys():
                    # get rna id with caps if were
                    rna_id = d_gff[prot_id_low][2]
                    #gene_id = d_gff[prot_id_low][0]
                    # write edges
                    d_edges[term].append(rna_id)
        # write nodes
        for term, desc in d_nodes.items():
            # new block
            g.add((URIRef(mpm + term), RDF.type, OWL.Class))
            # add mapman_id
            g.add((URIRef(mpm + term), OWL.name, Literal(term)))
            # label for neo4j
            g.add((URIRef(mpm + term), RDF.type, OWL.MapMan))
            # term description
            g.add((URIRef(mpm + term), RDFS.label, Literal(desc, datatype=XSD.string) ))
            g.bind('mapman', OWL)
            # create relationship between child and parent if not orphan
            l_previous_term = term.rsplit('.', 1)
            if len(l_previous_term) > 1:
                g.add((URIRef(mpm + term), RDFS.subClassOf, URIRef(mpm + l_previous_term[0])))
        g.serialize(args.output_nodes,format = 'turtle')

        # write edges
        with open(args.output_edges, 'w', encoding = 'utf-8') as ofl_edges:
            ofl_edges.write(':START_ID(rna-ID)' + ',' + ':END_ID(mapman_annotation-ID)' + ',' + ':TYPE' + '\n')
            for term, rnas in d_edges.items():
                ofl_edges.write(';'.join(rnas) + ',' + term + ',' + 'HAS_ANNOTATION' + '\n')
