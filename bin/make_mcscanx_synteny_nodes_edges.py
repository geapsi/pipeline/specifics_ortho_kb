#!/usr/bin/env python3

"""
2021/10/27
Creates neo4j nodes (csv format) from the collinearity file of MCScanX

Usage: python3 make_mcscanx_synteny_nodes.py -collinearity synt.collinearity  -output lcul_synteny_nodes.csv
"""

import argparse
import re
from collections import defaultdict

import pandas as pd
import pybedtools

from database_pipeline_functions import ensure_df_col_format

# ARGPARSE
parser = argparse.ArgumentParser(description = 'Obtain synteny nodes from \
    MCScanX collinearity file for use in neo4j')
parser.add_argument('-collinearity',
    help = 'input MCScanX collinearity file',
    required = True)
parser.add_argument('-tandem',
    help = 'input MCScanX tandem file',
    required = False)
parser.add_argument('-merged_gff',
    help = 'concatenation of all gff files from species included in the \
    collinearity file with additional first column being the species acronym',
    required = True)
parser.add_argument('-block_prefix',
    help = 'prefix for block ids (will be added a "_" at the end \
    (default is date: yyyy-mm-dd)',
    required = False, default = 'mcscanx_block')
parser.add_argument('-output_nodes_synteny',
    help = 'output file name of mcscanx syntenic blocks (nodes, .csv)',
    required = False, default = 'synteny_block_nodes.csv')
parser.add_argument('-output_nodes_bioinfo_protocol',
    help = 'output file name of mcscanx parameters (nodes, .csv)',
    required = False, default = 'synteny_experiment_nodes.csv')
parser.add_argument('-output_edges_gene_synteny',
    help = 'output file name of gene to synteny (edges, .csv)',
    required = False, default = 'gene_synteny_edges.csv')
parser.add_argument('-output_edges_tandem_gene_synteny',
    help = 'output file name of tandem gene to synteny (edges, .csv)',
    required = False, default = 'tandem_gene_synteny_edges.csv')
parser.add_argument('-output_edges_synteny_chromosome',
    help = 'output file name of synteny to chromosome (edges, .csv)',
    required = False, default = 'synteny_chromosome_edges.csv')
parser.add_argument('-output_edges_synteny_bioinfo_protocol',
    help = 'output file name of mcscanx syntenic blocks (nodes, .csv)',
    required = False, default = 'synteny_experiment_edges.csv')
args = parser.parse_args()

# FUNCTIONS

def collect_rna_info(line):
    """ Creates a list with parent and feature id (mRNA feature)
    @param line  line of the gff file

    @return a list with rna_id, gene_id, species
    """
    # carefull all rows are shifted by +1 (first column is species)
    pattern_id = 'ID=(.+?)[;\n]'
    pattern_parent_id = 'Parent=(.+?)[;\n]'
    row = line.split('\t')
    m_id = re.search(pattern_id, row[9])
    m_parent_id = re.search(pattern_parent_id, row[9])
    if m_id and m_parent_id:
        rna_id = m_id.group(1)
        gene_id = m_parent_id.group(1)
        species = row[0]
        l_gff_line = [rna_id, gene_id, species]
    else:
        print("Missing \n", row)
        print("Continue")
        return None
    return l_gff_line

def collect_gene_info(line):
    """ Creates a list with gene info (gene feature)
    @param line  line of the gff file

    @return a list with gene_id, chr, start, stop and species
    """
    # carefull all rows are shifted by +1 (first column is species)
    pattern_id = 'ID=(.+?)[;\n]'
    row = line.split('\t')

    m_id = re.search(pattern_id, row[9])
    if m_id:
        gene_id = m_id.group(1)
        species = row[0]
        chrom = row[1]
        start = int(row[4])
        end = int(row[5])
        if start > end:
            tmp = start
            start = end
            end = tmp
        l_gff_line = [gene_id, species, chrom, start, end]
    else:
        # missing row
        return None
    return l_gff_line

def get_mcscanx_options(line, d):
    """ From collinearity file extracts parameters of mcscanx run
    @param line line from collinearity file
    @param d  dictionnary storing parameters

    @return  all d with found parameter on the line
    """
    pattern = r'# (.+):'
    parameter = re.search(pattern, line).group(1).replace(' ', '_').lower()
    d[parameter] = line.split(' ')[-1].strip()

    return d

def get_run_statistics(line, d):
    """ From collinearity file line extracts stats of mcscanx run
    @param line line from collinearity file
    @param d  dictionnary storing stats

    @return  all d with found stats on the line
    """
    pattern = ' (.+):'
    row = line.strip().split(',')
    for s in row:
        if s != '':
            stat = re.search(pattern, s).group(1).lower().replace(' ', '_')
            d[stat] = s.split(':')[-1].strip()
    return d

def get_block_info(line, prefix):
    """ From collinearity file line extracts block information of mcscanx run
    @param line line from collinearity file
    @param prefix add the prefix before the block id to ensure unicity

    @return  all d with found information on the block
    """
    d = {}
    pattern_alignment = r'Alignment ([0-9]+):'
    pattern_score = r'score=([0-9-\.]+)'
    pattern_evalue = r'e_value=([0-9-\.-e-]+) '
    pattern_nb = r'N=([0-9]+) '
    pattern_chr = r'N=[0-9]+ (.+) '
    pattern_strand = r' (plus|minus)'

    row = line.strip()
    chromosome = re.search(pattern_chr, row).group(1)

    d['block_id'] = prefix + '_' + re.search(pattern_alignment, row).group(1)
    d['score'] = re.search(pattern_score, row).group(1)
    d['evalue'] = re.search(pattern_evalue, row).group(1)
    d['nb_genes'] = re.search(pattern_nb, row).group(1)
    d['chrA'] = chromosome.split('&')[0]
    d['chrB'] = chromosome.split('&')[1]
    d['flipped'] = re.search(pattern_strand, row).group(1).replace('plus', 'False').replace('minus', 'True')

    return d

def get_block_genes(line, l, d_rna_gene, prefix):
    """ From collinearity file line extracts block genes of mcscanx run
    @param line line from collinearity file
    @param l list to store dic of genes
    @param d_rna_gene dic with rna as key and gene as value
    @param prefix add the prefix before the block id to ensure unicity

    @return  all d with found information on the block
    """
    row = line.strip().split('\t')
    block_id = prefix + '_' + row[0].split('-')[0]
    gene_pair = row[0].split('-')[1].replace(':', ' ').strip()
    evalue = row[3].strip()
    l.append(
        {
        'gene': d_rna_gene[row[1]],
        'block_id': block_id,
        'evalue': evalue,
        'gene_pair': gene_pair
        }
    )
    l.append(
        {
        'gene': d_rna_gene[row[2]],
        'block_id': block_id,
        'evalue': evalue,
        'gene_pair': gene_pair
        }
    )
    return l

def get_block_limits(df_edges_gene_synteny, df_genes):
    """ Get minimal and maximal coordinate of genes on each chr of the block
    @param df_edges_gene_synteny df with genes belonging to each block
    @param df_genes df of genes with coordinates info

    @return  blocks with coordinates on each chr
    """
    df_merge = pd.merge(df_edges_gene_synteny, df_genes, on='gene', how='left')

    df_block_start = df_merge.groupby(['block_id', 'chromosome', 'species'])['start'].min().reset_index()
    df_block_end = df_merge.groupby(['block_id', 'chromosome', 'species'])['end'].max().reset_index()
    df_block_limits = pd.merge(df_block_start, df_block_end,
        how='left', on=['block_id', 'species', 'chromosome'])
    return df_block_limits

def parse_tandem(fl_tandem, d_rna_gene):
    """ From MCScanX tandem file, get the list of tandem genes
    @param fl_tandem a file object
    @param d_rna_gene a dic with rna_id as key and gene_id as value

    @return  a set containing all genes from the file
    """
    s_genes = set()
    for line in fl_tandem:
        if ',' in line:
            l_line = line.strip().split(',')
            for rna in l_line:
                s_genes.add(d_rna_gene[rna])
    return s_genes

def add_tandem(s_tandem, df_genes, df_block_limits):
    """ Locates tandem located inside syntenic blocks
    @param fl_tandem a file object

    @return  a set containing all genes from the file
    """
    df_tandem = pd.DataFrame(s_tandem)
    df_tandem.columns = ['gene']
    df_tandem_coordinates = pd.merge(df_tandem, df_genes,
        how = 'left', on = 'gene')
    df_tandem_coordinates_format = df_tandem_coordinates[['chromosome', 'start',
        'end', 'gene']]
    bed_tandem = pybedtools.\
        BedTool.from_dataframe(df_tandem_coordinates_format)
    df_block_limits_format = df_block_limits[['chromosome', 'start',
        'end', 'block_id']]
    bed_block_limits = pybedtools.\
        BedTool.from_dataframe(df_block_limits_format)
    min_fraction_of_tandem_gene_overlap = 1
    df_tandem_in_blocks = pybedtools.BedTool.intersect(bed_tandem,
        bed_block_limits, f = min_fraction_of_tandem_gene_overlap,
        wo = True).to_dataframe()
    # take gene and synt block
    df_tandem_in_blocks = df_tandem_in_blocks[['name', 'thickEnd']]
    df_tandem_in_blocks.columns = ['gene', 'block_id']
    return df_tandem_in_blocks

# MAIN
if __name__ == "__main__":

    # creating dictionnary from species_chromosomes file
    with open(args.merged_gff, 'r', encoding = 'utf-8') as fl:
        #d_gff_info = {}
        d_rna_gene = {}
        d_gene_info = defaultdict(list)
        for line in fl:
            if line.startswith('species') or line.startswith('#'):
                continue
            if line.split('\t')[3] == 'mRNA':
                l_rna_gene = collect_rna_info(line)
                if l_rna_gene is not None:
                    d_rna_gene[l_rna_gene[0]] = l_rna_gene[1]
            elif line.split('\t')[3] == 'gene':
                l_gene_info = collect_gene_info(line)
                if l_gene_info is not None:
                    d_gene_info[l_gene_info[0]] = l_gene_info[1:]


    # parsing collinearity file
    with open(args.collinearity,'r', encoding = 'utf-8') as fl:
        d_param = {}
        d_stat = {}
        l_block_info = []
        l_block_genes = []

        block_number = 0

        for line in fl:
            if line.startswith('#'):
                if 'Parameters' in str(line):
                    param_section = True
                    statistics_section = False
                    param_buffer = 6
                elif ('Statistics' in str(line)):
                    param_section = False
                    statistics_section = True
                    stat_buffer = 2
                elif (param_section == True):
                    # get MCScanX parameters information
                    d_param = get_mcscanx_options(line, d_param)
                elif ((param_section == False) and stat_buffer > 0):
                    # get run statistics
                    d_stat = get_run_statistics(line, d_stat)
                    stat_buffer -= 1
                    if stat_buffer == 0:
                            statistics_section = False
                elif 'alignment' in line.lower() and param_section == False and statistics_section == False:
                    # get blocks nodes
                    l_block_info.append(get_block_info(line, args.block_prefix))
                    block_number += 1
            else:
                # adding genes in the list of the block
                l_block_genes = get_block_genes(line, l_block_genes, d_rna_gene, args.block_prefix)

                # check if block intra or inter species, each pair divided in two dics in l_block_genes
                if l_block_genes[-1]['gene_pair'] == '0':
                    gene_spA = l_block_genes[-2]['gene']
                    sp_spA = d_gene_info[gene_spA][0]
                    gene_spB = l_block_genes[-1]['gene']
                    sp_spB = d_gene_info[gene_spB][0]
                    if sp_spA == sp_spB:
                        l_block_info[-1]['label'] = 'Intra;Synteny;Block'
                    else:
                        l_block_info[-1]['label'] = 'Inter;Synteny;Block'
    # ensure correct type for coordinates
    df_genes_types = {
        'gene': 'object',
        'species': 'object',
        'chromosome': 'object',
        'start': 'float',
        'end': 'float'
    }
    # pd cannot switch directly to Int64
    force_coordinates_types = {
        'start': 'Int64',
        'end': 'Int64'
    }
    df_genes = pd.DataFrame.from_dict(d_gene_info, orient = 'index').reset_index()
    df_genes.columns = ['gene', 'species', 'chromosome', 'start', 'end']
    df_genes = df_genes.astype(df_genes_types).astype(force_coordinates_types)

    # protocol nodes
    # merging parameters and statistics about the run to make nodes
    df_param = pd.DataFrame.from_dict(d_param, orient = 'index').transpose()
    df_stat = pd.DataFrame.from_dict(d_stat, orient = 'index').transpose()
    df_nodes_protocol = df_param.join(df_stat)
    protocol_id = 'synteny_protocol'
    df_nodes_protocol.insert(0, 'protocol_id', protocol_id)
    df_nodes_protocol['software'] = 'mcscanx'
    df_nodes_protocol[':LABEL'] = 'Protocol;Synteny'

    df_nodes_synteny = pd.DataFrame(l_block_info)
    df_edges_gene_synteny = pd.DataFrame(l_block_genes)
    # get block limits on each chr
    df_block_limits = get_block_limits(df_edges_gene_synteny, df_genes)
    df_edges_synteny_chromosome = df_block_limits[['block_id', 'chromosome', 'start', 'end']]

    # adding tandem genes belonging to the SB interval
    if args.tandem:
        with open(args.tandem, 'r', encoding = 'utf-8') as fl:
            s_tandem_genes = parse_tandem(fl, d_rna_gene)
        df_tandem_genes_in_block = add_tandem(s_tandem_genes, df_genes,
            df_block_limits)
        # ignore genes in .tandem but already in .collinearity
        df_edges_tandem_gene_synteny = df_tandem_genes_in_block[
            ~df_tandem_genes_in_block['gene'].isin(df_edges_gene_synteny['gene'])
            ]

    # edges synteny block to protocol
    df_edges_synteny_protocol = pd.DataFrame()
    df_edges_synteny_protocol['block_id'] = df_nodes_synteny['block_id'].copy()
    df_edges_synteny_protocol.loc[:,'protocol_id'] = df_nodes_protocol.loc[0,'protocol_id']

    # WRITING FILES (NODES AND EDGES)
    ## nodes
    col_synteny_nodes = ['block_id:ID(synteny_block-ID)', 'score:double',
        'evalue:double', 'nb_genes:long', 'chrA:String', 'chrB:String',
        'flipped:String', ':LABEL']
    df_nodes_synteny.columns = col_synteny_nodes
    df_nodes_synteny.to_csv(args.output_nodes_synteny, index = False, header = True)

    col_protocol_nodes = ['protocol_id:ID(bioinfo_protocol-ID)',
        'match_score:long', 'match_size:long', 'gap_penalty:double',
        'overlap_window:long', 'evalue:float','max_gaps:long',
        'number_of_collinear_genes:long', 'percentage:float',
        'number_of_all_genes:long', 'software:String', ':LABEL']
    df_nodes_protocol.columns = col_protocol_nodes
    df_nodes_protocol.to_csv(args.output_nodes_bioinfo_protocol, index = False, header = True)

    ## edges
    col_gene_synteny_edges = [':START_ID(gene-ID)', ':END_ID(synteny_block-ID)',
        'evalue:double', 'gene_pair:long']
    df_edges_gene_synteny = ensure_df_col_format(df_edges_gene_synteny, col_gene_synteny_edges)
    df_edges_gene_synteny[':TYPE'] = 'PART_OF'
    df_edges_gene_synteny.to_csv(args.output_edges_gene_synteny, index = False, header = True)

    if args.tandem:
        col_tandem_gene_synteny_edges = [':START_ID(gene-ID)',
            ':END_ID(synteny_block-ID)']
        df_edges_tandem_gene_synteny = ensure_df_col_format(df_edges_tandem_gene_synteny,
            col_tandem_gene_synteny_edges)
        df_edges_tandem_gene_synteny[':TYPE'] = 'LOCATED_IN'
        df_edges_tandem_gene_synteny.to_csv(args.output_edges_tandem_gene_synteny,
            index=False, header=True)

    col_synteny_chromosome_edges = [':START_ID(synteny_block-ID)',
        ':END_ID(chromosome-ID)', 'start:long', 'end:long']
    df_edges_synteny_chromosome = ensure_df_col_format(df_edges_synteny_chromosome,
        col_synteny_chromosome_edges)
    df_edges_synteny_chromosome[':TYPE'] = 'LOCATED_ON'
    df_edges_synteny_chromosome.to_csv(args.output_edges_synteny_chromosome, index = False, header = True)

    col_synteny_protocol_edges = [':START_ID(synteny_block-ID)', ':END_ID(bioinfo_protocol-ID)']
    df_edges_synteny_protocol = ensure_df_col_format(df_edges_synteny_protocol,
        col_synteny_protocol_edges)
    df_edges_synteny_protocol[':TYPE'] = 'PROCESSED_WITH'
    df_edges_synteny_protocol.to_csv(args.output_edges_synteny_bioinfo_protocol, index = False, header = True)