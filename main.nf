#!/usr/bin/env nextflow

include { prepare_genome_files } from './subworkflows/prepare_genome_files.nf'
include { prepare_annotation_files } from './subworkflows/prepare_annotation_files.nf'

include { copyFilesForTracking as copySpeciesFiles } from './modules/copyFilesForTracking.nf'
include { copyFilesForTracking as copyAnnotationFiles } from './modules/copyFilesForTracking.nf'
include { copyFilesForTracking as copyTranscriptomicFiles } from './modules/copyFilesForTracking.nf'
include { copyFilesForTracking as copyQtlFiles } from './modules/copyFilesForTracking.nf'
include { copyFilesForTracking as copySpeciesInfoFiles } from './modules/copyFilesForTracking.nf'
include { getGeneFamilies } from './modules/getGeneFamilies.nf'
include { getGenomeInfo } from './modules/getGenomeInfo.nf'
include { getGeneToRnaEdges } from './modules/getGeneToRnaEdges.nf'
include { getInterProAnnotation } from './modules/getInterProAnnotation.nf'
include { getRnaToGoEdges } from './modules/getRnaToGoEdges.nf'
include { getMapManAnnotation } from './modules/getMapManAnnotation.nf'
include { getProteinNodes } from './modules/getProteinNodes.nf'
include { getRnaFamilies } from './modules/getRnaFamilies.nf'
include { getRnaNodes } from './modules/getRnaNodes.nf'
include { getRnaToProteinEdges } from './modules/getRnaToProteinEdges.nf'
include { getSpeciesGFF } from './modules/getSpeciesGFF.nf'
include { getSyntenyInfo } from './modules/getSyntenyInfo.nf'
include { getHomologyInfo } from './modules/getHomologyInfo.nf'
include { getTranscriptomicCounts } from './modules/getTranscriptomicCounts.nf'
include { getQTLInfo } from './modules/getQTLInfo.nf'
include { extractTranscripts } from './modules/extractTranscripts.nf'
include { createMissingOntologyTerms } from './modules/createMissingOntologyTerms.nf'

/*
========================================================================================
    specifics_Ortho_KB
========================================================================================
    GitLab : https://forgemia.inra.fr/geapsi/pipeline/specifics_ortho_kb
----------------------------------------------------------------------------------------
*/


log.info "paths from file : ${params.species_files}"

def helpMessage() {
    log.info"""
    ==========================================
    Ortho_KB database creation pipeline

    Usage:
    nextflow run main.nf -c conf/example_data.config --outdir results/example_data
    -------------------------------------------
    See other optional arguments in the config file and prefix them with a double dash (i.e. --OF_log ./input/orthofinder/Log.txt)
    """
    .stripIndent()
}

// Show help message
if (params.help) {
    helpMessage()
    exit(0)
}

def merge_results(tpl, subdir) {
    // create merged files (collect all species/datasets in one file)
    // hide some files in subdir (will be manually imported in Neo4j later)
    tpl
        .collectFile(storeDir: params.outdir + '/' + params.merged_dir + '/' + subdir)
}

workflow {

    if(!params.species_info) {
        error("please specify --species_info option")
    }
    sp_info_ch = Channel
        .fromPath(params.species_info, checkIfExists: false)

    prepare_genome_files(params.species_files)
    prepare_genome_files.out.gff3
        .join(prepare_genome_files.out.protein)
        .join(prepare_genome_files.out.chr_conv)
        .join(prepare_genome_files.out.fai)
        .multiMap { ID, gff3, protein, chr_conv, fai ->
            gff3: [ID, gff3]
            protein: [ID, protein]
            chr_conv: [ID, chr_conv]
            fai: [ID, fai]
        }
        .set { samples_ch }

    prepare_annotation_files(params.annotation_files, samples_ch.protein)
    prepare_annotation_files.out.eggnog
        .join(prepare_annotation_files.out.mapman)
        .join(prepare_annotation_files.out.interproscan)
        .join(prepare_annotation_files.out.trapid_go)
        .join(prepare_annotation_files.out.trapid_gf)
        .join(prepare_annotation_files.out.trapid_rf)
        .multiMap { ID, eggnog, mapman, interproscan, trapid_go, trapid_gf, trapid_rf ->
            eggnog: [ID, eggnog]
            mapman: [ID, mapman]
            interproscan: [ID, interproscan]
            trapid_go: [ID, trapid_go]
            trapid_gf: [ID, trapid_gf]
            trapid_rf: [ID, trapid_rf]
        }
        .set { annotation_ch }

    copySpeciesFiles(Channel.fromPath(params.species_files, checkIfExists: false).map { tuple('species', it) })
    copyAnnotationFiles(Channel.fromPath(params.annotation_files, checkIfExists: false).map { tuple('annotation', it) })


    if(params.transcriptomic_files ) {
        //-------------------------------------------------//
        //            Get transcriptomics info             //
        //-------------------------------------------------//
        transcriptomic_f_ch = Channel
            .fromPath(params.transcriptomic_files, checkIfExists: false)
            .splitCsv(header:true, sep:"\t", strip:true)
            .map { row -> tuple(row.ID, row.status, file(row.bioinfo_protocol), file(row.dataset), file(row.counts), file(row.tpm), file(row.metadata), file(row.samples_annotation)) }
            .set { transcriptomics_ch }

        getTranscriptomicCounts(transcriptomics_ch)

        merge_results(getTranscriptomicCounts.out.bioinfo_protocol, subdir = '')
        merge_results(getTranscriptomicCounts.out.dataset, subdir = '')
        merge_results(getTranscriptomicCounts.out.condition, subdir = '')
        merge_results(getTranscriptomicCounts.out.free_term, subdir = '')
        merge_results(getTranscriptomicCounts.out.sample, subdir = '')
        merge_results(getTranscriptomicCounts.out.dataset_bioinfo_protocol, subdir = '')
        merge_results(getTranscriptomicCounts.out.condition_dataset, subdir = '')
        merge_results(getTranscriptomicCounts.out.sample_condition, subdir = '')
        merge_results(getTranscriptomicCounts.out.gene_sample, subdir = '')
        merge_results(getTranscriptomicCounts.out.gene_condition, subdir = '')
        merge_results(getTranscriptomicCounts.out.condition_onto, subdir = params.merged_subdir)
        merge_results(getTranscriptomicCounts.out.condition_free_term, subdir = '')

        copyTranscriptomicFiles(Channel.fromPath(params.transcriptomic_files, checkIfExists: false).map { tuple('transcriptomic', it) })
    }

    if(params.qtl_files) {
        //-------------------------------------------------//
        //            Get QTL information                  //
        //-------------------------------------------------//
        qtl_f_ch = Channel
            .fromPath(params.qtl_files, checkIfExists: false)
            .splitCsv(header:true, sep:"\t", strip:true)
            .map { row -> tuple(row.ID, row.status, row.species, file(row.dataset), file(row.qtl), file(row.qtl_annotation)) }
            .set { qtl_ch }

        // need to add the gff3 and chr_conv table to all datasets lines of the qtl
        // table, since multiple datasets for each species, join is not possible
        qtl_ch
            .map { tuple( it[2], *it ) }
            .combine( samples_ch.gff3, by: 0 )
            .combine( samples_ch.chr_conv, by: 0 )
            .map { it[1..-1] }
            .set { qtl_genome_ch }


        getQTLInfo(qtl_genome_ch)

        merge_results(getQTLInfo.out.qtl, subdir = '')
        merge_results(getQTLInfo.out.dataset, subdir = '')
        merge_results(getQTLInfo.out.population, subdir = '')
        merge_results(getQTLInfo.out.site, subdir = '')
        merge_results(getQTLInfo.out.year, subdir = '')
        merge_results(getQTLInfo.out.free_term, subdir = '')
        merge_results(getQTLInfo.out.gene_qtl, subdir = '')
        merge_results(getQTLInfo.out.qtl_chromosome, subdir = '')
        merge_results(getQTLInfo.out.qtl_dataset, subdir = '')
        merge_results(getQTLInfo.out.qtl_population, subdir = '')
        merge_results(getQTLInfo.out.qtl_site, subdir = '')
        merge_results(getQTLInfo.out.qtl_year, subdir = '')
        merge_results(getQTLInfo.out.qtl_onto, subdir = params.merged_subdir)
        merge_results(getQTLInfo.out.qtl_free_term, subdir = '')

        copyQtlFiles(Channel.fromPath(params.qtl_files, checkIfExists: false).map { tuple('qtl', it) })
    }

    // copy input files to results directory
    copySpeciesInfoFiles(Channel.fromPath(params.species_info, checkIfExists: false).map { tuple('species_info', it) })

    //-------------------------------------------------//
    //      Get basic genetic nodes and edges          //
    //-------------------------------------------------//
    // get CDS prediction
    getGenomeInfo(samples_ch.gff3.join(samples_ch.chr_conv).join(samples_ch.fai).join(annotation_ch.eggnog).combine(sp_info_ch))
    getRnaNodes(samples_ch.gff3.combine(sp_info_ch))
    getProteinNodes(samples_ch.protein.combine(sp_info_ch))

    getGeneToRnaEdges(samples_ch.gff3)
    // TODO create a single script to make gene->rna and rna->protein
    getRnaToProteinEdges(getRnaNodes.out.nodes_rna.join(getProteinNodes.out.nodes_protein))

    //-------------------------------------------------//
    //     Get functional annot nodes and edges        //
    //-------------------------------------------------//
    getRnaToGoEdges(annotation_ch.trapid_go)
    getGeneFamilies(samples_ch.gff3.join(annotation_ch.trapid_gf))
    getRnaFamilies(annotation_ch.trapid_rf)
    getMapManAnnotation(samples_ch.gff3.join(annotation_ch.mapman))
    getInterProAnnotation(annotation_ch.interproscan)

    //-------------------------------------------------//
    //   Get orthology and synteny information         //
    //-------------------------------------------------//

    // merge all gff files together (for orthogroups parsing)
    samples_ch.gff3
        .collectFile(name: 'merged_gff', newLine: false) { it[1] }
        .set { merged_gff_ch }

    // orthology
    if(!params.OF_N0) {
        error("please specify --OF_N0 option")
    }
    N0_ch = Channel
        .fromPath(params.OF_N0, checkIfExists: false)
    if(!params.OF_log) {
        error("please specify --OF_log option")
    }
    log_OF_ch = Channel
        .fromPath(params.OF_log, checkIfExists: false)
    getHomologyInfo(N0_ch, merged_gff_ch, log_OF_ch)

    // synteny
    s_collinearity = Channel
        .fromPath(params.mcscanx_collinearity, checkIfExists: false)
        .ifEmpty { exit 1, "please specify --mcscanx_collinearity option" }
    s_tandem = Channel
        .fromPath(params.mcscanx_tandem, checkIfExists: false)
        .ifEmpty { exit 1, "please specify --mcscanx_tandem option" }
    getSpeciesGFF(samples_ch.gff3)
    getSpeciesGFF.out
        .collectFile(name: 'merged_species_chr.tsv', newLine: false)
        .set { merged_species_chr_ch  }
    getSyntenyInfo(merged_species_chr_ch, s_collinearity, s_tandem)


    //-------------------------------------------------//
    //        Create missing ontology terms            //
    //-------------------------------------------------//

    if(params.new_ontology_terms) {
        new_ontology_terms_ch = Channel
            .fromPath(params.new_ontology_terms, checkIfExists:false)
            .ifEmpty { exit 1, "please specify --new_ontology_terms option" }
        createMissingOntologyTerms(new_ontology_terms_ch)
    }

    //-------------------------------------------------//
    //         Merge result file for import            //
    //-------------------------------------------------//
    // genome info (gene, genome, accession, species)
    merge_results(getGenomeInfo.out.gene, subdir = '')
    merge_results(getGenomeInfo.out.chromosome, subdir = '')
    merge_results(getGenomeInfo.out.genome, subdir = '')
    merge_results(getGenomeInfo.out.accession, subdir = '')
    merge_results(getGenomeInfo.out.species, subdir = '')
    merge_results(getGenomeInfo.out.gene_chromosome, subdir = '')
    merge_results(getGenomeInfo.out.chromosome_genome, subdir = '')
    merge_results(getGenomeInfo.out.genome_accession, subdir = '')
    merge_results(getGenomeInfo.out.accession_species, subdir = '')
    // rna nodes
    merge_results(getRnaNodes.out.rna, subdir = '')
    // protein nodes
    merge_results(getProteinNodes.out.protein, subdir = '')
    // gene to rna edges
    merge_results(getGeneToRnaEdges.out.gene_rna, subdir = '')
    // rna to protein edges
    merge_results(getRnaToProteinEdges.out.rna_protein, subdir = '')
    // gene families
    merge_results(getGeneFamilies.out.gene_family, subdir = '')
    merge_results(getGeneFamilies.out.gene_gene_family, subdir = '')
    // rna families
    merge_results(getRnaFamilies.out.rna_family, subdir = '')
    merge_results(getRnaFamilies.out.rna_rna_family, subdir = '')
    // go
    merge_results(getRnaToGoEdges.out.rna_go, subdir = params.merged_subdir)
    // mapman
    merge_results(getMapManAnnotation.out.rna_mapman, subdir = params.merged_subdir)
    // interpro
    merge_results(getInterProAnnotation.out.interpro, subdir = '')
    merge_results(getInterProAnnotation.out.protein_interpro, subdir = '')
}
