#!/usr/bin/env nextflow

include { gunzip as gunzipGFF } from '../modules/gunzip.nf'
include { gunzip as gunzipProtein } from '../modules/gunzip.nf'
include { gunzip as gunzipChrConv } from '../modules/gunzip.nf'
include { gunzip as gunzipFAI } from '../modules/gunzip.nf'
include { checkGFF } from '../modules/checkGFF.nf'


workflow prepare_genome_files {
    
    take:
    genome_files // file with one line per species, with columns ID, gff3, protein, converted_chr_names, fai
    
    main:

    Channel
        .fromPath(genome_files, checkIfExists: true)
        .splitCsv(header:true, sep:"\t", strip:true)
        .map { row -> tuple(row.ID, file(row.gff3), file(row.protein), file(row.converted_chr_names), file(row.fai)) }
        .multiMap {
            gff3:     [it[0], it[1]]
            protein:  [it[0], it[2]]
            chr_conv: [it[0], it[3]]
            fai:      [it[0], it[4]]
        }
        .set { genome_data_ch }

        // CHECK IF GZ COMPRESSED, IF SO UNCOMPRESS
        gff3_for_gunzip_ch = genome_data_ch.gff3
            .branch { ID, gff3 ->
                gunzip: gff3.name.endsWith('.gz')
                skip: true
            }
        gunzipGFF(gff3_for_gunzip_ch.gunzip)
        gunzipGFF.out.gunzip.mix(gff3_for_gunzip_ch.skip)
            .set { uncompressed_gff3_ch }

        protein_for_gunzip_ch = genome_data_ch.protein
            .branch { ID, protein ->
                gunzip: protein.name.endsWith('.gz')
                skip: true
            }
        gunzipProtein(protein_for_gunzip_ch.gunzip)
        gunzipProtein.out.gunzip.mix(protein_for_gunzip_ch.skip)
            .set { uncompressed_protein_ch }

        chr_conv_for_gunzip_ch = genome_data_ch.chr_conv
            .branch { ID, chr_conv ->
                gunzip: chr_conv.name.endsWith('.gz')
                skip: true
            }
        gunzipChrConv(chr_conv_for_gunzip_ch.gunzip)
        gunzipChrConv.out.gunzip.mix(chr_conv_for_gunzip_ch.skip)
            .set { uncompressed_chr_conv_ch }
        
        fai_for_gunzip_ch = genome_data_ch.fai
            .branch { ID, fai ->
                gunzip: fai.name.endsWith('.gz')
                skip: true
            }
        gunzipFAI(fai_for_gunzip_ch.gunzip)
        gunzipFAI.out.gunzip.mix(fai_for_gunzip_ch.skip)
            .set { uncompressed_fai_ch }

        if ( params.check_gff ) {
        checkGFF(uncompressed_gff3_ch)
        uncompressed_gff3_ch = checkGFF.out.gff3
        }

        emit:
        gff3 = uncompressed_gff3_ch
        protein = uncompressed_protein_ch
        chr_conv = uncompressed_chr_conv_ch
        fai = uncompressed_fai_ch

}