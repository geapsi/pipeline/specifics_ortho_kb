#!/usr/bin/env nextflow

include { gunzip as gunzipEggNOG } from '../modules/gunzip.nf'
include { gunzip as gunzipMapMan } from '../modules/gunzip.nf'
include { gunzip as gunzipInterProScan } from '../modules/gunzip.nf'
include { gunzip as gunzipTrapidGO } from '../modules/gunzip.nf'
include { gunzip as gunzipTrapidGF } from '../modules/gunzip.nf'
include { gunzip as gunzipTrapidRF } from '../modules/gunzip.nf'
include { eggnogDownloadDB } from '../modules/eggnogDownloadDB.nf'
include { getEggnogAnnotation } from '../modules/getEggnogAnnotation.nf'


workflow prepare_annotation_files {
    
    take:
    annotation_files // file with one line per species, with columns ID, eggnog, mapman, interproscan, trapid_go, trapid_gf, trapid_rf
    protein // to get eggNOG annotations if the eggNOG annotation file is not directly provided
    
    main:

    Channel
        .fromPath(annotation_files, checkIfExists: true)
        .splitCsv(header:true, sep:"\t", strip:true)
        .map { row -> tuple(row.ID, file(row.eggnog), file(row.mapman), file(row.interproscan), file(row.trapid_go), file(row.trapid_gene_family), file(row.trapid_rna_family)) }
        .multiMap {
            eggnog:       [it[0], it[1]]
            mapman:       [it[0], it[2]]
            interproscan: [it[0], it[3]]
            trapid_go:    [it[0], it[4]]
            trapid_gf:    [it[0], it[5]]
            trapid_rf:    [it[0], it[6]]
        }
        .set { genome_data_ch }

        // CHECK IF GZ COMPRESSED, IF SO UNCOMPRESS

        if ( params.eggnog_files ) {
        // user provided eggnog files
        println( "Using user provided eggnog files" )
        eggnog_for_gunzip_ch = genome_data_ch.eggnog
            .branch { ID, eggnog ->
                gunzip: eggnog.name.endsWith('.gz')
                skip: true
            }
        gunzipEggNOG(eggnog_for_gunzip_ch.gunzip)
        gunzipEggNOG.out.gunzip.mix(eggnog_for_gunzip_ch.skip)
            .set { uncompressed_eggnog_ch }
        }
        else if ( params.make_eggnog_db ) {
            // user wants eggNOG DB construction
            eggnogDownloadDB()
            getEggnogAnnotation(samples_ch.gff3, eggnogDownloadDB.out)
            getEggnogAnnotation.out.eggnog
                .set { uncompressed_eggnog_ch }
        }
        else {
            // eggnog run with provided DB
            eggnog_db_ch = Channel
                .fromPath(params.eggnog_db)

            getEggnogAnnotation(samples_ch.gff3, eggnog_db_ch.first())
            getEggnogAnnotation.out.eggnog
                .set { uncompressed_eggnog_ch }
        }

        mapman_for_gunzip_ch = genome_data_ch.mapman
            .branch { ID, mapman ->
                gunzip: mapman.name.endsWith('.gz')
                skip: true
            }
        gunzipMapMan(mapman_for_gunzip_ch.gunzip)
        gunzipMapMan.out.gunzip.mix(mapman_for_gunzip_ch.skip)
            .set { uncompressed_mapman_ch }

        interproscan_for_gunzip_ch = genome_data_ch.interproscan
            .branch { ID, interproscan ->
                gunzip: interproscan.name.endsWith('.gz')
                skip: true
            }
        gunzipInterProScan(interproscan_for_gunzip_ch.gunzip)
        gunzipInterProScan.out.gunzip.mix(interproscan_for_gunzip_ch.skip)
            .set { uncompressed_interproscan_ch }
        
        trapid_go_for_gunzip_ch = genome_data_ch.trapid_go
            .branch { ID, trapid_go ->
                gunzip: trapid_go.name.endsWith('.gz')
                skip: true
            }
        gunzipTrapidGO(trapid_go_for_gunzip_ch.gunzip)
        gunzipTrapidGO.out.gunzip.mix(trapid_go_for_gunzip_ch.skip)
            .set { uncompressed_trapid_go_ch }

        trapid_gf_for_gunzip_ch = genome_data_ch.trapid_gf
            .branch { ID, trapid_gf ->
                gunzip: trapid_gf.name.endsWith('.gz')
                skip: true
            }
        gunzipTrapidGF(trapid_gf_for_gunzip_ch.gunzip)
        gunzipTrapidGF.out.gunzip.mix(trapid_gf_for_gunzip_ch.skip)
            .set { uncompressed_trapid_gf_ch }

        trapid_rf_for_gunzip_ch = genome_data_ch.trapid_rf
            .branch { ID, trapid_rf ->
                gunzip: trapid_rf.name.endsWith('.gz')
                skip: true
            }
        gunzipTrapidRF(trapid_rf_for_gunzip_ch.gunzip)
        gunzipTrapidRF.out.gunzip.mix(trapid_rf_for_gunzip_ch.skip)
            .set { uncompressed_trapid_rf_ch }

        emit:
        eggnog = uncompressed_eggnog_ch
        mapman = uncompressed_mapman_ch
        interproscan = uncompressed_interproscan_ch
        trapid_go = uncompressed_trapid_go_ch
        trapid_gf = uncompressed_trapid_gf_ch
        trapid_rf = uncompressed_trapid_rf_ch

}