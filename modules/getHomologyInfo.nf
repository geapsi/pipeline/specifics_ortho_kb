//*************************************************//
//  create nodes and edges associated to orthology //
//*************************************************//

process getHomologyInfo {
    conda 'conda-forge::pandas=2.2.1'
    container 'quay.io/biocontainers/pandas:2.2.1'

    input:
    path N0
    path merged_gff
    path logfile
    output:
    path "*.header"
    path "*.csv"

    script:
    """
    python3 ${params.project_dir}/bin/make_homology_nodes_edges.py \\
    -forthogroups $N0 \\
    -gff $merged_gff \\
    -logfile $logfile \\
    -max_paralogs ${params.max_paralogs} \\
    -output_nodes_og orthogroup_nodes.tmp \\
    -output_nodes_bioinfo_protocol homology_bioinfo_protocol_nodes.tmp \\
    -output_edges_gene_og gene_orthogroup_edges.tmp \\
    -output_edges_gene_homologous_gene gene_homologous_gene_edges.tmp \\
    -output_edges_og_bioinfo_protocol orthogroup_bioinfo_protocol_edges.tmp

    for file in *.tmp; do
        filebase=\${file%.*}
        head -n1 \$file > \${filebase}.header
        tail -n +2 \$file > \${filebase}.csv
    done
    """
}
