//*************************************************//
//     get protein nodes from the fasta file       //
//*************************************************//

process getProteinNodes {
    conda 'conda-forge::pandas=2.2.3 conda-forge::biopython=1.85'
    container 'community.wave.seqera.io/library/biopython_pandas:200d10fd3619a239'

    input:
    tuple val(ID), path(protein), path(sp_info)
    output:
    path "protein_nodes.header"
    tuple val(ID), path("${ID}_protein_nodes.csv"), emit: nodes_protein
    tuple val("protein_nodes.csv"), path("${ID}_protein_nodes.csv"), emit: protein

    script:
    """
    python3 ${params.project_dir}/bin/make_protein_nodes.py \\
    -fasta $protein \\
    -sp_info $sp_info \\
    -species $ID \\
    -output tmp
    head -n1 tmp > protein_nodes.header
    tail -n+2 tmp > ${ID}_protein_nodes.csv
    """
}
