//*************************************************//
//   From TRAPID RF file, get nodes and edges      //
//*************************************************//

process getRnaFamilies {
    conda 'conda-forge::pandas=2.2.1'
    container 'quay.io/biocontainers/pandas:2.2.1'

    input:
    tuple val(ID), path(rf)
    output:
    path "*.header"
    tuple val("rna_family_nodes.csv"), path("${ID}_rna_family_nodes.csv"), emit: rna_family
    tuple val("rna_rna_family_edges.csv"), path("${ID}_rna_rna_family_edges.csv"), emit: rna_rna_family

    script:
    """
    python3 ${params.project_dir}/bin/make_rna_family_nodes_edges.py \\
    -rna_family $rf \\
    -output_nodes nodes.tmp \\
    -output_edges edges.tmp

    head -n1 nodes.tmp > rna_family_nodes.header
    tail -n+2 nodes.tmp > ${ID}_rna_family_nodes.csv

    head -n1 edges.tmp > rna_rna_family_edges.header
    tail -n+2 edges.tmp > ${ID}_rna_rna_family_edges.csv
    """
}
