//*************************************************//
//   From TRAPID GF file, get nodes and edges      //
//*************************************************//

process getGeneFamilies {
    conda 'conda-forge::pandas=2.2.1'
    container 'quay.io/biocontainers/pandas:2.2.1'

    input:
    tuple val(ID), path(gff3), path(gf)
    output:
    path "*.header"
    tuple val("gene_family_nodes.csv"), path("${ID}_gene_family_nodes.csv"), emit: gene_family
    tuple val("gene_gene_family_edges.csv"), path("${ID}_gene_gene_family_edges.csv"), emit: gene_gene_family

    script:
    """
    python3 ${params.project_dir}/bin/make_gene_family_nodes_edges.py \\
    -gene_family $gf \\
    -gff $gff3 \\
    -output_nodes nodes.tmp \\
    -output_edges edges.tmp

    head -n1 nodes.tmp > gene_family_nodes.header
    tail -n+2 nodes.tmp > ${ID}_gene_family_nodes.csv

    head -n1 edges.tmp > gene_gene_family_edges.header
    tail -n+2 edges.tmp > ${ID}_gene_gene_family_edges.csv
    """
}
