//*************************************************//
//            Get annotated RNA nodes              //
//*************************************************//

process getRnaNodes {
    conda 'conda-forge::pandas=2.2.1'
    container 'quay.io/biocontainers/pandas:2.2.1'

    input:
    tuple val(ID), path(gff3), path(sp_info)
    output:
    path "rna_nodes.header"
    tuple val(ID), path("${ID}_rna_nodes.csv"), emit: nodes_rna
    tuple val("rna_nodes.csv"), path("${ID}_rna_nodes.csv"), emit: rna

    script:
    """
    python3 ${params.project_dir}/bin/make_rna_nodes.py \\
    -gff $gff3 \\
    -sp_info $sp_info \\
    -species $ID \\
    -output tmp
    head -n1 tmp > rna_nodes.header
    tail -n+2 tmp > ${ID}_rna_nodes.csv
    """

}
