process getQTLInfo {
    conda 'conda-forge::pandas=2.2.3 bioconda::pybedtools=0.11.0'
    container 'community.wave.seqera.io/library/pybedtools_pandas:6eae56b7891a94c6'

    input:
    tuple val(ID), val(status), val(species), path(dataset), path(qtl), path(qtl_annotation), path(gff), path(chr_conv)
    output:
    path "*.header"
    tuple val("genetics_qtl_nodes.csv"), path("${ID}_genetics_qtl_nodes.csv"), emit: qtl
    tuple val("genetics_dataset_nodes.csv"), path("${ID}_genetics_dataset_nodes.csv"), emit: dataset
    tuple val("genetics_population_nodes.csv"), path("${ID}_genetics_population_nodes.csv"), emit: population
    tuple val("genetics_site_nodes.csv"), path("${ID}_genetics_site_nodes.csv"), emit: site
    tuple val("genetics_year_nodes.csv"), path("${ID}_genetics_year_nodes.csv"), emit: year
    tuple val("genetics_free_term_nodes.csv"), path("${ID}_genetics_free_term_nodes.csv"), emit: free_term
    tuple val("genetics_gene_qtl_edges.csv"), path("${ID}_genetics_gene_qtl_edges.csv"), emit: gene_qtl
    tuple val("genetics_qtl_chromosome_edges.csv"), path("${ID}_genetics_qtl_chromosome_edges.csv"), emit: qtl_chromosome
    tuple val("genetics_qtl_dataset_edges.csv"), path("${ID}_genetics_qtl_dataset_edges.csv"), emit: qtl_dataset
    tuple val("genetics_qtl_population_edges.csv"), path("${ID}_genetics_qtl_population_edges.csv"), emit: qtl_population
    tuple val("genetics_qtl_site_edges.csv"), path("${ID}_genetics_qtl_site_edges.csv"), emit: qtl_site
    tuple val("genetics_qtl_year_edges.csv"), path("${ID}_genetics_qtl_year_edges.csv"), emit: qtl_year
    tuple val("genetics_qtl_onto_edges.csv"), path("${ID}_genetics_qtl_onto_edges.csv"), emit: qtl_onto
    tuple val("genetics_qtl_free_term_edges.csv"), path("${ID}_genetics_qtl_free_term_edges.csv"), emit: qtl_free_term
    path "${ID}_qtl_logs.txt"

    script:
    if ( params.convert_qtl_chr )
        """
        python3 ${params.project_dir}/bin/make_qtl_nodes_edges.py \\
        -dataset $dataset \\
        -status $status \\
        -qtl $qtl \\
        -qtl_annotation $qtl_annotation \\
        -gff $gff \\
        -chr_conv $chr_conv \\
        -logs ${ID}_qtl_logs.txt \\
        -output_nodes_qtl ${ID}_genetics_qtl_nodes.tmp \\
        -output_nodes_dataset ${ID}_genetics_dataset_nodes.tmp \\
        -output_nodes_population ${ID}_genetics_population_nodes.tmp \\
        -output_nodes_site ${ID}_genetics_site_nodes.tmp \\
        -output_nodes_year ${ID}_genetics_year_nodes.tmp \\
        -output_nodes_free_term ${ID}_genetics_free_term_nodes.tmp  \\
        -output_edges_gene_qtl ${ID}_genetics_gene_qtl_edges.tmp \\
        -output_edges_qtl_chromosome ${ID}_genetics_qtl_chromosome_edges.tmp \\
        -output_edges_qtl_dataset ${ID}_genetics_qtl_dataset_edges.tmp \\
        -output_edges_qtl_population ${ID}_genetics_qtl_population_edges.tmp \\
        -output_edges_qtl_site ${ID}_genetics_qtl_site_edges.tmp \\
        -output_edges_qtl_year ${ID}_genetics_qtl_year_edges.tmp \\
        -output_edges_qtl_onto ${ID}_genetics_qtl_onto_edges.tmp \\
        -output_edges_qtl_free_term ${ID}_genetics_qtl_free_term_edges.tmp

        for file in *.tmp; do
            filebase=\${file%.*}
            prefix="$ID"
            file_no_prefix=\${filebase#"\$prefix"_}
            head -n1 \$file > \${file_no_prefix}.header
            tail -n +2 \$file > \${filebase}.csv
        done
        """
    else
        """
        python3 ${params.project_dir}/bin/make_qtl_nodes_edges.py \\
        -dataset $dataset \\
        -status $status \\
        -qtl $qtl \\
        -qtl_annotation $qtl_annotation \\
        -gff $gff \\
        -logs ${ID}_qtl_logs.txt \\
        -output_nodes_qtl ${ID}_genetics_qtl_nodes.tmp \\
        -output_nodes_dataset ${ID}_genetics_dataset_nodes.tmp \\
        -output_nodes_population ${ID}_genetics_population_nodes.tmp \\
        -output_nodes_site ${ID}_genetics_site_nodes.tmp \\
        -output_nodes_year ${ID}_genetics_year_nodes.tmp \\
        -output_nodes_free_term ${ID}_genetics_free_term_nodes.tmp  \\
        -output_edges_gene_qtl ${ID}_genetics_gene_qtl_edges.tmp \\
        -output_edges_qtl_chromosome ${ID}_genetics_qtl_chromosome_edges.tmp \\
        -output_edges_qtl_dataset ${ID}_genetics_qtl_dataset_edges.tmp \\
        -output_edges_qtl_population ${ID}_genetics_qtl_population_edges.tmp \\
        -output_edges_qtl_site ${ID}_genetics_qtl_site_edges.tmp \\
        -output_edges_qtl_year ${ID}_genetics_qtl_year_edges.tmp \\
        -output_edges_qtl_onto ${ID}_genetics_qtl_onto_edges.tmp \\
        -output_edges_qtl_free_term ${ID}_genetics_qtl_free_term_edges.tmp

        for file in *.tmp; do
            filebase=\${file%.*}
            prefix="$ID"
            file_no_prefix=\${filebase#"\$prefix"_}
            head -n1 \$file > \${file_no_prefix}.header
            tail -n +2 \$file > \${filebase}.csv
        done
        """
}
