process getTranscriptomicCounts {
    conda 'conda-forge::scikit-learn=1.2.1 conda-forge::pandas=1.5.3'
    container 'community.wave.seqera.io/library/pandas_pip_scikit-learn:bd58081d92fb05b5'

    input:
    tuple val(ID), val(status), path(bioinfo_protocol), path(dataset), path(counts), path(tpm), path(metadata), path(samples_annotation)
    output:
    path "*.header"

    tuple val("rnaseq_bioinfo_protocol_nodes.csv"), path("${ID}_rnaseq_bioinfo_protocol_nodes.csv"), emit: bioinfo_protocol
    tuple val("rnaseq_dataset_nodes.csv"), path("${ID}_rnaseq_dataset_nodes.csv"), emit: dataset
    tuple val("rnaseq_condition_nodes.csv"), path("${ID}_rnaseq_condition_nodes.csv"), emit: condition
    tuple val("rnaseq_free_term_nodes.csv"), path("${ID}_rnaseq_free_term_nodes.csv"), emit: free_term
    tuple val("rnaseq_sample_nodes.csv"), path("${ID}_rnaseq_sample_nodes.csv"), emit: sample
    tuple val("rnaseq_dataset_bioinfo_protocol_edges.csv"), path("${ID}_rnaseq_dataset_bioinfo_protocol_edges.csv"), emit: dataset_bioinfo_protocol
    tuple val("rnaseq_condition_dataset_edges.csv"), path("${ID}_rnaseq_condition_dataset_edges.csv"), emit: condition_dataset
    tuple val("rnaseq_sample_condition_edges.csv"), path("${ID}_rnaseq_sample_condition_edges.csv"), emit: sample_condition
    tuple val("rnaseq_gene_sample_edges.csv"), path("${ID}_rnaseq_gene_sample_edges.csv"), emit: gene_sample
    tuple val("rnaseq_gene_condition_edges.csv"), path("${ID}_rnaseq_gene_condition_edges.csv"), emit: gene_condition
    tuple val("rnaseq_condition_onto_edges.csv"), path("${ID}_rnaseq_condition_onto_edges.csv"), emit: condition_onto
    tuple val("rnaseq_condition_free_term_edges.csv"), path("${ID}_rnaseq_condition_free_term_edges.csv"), emit: condition_free_term

    script:
    """
    python3 ${params.project_dir}/bin/make_transcriptomics_counts_nodes_edges.py \\
    -mode nf-core \\
    -nf_salmon_counts $counts \\
    -nf_salmon_tpm $tpm \\
    -nf_metadata $metadata \\
    -metadata_annotation $samples_annotation \\
    -dataset $dataset \\
    -status $status \\
    -bioinfo_protocol $bioinfo_protocol \\
    -tpm_threshold ${params.tpm_threshold} \\
    -filter_genes_zero_counts ${params.filter_genes_zero_counts} \\
    -output_nodes_bioinfo_protocol ${ID}_rnaseq_bioinfo_protocol_nodes.tmp \\
    -output_nodes_dataset ${ID}_rnaseq_dataset_nodes.tmp \\
    -output_nodes_condition ${ID}_rnaseq_condition_nodes.tmp  \\
    -output_nodes_free_term ${ID}_rnaseq_free_term_nodes.tmp  \\
    -output_nodes_sample ${ID}_rnaseq_sample_nodes.tmp \\
    -output_edges_dataset_bioinfo_protocol ${ID}_rnaseq_dataset_bioinfo_protocol_edges.tmp \\
    -output_edges_condition_dataset ${ID}_rnaseq_condition_dataset_edges.tmp \\
    -output_edges_sample_condition ${ID}_rnaseq_sample_condition_edges.tmp \\
    -output_edges_gene_sample ${ID}_rnaseq_gene_sample_edges.tmp \\
    -output_edges_gene_condition ${ID}_rnaseq_gene_condition_edges.tmp \\
    -output_edges_condition_onto ${ID}_rnaseq_condition_onto_edges.tmp \\
    -output_edges_condition_free_term ${ID}_rnaseq_condition_free_term_edges.tmp

    for file in *.tmp; do
        filebase=\${file%.*}
        prefix="$ID"
        file_no_prefix=\${filebase#"\$prefix"_}
        echo \${file_no_prefix}.header
        head -n1 \$file > \${file_no_prefix}.header
        tail -n +2 \$file > \${filebase}.csv
    done
    """
}
