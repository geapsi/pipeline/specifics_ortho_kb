//*************************************************//
//            Get edges from RNA to GO             //
//*************************************************//

process getRnaToGoEdges {
    conda 'conda-forge::pandas=2.2.1'
    container 'quay.io/biocontainers/pandas:2.2.1'

    input:
    tuple val(ID), path(go)
    output:
    path "rna_go_edges.header"
    tuple val("rna_go_edges.csv"), path("${ID}_rna_go_edges.csv"), emit: rna_go


    script:
    """
    python3 ${params.project_dir}/bin/make_rna_go_edges.py \\
    -go $go \\
    -ignore_hidden_gos ${params.ignore_hidden_gos} \\
    -output_edges tmp

    head -n1 tmp > rna_go_edges.header
    tail -n+2 tmp > ${ID}_rna_go_edges.csv
    """
}
