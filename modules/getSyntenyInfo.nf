//*************************************************//
//          get synteny nodes and edges            //
//*************************************************//

process getSyntenyInfo {
    conda 'conda-forge::pandas=2.2.3 bioconda::pybedtools=0.11.0'
    container 'community.wave.seqera.io/library/pybedtools_pandas:6eae56b7891a94c6'

    // get several nodes and edges from synteny mcscanx files
    input:
    path species_chr
    path collinearity
    path tandem
    output:
    path "*.header"
    path "*.csv"

    script:
    """
    python3 ${params.project_dir}/bin/make_mcscanx_synteny_nodes_edges.py \\
    -collinearity $collinearity \\
    -merged_gff $species_chr \\
    -tandem $tandem \\
    -output_nodes_synteny synteny_nodes.tmp \\
    -output_nodes_bioinfo_protocol synt_bioinfo_protocol_nodes.tmp \\
    -output_edges_gene_synteny gene_synteny_edges.tmp \\
    -output_edges_tandem_gene_synteny tandem_gene_synteny_edges.tmp \\
    -output_edges_synteny_chromosome synteny_chromosome_edges.tmp \\
    -output_edges_synteny_bioinfo_protocol synteny_bioinfo_protocol_edges.tmp

    for file in *.tmp; do
        filebase=\${file%.*}
        head -n1 \$file > \${filebase}.header
        tail -n +2 \$file > \${filebase}.csv
    done
    """
}
