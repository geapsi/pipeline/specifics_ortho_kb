//*************************************************//
//  create edges btw genes and rnas using gff      //
//*************************************************//

process getGeneToRnaEdges {
    conda 'conda-forge::pandas=2.2.1'
    container 'quay.io/biocontainers/pandas:2.2.1'

    input:
    tuple val(ID), path(gff3)
    output:
    path "gene_rna_edges.header"
    tuple val("gene_rna_edges.csv"), path("${ID}_gene_rna_edges.csv"), emit: gene_rna

    script:
    """
    python3 ${params.project_dir}/bin/make_gene_rna_edges.py \\
    -gff $gff3 \\
    -output tmp
    head -n1 tmp > gene_rna_edges.header
    tail -n+2 tmp > ${ID}_gene_rna_edges.csv
    """
}
