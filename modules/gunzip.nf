//*************************************************//
//              Unzip a .gz file                   //
//*************************************************//

process gunzip {
    conda 'conda-forge::gzip=1.11'
    container 'quay.io/biocontainers/gzip:1.11'

    input:
    tuple val(ID), path(archive)
    output:
    tuple val(ID), path("$uncompressed_file"), emit: gunzip

    script:
    uncompressed_file = archive.toString() - '.gz'
    """
    gunzip -f ${archive} > ${uncompressed_file}
    """
}
