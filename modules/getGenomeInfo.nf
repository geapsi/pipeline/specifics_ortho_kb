//*************************************************//
//       Get gene nodes and its annotation         //
//*************************************************//

process getGenomeInfo {
    conda 'conda-forge::pandas=2.2.1'
    container 'quay.io/biocontainers/pandas:2.2.1'

    input:
    tuple val(ID), path(gff3), path(chr_conv), path(fai), path(eggnog), path(sp_info)
    output:
    path "*.header"
    tuple val(ID), path("${ID}_gene_nodes.csv"), emit: nodes_gene
    tuple val("gene_nodes.csv"), path("${ID}_gene_nodes.csv"), emit: gene
    tuple val("chromosome_nodes.csv"), path("${ID}_chromosome_nodes.csv"), emit: chromosome
    tuple val("genome_nodes.csv"), path("${ID}_genome_nodes.csv"), emit: genome
    tuple val("accession_nodes.csv"), path("${ID}_accession_nodes.csv"), emit: accession
    tuple val("species_nodes.csv"), path("${ID}_species_nodes.csv"), emit: species
    tuple val("gene_chromosome_edges.csv"), path("${ID}_gene_chromosome_edges.csv"), emit: gene_chromosome
    tuple val("chromosome_genome_edges.csv"), path("${ID}_chromosome_genome_edges.csv"), emit: chromosome_genome
    tuple val("genome_accession_edges.csv"), path("${ID}_genome_accession_edges.csv"), emit: genome_accession
    tuple val("accession_species_edges.csv"), path("${ID}_accession_species_edges.csv"), emit: accession_species


    script:
    """
    python3 ${params.project_dir}/bin/make_genome_nodes_edges.py \\
    -gff $gff3 \\
    -fai $fai \\
    -emappannot $eggnog \\
    -sp_info $sp_info \\
    -chr_conv_table $chr_conv \\
    -species $ID \\
    -output_nodes_gene gene_nodes.tmp \\
    -output_nodes_chromosome chromosome_nodes.tmp \\
    -output_nodes_genome genome_nodes.tmp \\
    -output_nodes_accession accession_nodes.tmp \\
    -output_nodes_species species_nodes.tmp \\
    -output_edges_gene_chromosome gene_chromosome_edges.tmp \\
    -output_edges_chromosome_genome chromosome_genome_edges.tmp \\
    -output_edges_genome_accession genome_accession_edges.tmp \\
    -output_edges_accession_species accession_species_edges.tmp

    head -n1 gene_nodes.tmp > gene_nodes.header
    tail -n+2 gene_nodes.tmp > ${ID}_gene_nodes.csv
    head -n1 chromosome_nodes.tmp > chromosome_nodes.header
    tail -n+2 chromosome_nodes.tmp > ${ID}_chromosome_nodes.csv
    head -n1 genome_nodes.tmp > genome_nodes.header
    tail -n+2 genome_nodes.tmp > ${ID}_genome_nodes.csv
    head -n1 accession_nodes.tmp > accession_nodes.header
    tail -n+2 accession_nodes.tmp > ${ID}_accession_nodes.csv
    head -n1 species_nodes.tmp > species_nodes.header
    tail -n+2 species_nodes.tmp > ${ID}_species_nodes.csv

    head -n1 gene_chromosome_edges.tmp > gene_chromosome_edges.header
    tail -n+2 gene_chromosome_edges.tmp > ${ID}_gene_chromosome_edges.csv
    head -n1 chromosome_genome_edges.tmp > chromosome_genome_edges.header
    tail -n+2 chromosome_genome_edges.tmp > ${ID}_chromosome_genome_edges.csv
    head -n1 genome_accession_edges.tmp > genome_accession_edges.header
    tail -n+2 genome_accession_edges.tmp > ${ID}_genome_accession_edges.csv
    head -n1 accession_species_edges.tmp > accession_species_edges.header
    tail -n+2 accession_species_edges.tmp > ${ID}_accession_species_edges.csv
    """
}
