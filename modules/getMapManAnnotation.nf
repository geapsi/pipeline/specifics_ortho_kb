//*************************************************//
// create nodes and edges associated to mapman     //
//*************************************************//

process getMapManAnnotation {
    conda 'conda-forge::pandas=2.2.3 conda-forge::rdflib=7.1.3'
    container 'community.wave.seqera.io/library/pandas_rdflib:d8d1f4e7216a8956'

    input:
    tuple val(ID), path(gff3), path(mapman)
    output:
    path "mapman_ontology.ttl"
    path "rna_mapman_edges.header"
    tuple val("rna_mapman_edges.csv"), path("${ID}_rna_mapman_edges.csv"), emit: rna_mapman

    script:
    """
    python3 ${params.project_dir}/bin/make_mapman_nodes_edges.py \\
    -mapman $mapman \\
    -gff $gff3 \\
    -skip_bin_35  \\
    -output_nodes mapman_ontology.ttl \\
    -output_edges edges.tmp

    head -n1 edges.tmp > rna_mapman_edges.header
    tail -n+2 edges.tmp > ${ID}_rna_mapman_edges.csv
    """
}
