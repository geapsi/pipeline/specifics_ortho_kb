//*************************************************//
//        Create eggnog annotation file            //
//*************************************************//

process getEggnogAnnotation {
    conda 'bioconda::eggnog-mapper=2.1.6'
    container 'quay.io/biocontainers/eggnog-mapper:2.1.6'

    input:
    tuple val(ID), path(protein)
    path eggnog_db

    output:
    tuple val(ID), path("${ID}.emapper.annotations"), emit: eggnog

    script:
    """
    emapper.py \\
    --cpu 10 \\
    -i $protein \\
    --output ${ID} \\
    -m diamond --data_dir $eggnog_db \\
    --database euk
    """
}
