//*************************************************//
//         Extract transcripts sequences           //
//*************************************************//

process extractTranscripts {
    conda 'conda-forge::python=3.9.12 anaconda::more-itertools=8.12.0 bioconda::jcvi=1.2.7'

    input:
    tuple val(ID), path(genome), path(gff3)
    output:
    tuple val(ID), path("${ID}_transcripts.fa.gz"), emit: transcripts

    script:
    """
    python -m jcvi.formats.gff load $gff3 $genome \\
        -o ${ID}_transcripts.fa \\
        --parents mRNA \\
        --children five_prime_UTR,CDS,three_prime_UTR \\
        --desc_attribute None
    gzip ${ID}_transcripts.fa
    """
}

