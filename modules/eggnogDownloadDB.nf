//*************************************************//
//   download the eggnog DB before eggnog run      //
//*************************************************//

process eggnogDownloadDB {
    // not working for now, make it beforehand and give path in config
    label 'eggnog_db'
    conda 'bioconda::eggnog-mapper=2.1.6'
    container 'quay.io/biocontainers/eggnog-mapper:2.1.6'

    output:
    path "eggnog-db"

    script:
    """
    mkdir eggnog-db
    download_eggnog_data.py --data_dir eggnog-db -y
    """
}
