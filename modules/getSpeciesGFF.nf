//*************************************************//
// Create merge of gff with species acronym inside //
//*************************************************//

process getSpeciesGFF {
    label 'sp_chr'

    input:
    tuple val(ID), path(gff3)
    output:
    path "${ID}_species_gff.tsv"
    script:
    """
    # get species acronym before gff line
    species=$ID
    awk -F"\t" -v sp=\$species '\$1!~ "^#" {print sp"\t"\$0}' $gff3 > ${ID}_species_gff.tsv
    """
}
