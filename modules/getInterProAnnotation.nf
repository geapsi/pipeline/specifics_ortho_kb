//*************************************************//
//  get nodes and edges associated to interpro     //
//*************************************************//

process getInterProAnnotation {
    conda 'conda-forge::pandas=2.2.1'
    container 'quay.io/biocontainers/pandas:2.2.1'

    input:
    tuple val(ID), path(interpro)
    output:
    path "*.header"
    tuple val("protein_annotation_nodes.csv"), path("${ID}_protein_annotation_nodes.csv"), emit: interpro
    tuple val("protein_protein_annotation_edges.csv"), path("${ID}_protein_protein_annotation_edges.csv"), emit: protein_interpro

    script:
    """
    python3 ${params.project_dir}/bin/make_protein_annotations_nodes_edges.py \\
    -ips $interpro \\
    -output_nodes nodes.tmp \\
    -output_edges edges.tmp

    head -n1 nodes.tmp > protein_annotation_nodes.header
    tail -n+2 nodes.tmp > ${ID}_protein_annotation_nodes.csv

    head -n1 edges.tmp > protein_protein_annotation_edges.header
    tail -n+2 edges.tmp > ${ID}_protein_protein_annotation_edges.csv
    """
}
