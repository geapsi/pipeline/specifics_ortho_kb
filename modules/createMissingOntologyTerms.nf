//**********************************************************//
//     Create nodes and edges for missing ontology terms    //
//**********************************************************//

process createMissingOntologyTerms {
    conda 'conda-forge::pandas=2.2.1'
    container 'quay.io/biocontainers/pandas:2.2.1'

    input:
    path csv
    output:
    path "*.header"
    path "*.csv"

    script:
    """
    python3 ${params.project_dir}/bin/make_missing_ontology_term_nodes_edges.py \\
    -csv_new_terms $csv \\
    -output_nodes_new_onto_terms new_ontology_term_nodes.tmp \\
    -output_edges_new_onto_terms_onto new_ontology_term_ontology_edges.tmp

    for file in *.tmp; do
        filebase=\${file%.*}
        head -n1 \$file > \${filebase}.header
        tail -n +2 \$file > \${filebase}.csv
    done
    """
}