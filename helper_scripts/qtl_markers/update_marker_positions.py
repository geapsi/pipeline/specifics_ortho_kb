#!/usr/bin/env python3

import argparse
import json

import statistics
import numpy as np
import pandas as pd
from icecream import ic

parser = argparse.ArgumentParser(description = 'Updates positions of markers in the \
    integration file to match those provided from the aligner')
parser.add_argument('-m', '--markers', help = 'alignment csv file of the markers',
    required=True)
parser.add_argument('-m2', '--markers2', help = 'second alignment csv file of markers \
    without any duplicate, which much contain the following columns: \
    marker (with the dataset prefix), chromosome, start, end, method',
    required=False)
parser.add_argument('-i', '--integration', help = 'integration csv of the markers',
    required=True)
parser.add_argument('-s', '--source', help = 'json matching each dataset id with \
    the dataset id of the source of the markers', required=True)
parser.add_argument('--output_updated_positions', help = 'output of the marker positions \
    of QTLs with annotation on whether the alignment is correct and/or complete',
    required=False, default='marker_positions_updated.csv')
parser.add_argument('--output_updated_integration', help = 'output of the integration csv \
    with the new positions', required=False, default='dataset_integration_updated.csv')
args = parser.parse_args()


# pd.set_option('display.max_columns', None)

def read_json_marker_source(json_file):
    """Converts a json to python format dic of marker sources
    @param json_file   a json file including an array called "marker_source" with
                        for each each dataset an array of sources
    @return TODO
    """
    l_json_marker_source = json.load(json_file)['marker_source']
    d_marker_source = {k: v for d in l_json_marker_source for k, v in d.items()}
    return d_marker_source

def select_qtl_position_cols(df):
    """Returns the df with only the row ids and marker information
    @param df   a pd dataframe from the integration process

    @return the same df with columns of markers only
    """
    cols = ['dataset_id', 'qtl_id', 'chromosome',
        'peakmarker_id', 'peakmarker_start', 'peakmarker_end',
        'leftmarker_id', 'leftmarker_start', 'leftmarker_end',
        'rightmarker_id', 'rightmarker_end', 'rightmarker_end']
    return df[cols].copy()

def select_marker_position_cols(df):
    """Returns the df with only the row ids and marker information
    @param df   a pd dataframe of markers with qseqid, sseqid, sstart and ssend or a m2 format

    @return the same df with columns of markers only
    """
    cols = ['qseqid', 'sseqid', 'sstart', 'send']
    cols_m2 = ['marker', 'chromosome', 'start', 'end', 'method']
    if all(col in df.columns for col in cols):
        df_return = df[cols].copy()
        df_return['method'] = 'minimap2'
    elif all(col in df.columns for col in cols_m2):
        df_return =  df[cols_m2].copy()
    else:
        return None
    df_return.columns = ['full_marker_id', 'chromosome', 'start', 'end', 'method']
    return df_return

def split_marker_id(df):
    """Returns the df with the marker id splitted into 2-cols with source_id and marker_id
    @param df   a pd dataframe of markers with qseqid in the form of "sourceid_markerid"

    @return the same df with 2 new columns, dataset_id and marker_id
    """
    df_split_id = df.copy()
    df_split_id[['source_id', 'marker_id']] = df_split_id['full_marker_id'].str.split('_',
        expand=True, n=1)
    return df_split_id

def assign_datasets_to_markers(df_markers, d_marker_sources):
    """Returns a subset of df where source_id from df match the source
    @param df_markers   a pd dataframe of markers
    @param l_marker_sources   a d of marker sources used for each dataset

    @return the subset of markers from sources in l_marker_sources
    """
    df_dataset_source = pd.DataFrame([(key, val) for key, values in d_marker_sources.items() for val in values], columns=['dataset_id', 'source_id'])
    df_marker_dataset = df_dataset_source.merge(df_markers, how='left', on='source_id')
    return df_marker_dataset

def select_markers_used_in_integration(df_marker, df_integration):
    """Returns a subset of df_marker that are used for QTL position in df_integration
    @param df_marker   a pd dataframe of aligned markers
    @param df_integration   a pd dataframe of integrated markers

    @return the subset of markers used
    """
    df_marker_used = df_marker.copy()
    l_marker_ids = [df_integration['peakmarker_id'],
        df_integration['leftmarker_id'], df_integration['rightmarker_id']]
    s_marker_used = pd.concat(l_marker_ids).dropna().drop_duplicates()
    df_marker_used = df_marker_used[df_marker_used['marker_id'].isin(s_marker_used)]
    return df_marker_used

def get_long_qtl_marker_type(df):
    """Returns for each qtl the marker used and their current status (left, right, peak)
    @param df   a pd dataframe of the integration, with dataset_id, qtl_id,
                peakmarker_id, leftmarker_id, rightmarker_id

    @return a df with the type of each marker
    """
    subset_cols = ['dataset_id', 'qtl_id', 'peakmarker_id', 'leftmarker_id', 'rightmarker_id']
    df_interval_type = df.copy()
    df_interval_type_melt = pd.melt(df_interval_type[subset_cols],
        id_vars=['dataset_id', 'qtl_id'],
        value_vars=['peakmarker_id', 'leftmarker_id', 'rightmarker_id']).dropna()

    d_colname_to_type = {'peakmarker_id': 'peak', 'leftmarker_id': 'left',
        'rightmarker_id': 'right'}
    df_interval_type_melt['variable'] = df_interval_type_melt['variable'].replace(d_colname_to_type)
    df_interval_type_melt.columns = ['dataset_id', 'qtl_id', 'marker_status', 'marker_id']
    return df_interval_type_melt

def check_qtl_left_peak_right_marker_presence(df):
    """Returns a df qtl and its type (interval, peak, interval_peak)
    @param df   a pd dataframe of the integration, with dataset_id, qtl_id,
                marker_status

    @return same df with dataset_id, qtl_id and qtl_initial_type
    """
    df_qtl_type_grouped = df.groupby(['dataset_id', 'qtl_id'])['marker_status'].apply(list).reset_index()
    df_qtl_type_grouped['marker_status'] = df_qtl_type_grouped['marker_status'].apply(set)

    def qtl_type_conditions(cond):
        if cond.issuperset({'left', 'right', 'peak'}):
            return 'LPR'
        if cond.issuperset({'left', 'right'}):
            return 'LR'
        if cond.issuperset({'peak'}):
            return 'P'
        return 'missing_marker'

    func_qtl_cat = np.vectorize(qtl_type_conditions)
    df_qtl_type_grouped['qtl_initial_type'] = func_qtl_cat(df_qtl_type_grouped['marker_status'])

    return df_qtl_type_grouped[['dataset_id', 'qtl_id', 'qtl_initial_type']]

def infer_best_marker_alignment(df):
    """Returns a df with new positions of markers and annotations
    @param df   a pd dataframe of the integration, with dataset_id, qtl_id,
                marker_status

    @return same df with dataset_id, qtl_id and qtl_initial_type
    """
    df_qtl_group = df.copy()
    df_marker_multi_aligned_count = df_qtl_group.dropna().groupby(['dataset_id', 'qtl_id', 'marker_status']).size().reset_index()
    df_qtl_group = df_qtl_group.merge(df_marker_multi_aligned_count, how='left', on=['dataset_id', 'qtl_id', 'marker_status'])
    df_qtl_group.rename(columns={0: 'marker_align_count'}, inplace=True)
    df_qtl_group['marker_align_count'] = df_qtl_group['marker_align_count'].fillna(0).astype(int)

    df_result = pd.DataFrame()
    # Deal with QTL defined only by a peak marker (P)
    df_qtl_group_p = df_qtl_group[(df_qtl_group['qtl_initial_type'].isin(['P']))]
    p_groups = df_qtl_group_p.groupby(['dataset_id', 'qtl_id'])
    for _, group in p_groups:
        if len(group) > 1:
            group['info_peak'] = 'P aligned multiple times'
        elif group['chromosome'].isnull().all():
            group['info_peak'] = 'P never aligned'
        else:
            group['info_peak'] = 'P aligned once'

        df_result = pd.concat([df_result, group])

    # Deal with QTL defined by left and right markers (LR)
    df_qtl_group_lr = df_qtl_group[(df_qtl_group['qtl_initial_type'].isin(['LR', 'LPR']))]
    lr_groups = df_qtl_group_lr.groupby(['dataset_id', 'qtl_id'])
    for _, group in lr_groups:
        left_chromosome = set(group[group['marker_status'] == 'left']['chromosome'].values)
        left_pos = list(group[group['marker_status'] == 'left']['start'].values)
        right_chromosome = set(group[group['marker_status'] == 'right']['chromosome'].values)
        right_pos = list(group[group['marker_status'] == 'right']['start'].values)
        peak_chromosome = set(group[group['marker_status'] == 'peak']['chromosome'].values)
        peak_pos_start = list(group[group['marker_status'] == 'peak']['start'].values)
        peak_pos_end = list(group[group['marker_status'] == 'peak']['end'].values)

        case_missing_left_marker = np.nan in left_chromosome
        case_missing_right_marker = np.nan in right_chromosome
        case_left_right_markers_same_defined_chr = len(left_chromosome.intersection(right_chromosome)) == 1
        case_left_right_markers_diff_chr = len(left_chromosome.intersection(right_chromosome)) == 0
        case_left_several_align = len(left_pos) > 1
        case_right_several_align = len(right_pos) > 1
        case_peak_several_align = len(peak_pos_start) > 1
        case_missing_peak_marker = np.nan in peak_chromosome
        case_peak_several_chr = len(peak_chromosome) > 1
        case_peak_diff_chr_as_left_right = case_left_right_markers_same_defined_chr and (peak_chromosome != set()) and (
            len(left_chromosome.intersection(peak_chromosome)) != 1 or len(right_chromosome.intersection(peak_chromosome)) != 1)
        case_peak_out_left_right_interval = (len(peak_pos_start) == 1) and (
            not case_peak_diff_chr_as_left_right) and not ((
                min(left_pos) <= statistics.mean([peak_pos_start[0], peak_pos_end[0]]) <= max(right_pos)) or (
                min(right_pos) <= statistics.mean([peak_pos_start[0], peak_pos_end[0]]) <= max(left_pos)))

        group['info_interval'] = ''
        group['info_peak'] = ''
        if case_missing_left_marker:
            group['info_interval'] = 'L marker missing'
        if case_missing_right_marker:
            group['info_interval'] = 'R marker missing'
        if case_missing_left_marker and case_missing_right_marker:
            group['info_interval'] = 'L/R marker missing'
        if not (case_missing_left_marker or case_missing_right_marker) and case_left_right_markers_diff_chr:
            group['info_interval'] = 'L/R markers not same chr'
        if case_left_right_markers_same_defined_chr and not(case_missing_left_marker or case_missing_right_marker):
            group['info_interval'] = 'L/R markers are ok'
        if case_left_right_markers_same_defined_chr and (case_left_several_align or case_right_several_align):
            group['info_interval'] = 'L/R markers on same chr but several alignments'

        if len(peak_chromosome) >= 1:
            if case_peak_several_chr:
                group['info_peak'] = 'P on several chr'
            if case_peak_diff_chr_as_left_right:
                group['info_peak'] = 'P not on same chr as L/R markers'
            if case_peak_out_left_right_interval:
                group['info_peak'] = 'P not in the L/R interval'
            if case_left_right_markers_same_defined_chr and (not case_peak_diff_chr_as_left_right) and (not case_peak_out_left_right_interval):
                group['info_peak'] = 'P in the L/R interval'
            if case_left_right_markers_same_defined_chr and (not case_peak_diff_chr_as_left_right) and (not case_peak_out_left_right_interval) and case_peak_several_align:
                group['info_peak'] = 'P in the L/R interval with several alignments'
            if case_missing_peak_marker:
                group['info_peak'] = 'P marker is missing'

        df_result = pd.concat([df_result, group])
    df_result = df_result[['dataset_id', 'qtl_id', 'marker_status', 'marker_id', 'chromosome',
    'start', 'end', 'qtl_initial_type', 'marker_align_count', 'info_interval', 'info_peak']]

    df_result['automation'] = 'lost'
    df_result.loc[(df_result['qtl_initial_type'] == 'P') & (
        df_result['info_peak'] == 'P aligned once'), 'automation'] = 'OK'
    df_result.loc[(df_result['qtl_initial_type'] == 'LR') & (
        df_result['info_interval'] == 'L/R markers are ok'), 'automation'] = 'OK'
    df_result.loc[(df_result['qtl_initial_type'].isin(['LR', 'LPR'])) & (
        df_result['info_interval'] == 'L/R markers on same chr but several align'), 'automation'] = 'to_revise'
    df_result.loc[(df_result['qtl_initial_type'] == 'LPR') & (
        df_result['info_interval'] == 'L/R markers are ok') & (
            df_result['info_peak'] == 'P in the L/R interval'), 'automation'] = 'OK'
    df_result.loc[(df_result['qtl_initial_type'] == 'LPR') & (
        df_result['info_interval'] == 'L/R markers are ok') & (
            df_result['info_peak'].isin(['P not in the L/R interval', 'P not on same chr as L/R markers', 'P in the L/R interval with several alignments'])), 'automation'] = 'losing_peak'
    return df_result


def check_qtl_validity(df):
    """Returns a df with QTL with for each the information on their
        initial type (interval=LR, peak=P, interval_peak=LPR) and if any of
        markers from their interval is multi-aligned
    @param df   a pd dataframe of the integration, with dataset_id, qtl_id,
                marker_status, marker_id, chromosome, start, end

    @return same df as above with qtl_initial_type and multi_aligned_marker columns
    """
    df_qtl_type = df[['dataset_id', 'qtl_id', 'marker_status']]
    df_qtl_type = check_qtl_left_peak_right_marker_presence(df_qtl_type)
    df_qtl_type_align = df.merge(df_qtl_type, how='left',
        on=['dataset_id', 'qtl_id'])
    df_qtl_conservation = infer_best_marker_alignment(df_qtl_type_align)
    return df_qtl_conservation

def select_valid_qtl_in_wide(df_qtl_validity):
    """Returns a df with QTL with 'OK' automation status in a wide format based
        on marker_status
    @param df_qtl_validity  a pd dataframe with new marker positions which must at
        least include 'dataset_id', 'qtl_id', 'marker_status',
        'marker_id', 'chromosome', 'start', 'end', 'automation'

    @return same the df in a wide format and only for automation status where OK
    """
    df_qtl_validity_ok = df_qtl_validity[df_qtl_validity['automation'] == 'OK']
    df_qtl_validity_ok = df_qtl_validity_ok[['dataset_id', 'qtl_id', 'marker_status',
        'marker_id', 'chromosome', 'start', 'end']]

    df_qtl_validity_ok_wide = df_qtl_validity_ok.pivot(index=['dataset_id', 'qtl_id', 'chromosome'],
        columns='marker_status', values=['marker_id', 'start', 'end'])
    # flatten the column multi-index
    df_qtl_validity_ok_wide.columns = [f'{col[0]}_{col[1]}' for col in df_qtl_validity_ok_wide.columns]
    df_qtl_validity_ok_wide.reset_index(inplace=True)
    df_qtl_validity_ok_wide.columns = ['dataset_id', 'qtl_id', 'chromosome', 'leftmarker_id',
        'peakmarker_id', 'rightmarker_id', 'leftmarker_start', 'peakmarker_start',
        'rightmarker_start', 'leftmarker_end', 'peakmarker_end', 'rightmarker_end']
    df_qtl_validity_ok_wide = df_qtl_validity_ok_wide[['dataset_id', 'qtl_id', 'chromosome',
        'peakmarker_start', 'peakmarker_end',
        'leftmarker_start', 'leftmarker_end',
        'rightmarker_start', 'rightmarker_end']]
    return df_qtl_validity_ok_wide


def update_integration_with_new_pos(df_integration, df_qtl_valid_wide):
    """Returns a df with QTL with 'OK' automation status updated to new positions
        For non 'OK' QTL, positions are left empty and the 'ignore' is set to 'yes'
    @param df_datasets  a pd dataframe of the integration, which must at least include
        'dataset_id', 'qtl_id', 'chromosome', 'peakmarker_start', 'peakmarker_end',
        'leftmarker_start', 'leftmarker_end', 'rightmarker_start', 'rightmarker_end', ('ignore')
    @param df_qtl_validity  a pd dataframe with new marker positions which must at
        least include 'dataset_id', 'qtl_id', 'chromosome', 'peakmarker_start',
        'peakmarker_end', 'leftmarker_start', 'leftmarker_end',
        'rightmarker_start', 'rightmarker_end'

    @return same df as df_datasets with updated marker positions where automation is 'OK'
    """
    df_integration_cols = df_integration.columns
    df_integration_updated = df_integration.merge(df_qtl_valid_wide, how='left',
        on=['dataset_id', 'qtl_id'], suffixes=('_old_pos', ''))
    l_old_cols = [col for col in df_integration_updated.columns if col.endswith('_old_pos')]
    df_integration_updated.drop(l_old_cols, inplace=True, axis=1)
    df_integration_updated = df_integration_updated[df_integration_cols]
    # df_integration_updated['chromosome'] = df_integration_updated['chromosome'].fillna('TODO')
    df_integration_updated['ignore'] = 'no'
    df_integration_updated.loc[df_integration_updated['chromosome'].isna(), 'ignore'] = 'yes'
    return df_integration_updated

if __name__ == "__main__":

    with open(args.source, 'r', encoding='utf-8-sig') as json_f:
        d_marker_source = read_json_marker_source(json_f)

    df_integration = pd.read_csv(args.integration, header=0)
    df_integration = df_integration.applymap(lambda x: x.strip() if isinstance(x, str) else x)

    df_integration_marker_pos = select_qtl_position_cols(df_integration)
    # datasets = df_integration_marker_pos['dataset_id'].drop_duplicates()

    df_markers = pd.read_csv(args.markers, header=0)
    df_markers = df_markers.applymap(lambda x: x.strip() if isinstance(x, str) else x)
    df_markers_pos = select_marker_position_cols(df_markers)

    if args.markers2:
        # markers with known position obtained from another source
        df_markers_2 = pd.read_csv(args.markers2, header=0)
        df_markers_2 = df_markers_2.applymap(lambda x: x.strip() if isinstance(x, str) else x).dropna()
        df_markers_2_pos = select_marker_position_cols(df_markers_2)
        df_markers_2_pos = df_markers_2_pos[~df_markers_2_pos['full_marker_id'].isin(df_markers_pos['full_marker_id'])]
        df_markers_pos = pd.concat([df_markers_pos, df_markers_2_pos])

    df_markers_pos_split_id = split_marker_id(df_markers_pos)

    df_marker_source = assign_datasets_to_markers(df_markers_pos_split_id, d_marker_source)
    df_integration_qtl_marker = get_long_qtl_marker_type(df_integration_marker_pos)

    df_markers_dataset_pos = select_markers_used_in_integration(
        df_marker_source, df_integration_marker_pos) # markers used in the dataset


    df_integration_qtl_new_pos = df_integration_qtl_marker.merge(df_markers_dataset_pos,
        how='left', on=['dataset_id', 'marker_id'])
    df_integration_qtl_new_pos.drop('source_id', inplace=True, axis=1)
    df_integration_qtl_new_pos.columns = ['dataset_id', 'qtl_id', 'marker_status',
        'marker_id', 'align_marker_id', 'chromosome', 'start', 'end', 'method']
    df_qtl_validity = check_qtl_validity(df_integration_qtl_new_pos)
    df_qtl_validity.to_csv(args.output_updated_positions, index=False)
    df_qtl_validity_ok_wide = select_valid_qtl_in_wide(df_qtl_validity)
    df_integration_updated = update_integration_with_new_pos(df_integration, df_qtl_validity_ok_wide)
    df_integration_updated.to_csv(args.output_updated_integration, index=False)
